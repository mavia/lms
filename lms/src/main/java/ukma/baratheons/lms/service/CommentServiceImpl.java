package ukma.baratheons.lms.service;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.dao.CommentDao;
import ukma.baratheons.lms.dao.TaskDao;
import ukma.baratheons.lms.entity.Comment;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.User;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	private CommentDao commentDao;
	
	@Autowired
    private TaskDao taskDao;

	private static final Logger LOGGER = LogManager.getLogger(CommentServiceImpl.class);

	@Override
	public void addNew(long taskId, User user, String text) throws SQLException{
		long parentId = taskId;
		Task task = taskDao.getTasksById(taskId);
		if(task.getParentId() != null)
			if(task.getId()!=task.getParentId())
				parentId = task.getParentId();
		if(task.getParentId() != null){
        List<Task> dublicateTasks = taskDao.getTasksByParentId(parentId);
		for(Task t: dublicateTasks){
			Comment comment = new Comment();
			Date date = new Date();
			comment.setDate(new Timestamp(date.getTime()));
			comment.setVisibility(1);
	        comment.setText(text);
	        comment.setTask(new Task(t.getId()));
	        comment.setUser(user);
	        try {
				commentDao.create(comment);
				LOGGER.info("Create comment: " + comment.getText());
			} catch (SQLException e) {
				LOGGER.error("Failed create comment. " +
						" Exception: " + e.toString());
				e.printStackTrace();
			}
		}
		}else{
			Comment comment = new Comment();
			Date date = new Date();
			comment.setDate(new Timestamp(date.getTime()));
			comment.setVisibility(1);
	        comment.setText(text);
	        comment.setTask(new Task(parentId));
	        comment.setUser(user);
	        try {
				commentDao.create(comment);
				LOGGER.info("Create comment: " + comment.getText());
			} catch (SQLException e) {
				LOGGER.error("Failed create comment. " +
						" Exception: " + e.toString());
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<Comment> getCommentsByEqpt(long id) {
		return commentDao.getCommentsByEqpt(id);
	}
	
	@Override
	public List<Comment>getCommentsByTask(long taskId){
		return commentDao.getCommentsByTask(taskId);
	}
	
}
