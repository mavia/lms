package ukma.baratheons.lms.service;

import java.util.List;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.entity.Log;
import ukma.baratheons.lms.entity.Role;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.Unconfirmed;
import ukma.baratheons.lms.entity.User;

/**
 * This class is responsible for sending emails.
 * 
 * @version 0.1(waiting for implementation of all services)
 * @author Vitalii Mogolivskyi
 */

@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private MailBuilderImpl mailBuilder;


	@Override
	public void sentNewTaskSubmittedEmail(Task task) {	
			send(mailBuilder.buildTaskSubmittedEmail(task));
	}

	@Override
	@Async
	public void send(MimeMessagePreparator email) {
		mailSender.send(email);
	}
	
	@Override
	public void send(List<MimeMessagePreparator> emails) {
		for (MimeMessagePreparator email : emails)
			send(email);
	}
	@Override
	public void sendTaskConfirmationEmail(Unconfirmed unTask, long taskId) {
		send(mailBuilder.buildTaskConfirmationEmail(unTask, taskId));
	}
	@Override
	public void sendResetPasswordEmail(Unconfirmed unc, int key) {
		send(mailBuilder.buildResetPasswordEmail(unc, key));
	}
	@Override
	public void sendUserConfirmedEmail(User user) {
		send(mailBuilder.buildUserConfirmedEmail(user));	
	}
	
	@Override
	public void sendUserPaswordChangedEmail(User user, String password) {
		send(mailBuilder.buildUserPaswordChangedEmail(user, password));	
	}

	@Override
	public void sendTaskStatusChangedEmail(Task task, User user) {
		send(mailBuilder.buildTaskStatusChangedEmail(task, user));
	}
	@Override
	public void sendTaskRefusedByStaffEmail(Task task, User staff, User admin){
		send(mailBuilder.buildTaskRefusedByStaffEmail(task, staff, admin));
	}
	@Override
	public List<Log> getLogs(User user) {
		// TODO Auto-generated method stub
		return null;
	}

}
