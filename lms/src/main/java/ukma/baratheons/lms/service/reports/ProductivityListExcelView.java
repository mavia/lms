package ukma.baratheons.lms.service.reports;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class ProductivityListExcelView extends AbstractExcelView {

    @Override
    protected void buildExcelDocument(Map model, HSSFWorkbook workbook,
                                      HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HSSFSheet excelSheet = workbook.createSheet("Productivity");
        setExcelHeader(excelSheet);


        List<User> userList = (List<User>) model.get("productivityList");
        setExcelRows(excelSheet,userList);

    }

    public void setExcelHeader(HSSFSheet excelSheet) {
        HSSFRow excelHeader = excelSheet.createRow(0);
        excelHeader.createCell(0).setCellValue("User ID");
        excelHeader.createCell(1).setCellValue("First Name");
        excelHeader.createCell(2).setCellValue("Second Name");
        excelHeader.createCell(3).setCellValue("Completed");
        excelHeader.createCell(4).setCellValue("Rejected");
        excelSheet.autoSizeColumn(0);
        excelSheet.autoSizeColumn(1);
        excelSheet.autoSizeColumn(2);
        excelSheet.autoSizeColumn(3);
        excelSheet.autoSizeColumn(4);
    }

    public void setExcelRows(HSSFSheet excelSheet, List<User> userList){
        int record = 1;
        for (User user : userList) {
            HSSFRow excelRow = excelSheet.createRow(record++);
            excelRow.createCell(0).setCellValue(user.getId());
            excelRow.createCell(1).setCellValue(user.getFirstName());
            excelRow.createCell(2).setCellValue(user.getLastName());
            excelRow.createCell(3).setCellValue(user.getCountCompleted());
            excelRow.createCell(4).setCellValue(user.getCountCanceled());
            excelSheet.autoSizeColumn(0);
            excelSheet.autoSizeColumn(1);
            excelSheet.autoSizeColumn(2);
            excelSheet.autoSizeColumn(3);
            excelSheet.autoSizeColumn(4);
        }
    }
}
