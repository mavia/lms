package ukma.baratheons.lms.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.query.SearchEngine;
import ukma.baratheons.lms.service.SearchTaskProcessor.Filter;

@Service
public class TaskSearchServiceImp implements SearchEngine<Task> {
	
	@Autowired
	private CachedInputSearchProcessor<Filter> inputProcessor;
	
	@Autowired
	private SearchTaskProcessor searchProcessor;
	
	private boolean cacheEmailEnabled = true;
	
	private boolean cacheResultEnabled = true;
	
	private int resultCacheSize = 1000;

	private Map<String[], List<Task>> cachedResults;

	@Override
	public List<Task> search(int rights, final String query) {
		
		final String [] args = CachedInputSearchProcessor.splitInput(query);
		final List<Filter> inptCache = inputProcessor.getCache(args);
		
		if(inptCache != null && getCachedResults().containsKey(args)) {
			return getCachedResults().get(args);
		}
		
		List<Task> result = searchProcessor.getTasks(rights, inputProcessor.processInput(query));
		
		if(isCacheResultEnabled()) {
			getCachedResults().put(args, result);
		}
		
		return result;
	}
	
	private Map<String[], List<Task>> getCachedResults() {
		
		if(isCacheResultEnabled()) {
			cachedResults = new HashMap<String [], List<Task>>(resultCacheSize);
		}
		
		return cachedResults;
	}

	public boolean isCacheEmailEnabled() {
		return cacheEmailEnabled;
	}

	public void setCacheEmailEnabled(boolean cacheEmailEnabled) {
		this.cacheEmailEnabled = cacheEmailEnabled;
	}

	public boolean isCacheResultEnabled() {
		return cacheResultEnabled;
	}

	public void setCacheResultEnabled(boolean cacheResultEnabled) {
		this.cacheResultEnabled = cacheResultEnabled;
	}

	public int getResultCacheSize() {
		return resultCacheSize;
	}

	public void setResultCacheSize(int resultCacheSize) {
		this.resultCacheSize = resultCacheSize;
	}

}
