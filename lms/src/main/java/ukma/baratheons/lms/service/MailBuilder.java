package ukma.baratheons.lms.service;

import java.util.List;

import org.springframework.mail.javamail.MimeMessagePreparator;

import ukma.baratheons.lms.entity.Comment;
import ukma.baratheons.lms.entity.Role;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.Unconfirmed;
import ukma.baratheons.lms.entity.User;

public interface MailBuilder {

	public MimeMessagePreparator buildTaskStatusChangedEmail(Task task, User staff);

	public MimeMessagePreparator buildUserRightsChangedEmail(User user);

	public MimeMessagePreparator buildTaskCommentAddedEmail(Comment comment);

	public MimeMessagePreparator buildTaskSubmittedEmail(Task task);

	public MimeMessagePreparator buildTaskUrgencyChangedEmail(Task task);

	public MimeMessagePreparator buildTaskRefusedByStaffEmail(Task task, User staff, User admin);

	public MimeMessagePreparator buildRegistrationEmail(Unconfirmed unconfirmed, Role role);
	
	public MimeMessagePreparator buildTaskConfirmationEmail(Unconfirmed unconfirmed, long taskId);
	
	public MimeMessagePreparator buildResetPasswordEmail(Unconfirmed unconfirmed, int key);
	
	public MimeMessagePreparator buildUserPaswordChangedEmail(User user, String password);

	public MimeMessagePreparator buildUserConfirmedEmail(User user);

}
