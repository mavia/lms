package ukma.baratheons.lms.service;

import java.util.List;

import ukma.baratheons.lms.entity.Task;

/**
 * This interface provides
 * service to search tasks
 * @version 1.0
 * @author Max Oliynick
 * */

public interface SearchTaskProcessor {
	
	/**Describes basic supported search types*/
	public static enum BasicTaskSearch {
		BY_TASK_ID,
		BY_STAFF_FIRST_NAME,
		BY_STAFF_LAST_NAME,
		BY_STAFF_EMAIL,
		BY_CREATOR_FIRST_NAME,
		BY_CREATOR_LAST_NAME,
		BY_CREATOR_EMAIL,
		BY_TYPE, BY_STATUS,
		BY_ROOM_LOC_N,
		BY_EQPT_LOC_N,
		BY_EQPT_ID
	}
	
	/**Custom search filter*/
	public static class Filter {
		
		private BasicTaskSearch searchType;
		private Object value;
		
		/**
		 * Constructs search task processor filter
		 * @param searchType - search type to be used
		 * @param value - search argument
		 * */
		public Filter(final BasicTaskSearch searchType, final Object value) {
			this.searchType = searchType;
			this.value = value;
		}

		public BasicTaskSearch getSearchType() {
			return searchType;
		}

		public void setSearchType(BasicTaskSearch searchType) {
			this.searchType = searchType;
		}

		public Object getValue() {
			return value;
		}

		public void setValue(Object value) {
			this.value = value;
		}
		
	}
	
	/**
	 * Returns list of all tasks using given filters.
	 * Note that search result depends on the user rights in the system
	 * @param rights - user rights
	 * @param filters - list of filters to apply
	 * */
	public List<Task> getTasks(int rights, final List<Filter> filters);

}
