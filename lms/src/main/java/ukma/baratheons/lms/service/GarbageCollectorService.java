package ukma.baratheons.lms.service;

public interface GarbageCollectorService {

	public void deleteUnvisibleTasks();
	
	public void deleteUnconfirmed();
	
	public void deleteUsersOlder();
	
	public void run();
	
}
