package ukma.baratheons.lms.service.ban;

import java.sql.SQLException;

/**
 * Created by pitermiller on 28.11.15.
 */
public interface UnbanUserCron {

    public void checkForUnban() throws SQLException;

}
