package ukma.baratheons.lms.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.controller.TaskController;
import ukma.baratheons.lms.dao.CommentDao;
import ukma.baratheons.lms.dao.TaskDao;
import ukma.baratheons.lms.dao.UnconfirmedDao;
import ukma.baratheons.lms.dao.UserDao;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by user on 09.11.2015.
 */

@Service
public class GarbageCollectorServiceImpl implements GarbageCollectorService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private TaskDao taskDao;

    @Autowired
    private UnconfirmedDao unconfirmedDao;

    @Autowired
    private CommentDao commentDao;

    private Timestamp dateTimeWeekAgo;
    private Timestamp dateTime2YearsAgo;
    private Timestamp date;
    private static final Logger LOGGER = LogManager
			.getLogger(TaskController.class);

    @Override
    public void deleteUnvisibleTasks() {
        try {
			taskDao.deleteUnvisibleTaskByDate(dateTimeWeekAgo);
            LOGGER.info("Delete tasks");
		} catch (SQLException e) {
            LOGGER.error("Failed delete tasks " +
                    " Exception: " + e.toString());
			e.printStackTrace();
		}
    }

    @Override
    public void deleteUnconfirmed() {
        try {
			unconfirmedDao.deleteAfterDate(date);
            LOGGER.info("Delete unconfirmed");
		} catch (SQLException e) {
            LOGGER.error("Failed delete unconfirmed " +
                    " Exception: " + e.toString());
			e.printStackTrace();
		}
    }

    @Override
    public void deleteUsersOlder() {
        try {
			userDao.deleteByLastOnline(dateTime2YearsAgo);
            LOGGER.info("Delete users");
		} catch (SQLException e) {
            LOGGER.error("Failed delete users " +
                    " Exception: " + e.toString());
			e.printStackTrace();
		}
    }


    @Override
    @Scheduled(cron = "0 0 1 * * ?")
    public void run() {
        LOGGER.info("Start garbage collector");
        setDate();
        deleteUnconfirmed();
        deleteComment();
        deleteUnvisibleTasks();
        deleteUsersOlder();
    }

    private void deleteComment() {
		try {
			commentDao.deleteCommentByTask(dateTimeWeekAgo);
            LOGGER.info("Delete comments");
		} catch (SQLException e) {
            LOGGER.error("Failed delete comments " +
                    " Exception: " + e.toString());
			e.printStackTrace();
		}
    }

    private void setDate() {
        Calendar calendar = Calendar.getInstance();
        date = new java.sql.Timestamp(calendar.getTime().getTime());
        calendar.add(Calendar.DATE, -7);
        dateTimeWeekAgo = new java.sql.Timestamp(calendar.getTime().getTime());
        calendar.add(Calendar.YEAR, -2);
        dateTime2YearsAgo = new java.sql.Timestamp(calendar.getTime().getTime());
    }
}
