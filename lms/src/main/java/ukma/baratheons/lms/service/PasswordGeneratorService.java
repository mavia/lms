package ukma.baratheons.lms.service;

import ukma.baratheons.lms.entity.User;

public interface PasswordGeneratorService {

	public String generate();
	
}
