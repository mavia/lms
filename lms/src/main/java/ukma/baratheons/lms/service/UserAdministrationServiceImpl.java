package ukma.baratheons.lms.service;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.anorm.Operator;
import ukma.baratheons.lms.dao.RoleDao;
import ukma.baratheons.lms.dao.UnconfirmedDao;
import ukma.baratheons.lms.dao.UserDao;
import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.entity.Filter;
import ukma.baratheons.lms.entity.Role;
import ukma.baratheons.lms.entity.Unconfirmed;
import ukma.baratheons.lms.query.Table;
import ukma.baratheons.lms.service.ban.UnbanUserCronImpl;
import ukma.baratheons.lms.util.BitUtils;
import ukma.baratheons.lms.util.MD5;

@Service
public class UserAdministrationServiceImpl implements UserAdministrationService{

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UnconfirmedDao unconfirmedDao;
	
	@Autowired
	private RoleDao roleDao;
	
	@Override
	public List<User> getUsers() {
		return userDao.getUsers();
	}
	
	@Override
	public List<User> getUsersForNonChief(){
		return userDao.getUsersForNonChief(StaticConstant.Role.ADMIN);
	}

	@Override
	public List<User> getUsersFiltered(long filters, String chief) {
		List<User> users = new ArrayList<User>();
		
		if(filters==0){
			if(chief !=null){
				users = userDao.getUsers();
			}else{
				users = userDao.getUsersForNonChief(StaticConstant.Role.ADMIN);
			}
			return users;
		}
		if(filters == StaticConstant.Role.ADMIN){
			if(chief !=null){
				users = userDao.getUsersFiltered(filters);
			}
			return users;
		}
		
		users = userDao.getUsersFiltered(filters);
		return users;
	}

	@Override
	public List<Role> getRoles() {
		return roleDao.getAll();
	}
	
	@Override
	public Role getRoleById(long id) {
		return roleDao.get(id);
	}
	
	@Override
	public User getUserByEmail(String email){
		return userDao.getUserByEmail(email);
	}
	
	@Override
	public User getChiefAdmin(){
		
		List<User> users = userDao.getAdmins();
		
		for(User user: users){
			if (BitUtils.isBitSet(1, user.getRights())){
				return user;
			}
		}
		
		return null;
	}
	
	@Override
	public User getUserById(long id){
		return userDao.getUserById(new ukma.baratheons.lms.anorm.Filter(Table.UserTable.TABLE_NAME, Table.UserTable.ID, Operator.EQ, id));
	}
	
	@Override
	public long getUserIdByTask(long taskId){
		return userDao.getUserIdByTask(taskId);
	}
	
	@Override
	public Unconfirmed getUnconfirmedByHash(String hash){
		return unconfirmedDao.getByHash(hash);
	}
	
	@Override
	public void create(User user) throws SQLException {
		
		user.setPassword(MD5.getHash(user.getPassword()));
			userDao.create(user);
	}
	public void createUnconfirmed(Unconfirmed unconfirmed) throws SQLException {
		unconfirmedDao.addNew(unconfirmed);
	}
	public void deleteUnconfirmedByHash(String hash) {
		unconfirmedDao.deleteByHash(hash);
	}

	@Override
	public List<User> getStaffProductivityReport() {
		return userDao.getStaffProductivityInPeriod(null, null);
	}
	
	@Override
	public void update(User user) throws SQLException{
		userDao.update(user);
	}

	@Override
	public void updateBanTime(long id, Timestamp timestamp ) throws SQLException{
		User currentUser = userDao.getUserById(new ukma.baratheons.lms.anorm.Filter(Table.UserTable.TABLE_NAME, Table.UserTable.ID, Operator.EQ, id));
		currentUser.setBanEndTime(timestamp);
		userDao.updateBan(currentUser);
	}

	@Override
	public void unban(long id) throws SQLException{
		User currentUser = userDao.getUserById(new ukma.baratheons.lms.anorm.Filter(Table.UserTable.TABLE_NAME, Table.UserTable.ID, Operator.EQ, id));
		currentUser.setBanEndTime(new Timestamp((new java.util.Date()).getTime()));
		currentUser.setPenaltyScore(0);
		update(currentUser);
		updateBanTime(id, new Timestamp((new java.util.Date()).getTime()));
	}

	@Override
	public List<User> getStaffByActive(boolean b) {
		return userDao.getStaffByActive(b);
	}

	@Override
	public void updateLastOnline(String email) throws SQLException{
		User currentUser = userDao.getUserByEmail(email);
		Date date = new Date();
		currentUser.setLastOnline(new Timestamp(date.getTime()));
		userDao.update(currentUser);
	}

	@Override
	public List<User> getStaff() {
		return userDao.getStaff();
	}
	
}
