package ukma.baratheons.lms.service;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;
import ukma.baratheons.lms.entity.Comment;
import ukma.baratheons.lms.entity.Role;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.Unconfirmed;
import ukma.baratheons.lms.entity.User;

/**
 * This class builds emails using Apache VelocityEngine, which helps to separate
 * display and business logic, and Spring MimeMessagePreparator, which prepare
 * email to be sent by Spring JavaMailSender(configuration for this class can be
 * found in config.xml).
 * 
 * All templates for emails is storing in WEB-INF/classes/emailTemplates
 * directory.
 * 
 * @author Vitalii Mogolivskyi
 */
@Service
public class MailBuilderImpl implements MailBuilder {

	/**
	 * VelocityEngine, which is used to pass model into view template and
	 * receive rendered view, which will be send to appropriate email
	 */
	@Autowired
	private VelocityEngine velocityEngine;

	private static final String HOSTNAME = "localhost:8080";
	private static final String SENDER_EMAIL = "dyhdalo.andriy@gmail.com";

	/**
	 * Builds email according to MIME standard
	 * 
	 * @return MimeMessagePreparator object, which is ready to be send by
	 *         JavaMailSender
	 */
	private MimeMessagePreparator buildEmail(final String receiverEmail, final String subject, final String text) {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
				message.setTo(receiverEmail);
				message.setFrom(SENDER_EMAIL);
				message.setSubject(subject);
				message.setSentDate(new Date());
				message.setText(text, true);
			}
		};
		return preparator;
	}
	
	/**
	 * Builds email which should be sent to user or laborant,
	 * when something has happened with their task 
	 * 
	 * @return MimeMessagePreparator object, which is ready to be send by
	 *         JavaMailSender
	 */
	public MimeMessagePreparator buildTaskStatusChangedEmail(final Task task, final User user) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("hostname", HOSTNAME);
		model.put("user", user);
		model.put("task", task);
		model.put("status", task.getTaskStatus());
		model.put("room", task.getEqpt().getRoom());
		model.put("eqpt", task.getEqpt());
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "emailTemplates/taskStatusChangedEmail.vm",
				"UTF-8", model);
		return buildEmail(user.getEmail(), "Task Status Changed", text);
	}

	/**
	 * Builds email which should be send to user if his/her rights has changed.
	 * 
	 * @param user,
	 *            whose rights has changed.
	 * @return MimeMessagePreparator object, which is ready to be send by
	 *         JavaMailSender
	 */
	@Override
	public MimeMessagePreparator buildUserRightsChangedEmail(final User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MimeMessagePreparator buildTaskCommentAddedEmail(Comment comment) {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * Builds email which should be send to laborant
	 * when somebody reported a problem in room he is responsible for
	 * 
	 * @param user,
	 *            whose rights has changed.
	 * @return MimeMessagePreparator object, which is ready to be send by
	 *         JavaMailSender
	 */
	@Override
	public MimeMessagePreparator buildTaskSubmittedEmail(final Task task) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("hostname", HOSTNAME);
		model.put("staff", task.getStaff());
		model.put("task", task);
		model.put("room", task.getEqpt().getRoom());
		model.put("eqpt", task.getEqpt());
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				"emailTemplates/taskSubmittedEmail.vm", "UTF-8", model);
		return buildEmail(task.getStaff().getEmail(), "Someone has reported about problem in your room", text);
	}

	@Override
	public MimeMessagePreparator buildTaskUrgencyChangedEmail(Task task) {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * Builds email which should be send to chief administrator
	 * when some staff member refused this task
	 * 
	 * @param user,
	 *            whose rights has changed.
	 * @return MimeMessagePreparator object, which is ready to be send by
	 *         JavaMailSender
	 */
	@Override
	public MimeMessagePreparator buildTaskRefusedByStaffEmail(Task task, User staff, User admin) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("hostname", HOSTNAME);
		model.put("staff", staff);
		model.put("task", task);
		model.put("status", task.getTaskStatus());
		model.put("room", task.getEqpt().getRoom());
		model.put("eqpt", task.getEqpt());
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "emailTemplates/taskRefusedByStaff.vm",
				"UTF-8", model);
		return buildEmail(admin.getEmail(), "Task Refused By Staff", text);
	}

	/**
	 * Builds email which should be send to user when administrator invites
	 * her/him to system
	 * 
	 * This email contains special link with hash code, which leads to unique
	 * registration page
	 * 
	 * @return MimeMessagePreparator object, which is ready to be send by
	 *         JavaMailSender
	 */
	@Override
	public MimeMessagePreparator buildRegistrationEmail(final Unconfirmed unUser, final Role role) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("hostname", HOSTNAME);
		model.put("unUser", unUser);
		model.put("role", role);
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				"emailTemplates/registrationEmail.vm", "UTF-8", model);
		return buildEmail(unUser.getEmail(), "Registration link", text);
	}
	/**
	 * Builds email which should be send to email address
	 * which was mentioned in task creation form and
	 * user with such email is not registered in system
	 * or is not unauthorized.
	 * 
	 * This email contains special link with hash code, which leads to unique
	 * task confirmation page
	 * 
	 * @param presentation
	 *            of unregistered in the system
	 * @return MimeMessagePreparator object, which is ready to be send by
	 *         JavaMailSender
	 */
	@Override
	public MimeMessagePreparator buildTaskConfirmationEmail(final Unconfirmed unUser, final long taskId) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("hostname", HOSTNAME);
		model.put("unUser", unUser);
		model.put("taskId", taskId);
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				"emailTemplates/taskConfirmationEmail.vm", "UTF-8", model);
		return buildEmail(unUser.getEmail(), "Task confirmation", text);
	}
	
	/**
	 * Builds email which should be send to email address
	 * which was mentioned in task creation form and
	 * was confirmed but there weren't such user in system before.
	 * 
	 * This email contains auto generated password for new user
	 * 
	 * @param presentation
	 *            of unregistered in the system
	 * @return MimeMessagePreparator object, which is ready to be send by
	 *         JavaMailSender
	 */
	@Override
	public MimeMessagePreparator buildUserConfirmedEmail(final User user) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("hostname", HOSTNAME);
		model.put("password", user.getPassword());
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				"emailTemplates/newUserRegistratedEmail.vm", "UTF-8", model);
		return buildEmail(user.getEmail(), "Email confirmation", text);
	}
	/**
	 * Builds email which should be send to user when his/her has forgotten password
	 * to confirm that it is really his/her request to reset it.
	 * 
	 * @return MimeMessagePreparator object, which is ready to be send by
	 *         JavaMailSender
	 */
	@Override
	public MimeMessagePreparator buildResetPasswordEmail(Unconfirmed unconfirmed, int key) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("hostname", HOSTNAME);
		model.put("unconfirmed", unconfirmed);
		model.put("key", key);
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				"emailTemplates/resetPasswordEmail.vm", "UTF-8", model);
		return buildEmail(unconfirmed.getEmail(), "Password Reset", text);
	}
	/**
	 * Builds email with user new password which is sent after reseting old one.
	 * 
	 * @return MimeMessagePreparator object, which is ready to be send by
	 *         JavaMailSender
	 */
	@Override
	public MimeMessagePreparator buildUserPaswordChangedEmail(User user, String password) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("hostname", HOSTNAME);
		model.put("user", user);
		model.put("password", password);
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
				"emailTemplates/userPasswordChangedEmail.vm", "UTF-8", model);
		return buildEmail(user.getEmail(), "New password", text);
	}
}
