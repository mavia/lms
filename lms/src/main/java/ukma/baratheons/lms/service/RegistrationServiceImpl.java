package ukma.baratheons.lms.service;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.entity.ConfirmActionType;
import ukma.baratheons.lms.entity.Role;
import ukma.baratheons.lms.entity.Unconfirmed;
import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.util.Time;

@Service
public class RegistrationServiceImpl implements RegistrationService{

	@Autowired
	private UserAdministrationService userService;
	
	@Autowired
	private ConfirmationService confirmationService;
	
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private MailBuilderImpl mailBuilder;
	
	private final static long millisForAWeek = 604800000;

	@Override
	public void createUnconfirmed(String email, Role role) throws SQLException {
		Unconfirmed unUser;
		unUser = confirmationService
				.createUnconfirmed(email, ConfirmActionType.registration_user
				, new java.sql.Date((new Date(millisForAWeek+System.currentTimeMillis())).getTime())
				, role.getId());
		mailSender.send(mailBuilder.buildRegistrationEmail(unUser, role));
		userService.createUnconfirmed(unUser);
	}

	@Override
	public void registrateNewUser(String email, String password, String firstName, String lastName, int role) {
		User newUser = new User();
		newUser.setEmail(email);
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);
		newUser.setActive(true);
		newUser.setPassword(password);
		newUser.setRole(userService.getRoleById(role));
		newUser.setRights(userService.getRoleById(role).getDefRights());
		java.util.Date date = new java.util.Date();
		newUser.setRegDate(new java.sql.Date(date.getTime()));
		newUser.setPenaltyScore(0);
		newUser.setReceiveEmail(true);
		try {
			userService.create(newUser);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
