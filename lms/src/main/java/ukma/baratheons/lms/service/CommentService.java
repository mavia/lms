package ukma.baratheons.lms.service;

import java.sql.SQLException;
import java.util.List;

import ukma.baratheons.lms.entity.Comment;
import ukma.baratheons.lms.entity.User;

public interface CommentService {

	public List<Comment>getCommentsByEqpt(long id);
	
	public List<Comment>getCommentsByTask(long taskId);
	
	public void addNew(long taskId, User user, String text) throws SQLException;
	
}
