package ukma.baratheons.lms.service;

import java.util.List;

import org.aspectj.lang.annotation.AfterReturning;
import org.springframework.mail.javamail.MimeMessagePreparator;

import ukma.baratheons.lms.entity.Log;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.Unconfirmed;
import ukma.baratheons.lms.entity.User;


public interface NotificationService {

	public void send(MimeMessagePreparator email);

	public void send(List<MimeMessagePreparator> emails);
	
	public void sentNewTaskSubmittedEmail(Task task);

	public List<Log> getLogs(User user);

	public void sendTaskConfirmationEmail(Unconfirmed unTask, long commentTaskId);
	
	public void sendResetPasswordEmail(Unconfirmed unc, int key);
	
	public void sendUserPaswordChangedEmail(User user, String password);

	public void sendUserConfirmedEmail(User user);

	public void sendTaskStatusChangedEmail(Task task, User user);
	
	public void sendTaskRefusedByStaffEmail(Task task, User staff, User admin);
}
