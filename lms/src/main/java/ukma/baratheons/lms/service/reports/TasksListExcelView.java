package ukma.baratheons.lms.service.reports;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;
import ukma.baratheons.lms.entity.Task;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class TasksListExcelView extends AbstractExcelView {

    @Override
    protected void buildExcelDocument(Map model, HSSFWorkbook workbook,
                                      HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HSSFSheet excelSheet = workbook.createSheet("Tasks");
        setExcelHeader(excelSheet);


        List<Task> taskList = (List<Task>) model.get("taskList");
        setExcelRows(excelSheet,taskList);

    }

    public void setExcelHeader(HSSFSheet excelSheet) {
        HSSFRow excelHeader = excelSheet.createRow(0);
        excelHeader.createCell(0).setCellValue("Id");
        excelHeader.createCell(1).setCellValue("ParentId");
        excelHeader.createCell(2).setCellValue("TaskStatusId");
        excelHeader.createCell(3).setCellValue("TaskTypeId");
        excelHeader.createCell(4).setCellValue("EquipmentId");
        excelHeader.createCell(5).setCellValue("UserId");
        excelHeader.createCell(6).setCellValue("StaffId");
        excelHeader.createCell(7).setCellValue("CreationTime");
        excelHeader.createCell(8).setCellValue("CompletionTime");
        excelSheet.autoSizeColumn(0);
        excelSheet.autoSizeColumn(1);
        excelSheet.autoSizeColumn(2);
        excelSheet.autoSizeColumn(3);
        excelSheet.autoSizeColumn(4);
        excelSheet.autoSizeColumn(5);
        excelSheet.autoSizeColumn(6);
        excelSheet.autoSizeColumn(7);
        excelSheet.autoSizeColumn(8);
    }

    public void setExcelRows(HSSFSheet excelSheet, List<Task> taskList){
        int record = 1;
        for (Task task : taskList) {
            HSSFRow excelRow = excelSheet.createRow(record++);
            excelRow.createCell(0).setCellValue(task.getId());
            if(task.getParentId()!=null)
                excelRow.createCell(1).setCellValue(task.getParentId());
            excelRow.createCell(2).setCellValue(task.getTaskStatus().getName());
            excelRow.createCell(3).setCellValue(task.getTaskType().getName());
            excelRow.createCell(4).setCellValue(task.getEqpt().getLocalNumber());
            excelRow.createCell(5).setCellValue(task.getUser().getFirstName());
            excelRow.createCell(6).setCellValue(task.getStaff().getFirstName());
            excelRow.createCell(7).setCellValue(task.getCreationTime().toString().substring(0,16));
            if(task.getCompletionTime()!=null)
                excelRow.createCell(8).setCellValue(task.getCompletionTime().toString());

            excelSheet.autoSizeColumn(0);
            excelSheet.autoSizeColumn(1);
            excelSheet.autoSizeColumn(2);
            excelSheet.autoSizeColumn(3);
            excelSheet.autoSizeColumn(4);
            excelSheet.autoSizeColumn(5);
            excelSheet.autoSizeColumn(6);
            excelSheet.autoSizeColumn(7);
            excelSheet.autoSizeColumn(8);
        }
    }
}
