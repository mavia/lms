package ukma.baratheons.lms.service.ban;

import java.sql.SQLException;

/**
 * Created by pitermiller on 23.11.15.
 */
public interface BanUser {

    public void banUserById(Long id, String time) throws SQLException;
    public void banUserById(Long id, Integer time) throws SQLException;
    public void increasePenaltyScoreAndBanTime(Long id) throws SQLException;

}
