package ukma.baratheons.lms.service;

import java.util.List;
import java.util.regex.Pattern;

/**
 * raw interface for task search by users input!
 * */
public abstract class CachedInputSearchProcessor<R> {
	
	protected static final Pattern SPLITTER = Pattern.compile("[\\s*](?=[\\'\\[\\\"])|(?<=[\\'\\]\\\"])[\\s*]");
	
	public static String [] splitInput(final String searchQuery) {
		return searchQuery.split(SPLITTER.toString());
	}
	
	public abstract List<R> processInput(final String searchQuery);
	
	public abstract List<R> processInput(final String[] searchQueryArgs);

	public abstract List<R> getCache(final String[] searchQueryArgs);
}
