package ukma.baratheons.lms.service;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.entity.Filter;
import ukma.baratheons.lms.entity.Role;
import ukma.baratheons.lms.entity.Unconfirmed;

public interface UserAdministrationService {
	
	public List<User> getUsers();
	
	public List<User> getUsersForNonChief();

	public List<User> getUsersFiltered(long filters, String chief);
	
	public List<Role> getRoles();
	
	public Role getRoleById(long id);
	
	public List<User> getStaff();
	
	public User getUserByEmail(String email);
	
	public User getChiefAdmin();
	
	public User getUserById(long id);
	
	public long getUserIdByTask(long taskId);
	
	public Unconfirmed getUnconfirmedByHash(String hash);
	
	public void deleteUnconfirmedByHash(String hash);

	public void create(User user) throws SQLException;
	
	public void createUnconfirmed(Unconfirmed unconfirmed) throws SQLException;

	public List<User> getStaffProductivityReport();

	public void update(User user) throws SQLException;
	
	public void updateLastOnline(String email) throws SQLException;

	public void updateBanTime(long id, Timestamp timestamp ) throws SQLException;

	public void unban(long id) throws SQLException;

	public List<User> getStaffByActive(boolean b);

}
