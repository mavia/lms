package ukma.baratheons.lms.service;

import java.sql.SQLException;

import ukma.baratheons.lms.entity.Role;
import ukma.baratheons.lms.entity.Unconfirmed;

public interface RegistrationService {

	public void createUnconfirmed(String email, Role role) throws SQLException;

	public void registrateNewUser(String email, String password, String firstName, String lastName, int role);
}
