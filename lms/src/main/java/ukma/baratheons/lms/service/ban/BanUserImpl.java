package ukma.baratheons.lms.service.ban;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.service.UserAdministrationService;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by pitermiller on 23.11.15.
 */
@Service
public class BanUserImpl implements BanUser {

    @Autowired
    UserAdministrationService userAdministrationService;

    @Override
    public void banUserById(Long id, String time) throws SQLException {

        Timestamp res=null;
        User user = userAdministrationService.getUserById(id);
        res = new Timestamp((new java.util.Date()).getTime());

        Calendar cal = Calendar.getInstance();
        cal.setTime(res);
        switch (time){
            case "hour":
                cal.add(Calendar.HOUR, 1);
                break;

            case "day":
                cal.add(Calendar.DAY_OF_WEEK, 1);
                break;

            case "week":
                cal.add(Calendar.DAY_OF_WEEK, 7);
                break;

            case "month":
                cal.add(Calendar.MONTH,1);
                break;

            case "year":
                cal.add(Calendar.YEAR, 1);
                break;

        }

        res.setTime(cal.getTime().getTime());

        userAdministrationService.updateBanTime(id,res);
    }

    @Override
    public void banUserById(Long id, Integer time) throws SQLException {
        switch (time){
            case 1:
                banUserById(id,"hour");
                break;

            case 2:
                banUserById(id, "day");
                break;

            case 3:
                banUserById(id, "week");
                break;

            case 4:
                banUserById(id, "month");
                break;

            case 5:
                banUserById(id, "year");
                break;
        }
    }

    @Override
    public void increasePenaltyScoreAndBanTime(Long id) throws SQLException {
        User user = userAdministrationService.getUserById(id);
        if (user.getPenaltyScore()<5) {
            user.setPenaltyScore(user.getPenaltyScore() + 1);
            userAdministrationService.update(user);
        }
        banUserById(id, user.getPenaltyScore());
    }
}
