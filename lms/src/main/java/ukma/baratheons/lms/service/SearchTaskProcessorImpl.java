package ukma.baratheons.lms.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import ukma.baratheons.lms.dao.TaskDao;
import ukma.baratheons.lms.entity.Equipment;
import ukma.baratheons.lms.entity.Room;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.TaskStatus;
import ukma.baratheons.lms.entity.TaskType;
import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.query.Join;
import ukma.baratheons.lms.query.JoinBuilder;
import ukma.baratheons.lms.query.Logical;
import ukma.baratheons.lms.query.Logical.Operator;
import ukma.baratheons.lms.query.SelectQuery;
import ukma.baratheons.lms.query.SelectQueryBuilder;
import ukma.baratheons.lms.query.Table;
import ukma.baratheons.lms.query.Table.EquipmentTable;
import ukma.baratheons.lms.query.Table.RoomTable;
import ukma.baratheons.lms.query.Table.TaskStatusTable;
import ukma.baratheons.lms.query.Table.TaskTable;
import ukma.baratheons.lms.query.Table.TaskTypeTable;
import ukma.baratheons.lms.query.Table.UserTable;
import ukma.baratheons.lms.query.Where;
import ukma.baratheons.lms.query.WhereBuilder;

/**
 * This class is a simple implementation
 * of {@linkplain SearchTaskProcessor} interfase
 * @see {@linkplain SearchTaskProcessor}
 * @author Max Oliynick
 * */

@Service
public class SearchTaskProcessorImpl implements SearchTaskProcessor {
	
	/**Holds basic search sql query*/
	private static final SelectQuery BASE_SQL;
	
	/**Default mapper for base sql search query*/
	private static final SearchTaskMapper DEFAULT_MAPPER = new SearchTaskMapper();
	
	@Autowired
	private TaskDao taskDao;
	
	static {
		
		/*Selects all information about task
		 * t1 is task table
		 * t2 is staff
		 * t3 is creator
		 * t4 is task type
		 * t5 is task status
		 * t6 is equipment
		 * t7 is room
		 * */
		BASE_SQL = SelectQueryBuilder.selectQuery(
                "t1." + Table.TaskTable.ID, "t1." + Table.TaskTable.STATUS,
                "t1." + Table.TaskTable.TYPE, "t1." + Table.TaskTable.EQUIPMENT,
                "t1." + Table.TaskTable.CREATOR , "t1." + Table.TaskTable.STAFF,
                "t1." + Table.TaskTable.CREATION_TIME, "t1." + Table.TaskTable.COMPLETION_TIME,
                "t1." + Table.TaskTable.VISIBILITY, "t4.*", "t5.*", "t6.*", "t7.*"
        );
        
        BASE_SQL.column("t2." + Table.UserTable.FIRST_NAME, "t_staff_fn");
        BASE_SQL.column("t2." + Table.UserTable.LAST_NAME, "t_staff_ln");
        BASE_SQL.column("t2." + Table.UserTable.EMAIL, "t_staff_email");
        
        BASE_SQL.column("t3." + Table.UserTable.FIRST_NAME, "t_user_fn");
        BASE_SQL.column("t3." + Table.UserTable.LAST_NAME, "t_user_ln");
        BASE_SQL.column("t3." + Table.UserTable.EMAIL, "t_user_email");
        BASE_SQL.column("t5." + Table.TaskStatusTable.NAME, "t_stat_name");
        BASE_SQL.column("t4." + Table.TaskTypeTable.NAME, "t_type_name");
        
        Join joinStaff = JoinBuilder.innerJoin(
        		Table.UserTable.TABLE_NAME, "t2",
        		"t1." + Table.TaskTable.STAFF, Logical.Operator.EQ, "t2." + Table.UserTable.ID
        		);
        
        Join joinUser = JoinBuilder.innerJoin(
        		Table.UserTable.TABLE_NAME, "t3", 
        		"t1." + Table.TaskTable.CREATOR, Logical.Operator.EQ, "t3." + Table.UserTable.ID);
        
        Join joinTaskType = JoinBuilder.innerJoin(
        		Table.TaskTypeTable.TABLE_NAME, "t4",
        		"t1." + Table.TaskTable.TYPE, Logical.Operator.EQ, "t4." + Table.TaskTypeTable.ID);
        
        Join joinTaskStatus = JoinBuilder.innerJoin(
        		Table.TaskStatusTable.TABLE_NAME, "t5",
        		"t1." + Table.TaskTable.STATUS, Logical.Operator.EQ, "t5." + Table.TaskStatusTable.ID);
        
        Join joinEquipment = JoinBuilder.innerJoin(
        		Table.EquipmentTable.TABLE_NAME, "t6",
        		"t1." + Table.TaskTable.EQUIPMENT, Operator.EQ, "t6." + Table.EquipmentTable.ID);
        
        Join joinRoom = JoinBuilder.innerJoin(
        		Table.RoomTable.TABLE_NAME, "t7",
        		"t7." + Table.RoomTable.ID, Operator.EQ, "t6." + Table.EquipmentTable.ROOM).
        		andOn("t1." + Table.TaskTable.EQUIPMENT, Operator.EQ, "t6." + Table.EquipmentTable.ID);
        
        BASE_SQL.from(Table.TaskTable.TABLE_NAME, "t1").
        join(joinStaff).
        join(joinUser).
        join(joinTaskType).
        join(joinTaskStatus).
        join(joinEquipment).
        join(joinRoom);
        
	}
	
	/**
	 * Holds prepared search query attributes such as
	 * sql query and its arguments
	 * */
	private static class SearchQueryHolder {
		private final Object [] args;
		private final String sql;
		
		/**
		 * Constructs SearchQueryHolder object using given parameters
		 * @see {@linkplain SearchQueryHolder}
		 * @param sql - prepared sql
		 * @param args - given arguments
		 * */
		public SearchQueryHolder(final String sql, final Object [] args) {
			this.sql = sql;
			this.args = args;
		}

		public Object[] getArgs() {
			return args;
		}

		public String getSql() {
			return sql;
		}
		
	}
	
	/**
	 * Custom search query mapper implementation.
	 * */
	public static class SearchTaskMapper implements RowMapper<List<Task>> {

		@Override
		public List<Task> mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			List<Task> result = new ArrayList<>();
			
			while(rs.next()) {
				
				Task task = new Task();
				task.setId(rs.getLong(TaskTable.ID));
				
				RowMapper<TaskStatus> subMapper = new RowMapper<TaskStatus>() {

					@Override
					public TaskStatus mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						TaskStatus taskStatus = new TaskStatus();
						taskStatus.setId(rs.getInt(TaskStatusTable.ID));
						taskStatus.setName(rs.getString("t_stat_name"));
						return taskStatus;

					}
				};
				
				task.setTaskStatus(subMapper.mapRow(rs, -1));
				
				RowMapper<TaskType> taskTypeMapper = new RowMapper<TaskType>() {

					@Override
					public TaskType mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						TaskType taskType = new TaskType();
						taskType.setId(rs.getInt(TaskTypeTable.ID));
						taskType.setName(rs.getString("t_type_name"));
						return taskType;

					}
				};
				task.setTaskType(taskTypeMapper.mapRow(rs, -1));
				
				RowMapper<Equipment> equipmentMapper = new RowMapper<Equipment>() {
					
					@Override
					public Equipment mapRow(ResultSet rs, int rowNum) throws SQLException {
						
						Equipment equipment = new Equipment();
						equipment.setId(rs.getLong(EquipmentTable.ID));
						equipment.setLocalNumber(rs.getString(EquipmentTable.LOCAL_NAME));
						
						Room room = new Room();
						room.setId(rs.getLong(RoomTable.ID));
						room.setNumber(rs.getString(RoomTable.NUMBER));
						
						equipment.setRoom(room);
						
						return equipment;
					}
				};
				
				task.setEqpt(equipmentMapper.mapRow(rs, -1));
				
				User staff = new User();
				staff.setId(rs.getLong(TaskTable.STAFF));
				staff.setFirstName(rs.getString("t_staff_fn"));
				staff.setLastName(rs.getString("t_staff_ln"));
				staff.setEmail(rs.getString("t_staff_email"));
				
				User user = new User();
				user.setId(rs.getLong(TaskTable.CREATOR));
				user.setFirstName(rs.getString("t_user_fn"));
				user.setLastName(rs.getString("t_user_ln"));
				user.setEmail(rs.getString("t_user_email"));
				
				task.setStaff(staff);
				task.setUser(user);
				
				task.setCreationTime(rs.getTimestamp(TaskTable.CREATION_TIME));
				task.setCompletionTime(rs.getDate(TaskTable.COMPLETION_TIME));
				task.setVisibility(rs.getBoolean(TaskTable.VISIBILITY));
				
				result.add(task);
			}
			
			return result;
		}
		
	}
	
	@Override
	public List<Task> getTasks(int rights, final List<Filter> filters) {
		
		if(filters == null)
			throw new NullPointerException("filters == null");
		
		final SearchQueryHolder qh = doPrepareSql(rights, filters);
		
		System.out.println(qh.getSql());
		
		return taskDao.queryRaw(qh.getSql(), DEFAULT_MAPPER, qh.getArgs());
	}
	
	private SearchQueryHolder doPrepareSql(int rights, final List<Filter> filters) {
		
		SelectQuery select = new SelectQuery(BASE_SQL);
		Where where = WhereBuilder.where();
		
		Object args[] = new Object[filters.size()];
		int i = 0;
		
		// leaves only email filters
		/*final List<Filter> emails = filters.stream().
			    filter(p -> p.getSearchType() == BasicTaskSearch.BY_STAFF_EMAIL 
			             || p.getSearchType() == BasicTaskSearch.BY_CREATOR_EMAIL).
			    collect(Collectors.toList());
		
		// removes from src filter list filtered list
		filters.removeAll(emails);*/
		
		// builds sql
		/*for(final Filter filter : emails) {
			
			if(filter.getSearchType() == BasicTaskSearch.BY_STAFF_EMAIL) {
				
				where.orLike("t2." + UserTable.EMAIL, '?');
				args[i++] = prepareWildCard(filter.getValue());
				
			} else {
				
				where.orLike("t3." + UserTable.EMAIL, '?');
				args[i++] = prepareWildCard(filter.getValue());
				
			}
		}*/
		
		/*final List<Filter> fullNames = filters.stream().
			    filter(p -> p.getSearchType() == BasicTaskSearch.BY_STAFF_FIRST_NAME 
			             || p.getSearchType() == BasicTaskSearch.BY_STAFF_LAST_NAME
			             || p.getSearchType() == BasicTaskSearch.BY_CREATOR_FIRST_NAME
			             || p.getSearchType() == BasicTaskSearch.BY_CREATOR_LAST_NAME).
			    collect(Collectors.toList());
		
		filters.removeAll(fullNames);
		
		for(final Filter filter : fullNames) {
			
			if(filter.getSearchType() == BasicTaskSearch.BY_STAFF_FIRST_NAME) {
				
				where.orLike("CAST (t2." + UserTable.FIRST_NAME + " AS TEXT)", '?');
				args[i++] = prepareWildCard(filter.getValue());
				
			} else if(filter.getSearchType() == BasicTaskSearch.BY_STAFF_LAST_NAME) {
				
				where.orLike("CAST (t2." + UserTable.LAST_NAME + " AS TEXT)", '?');
				args[i++] = prepareWildCard(filter.getValue());
				
			}  else if(filter.getSearchType() == BasicTaskSearch.BY_CREATOR_FIRST_NAME) {
				
				where.orLike("CAST (t3." + UserTable.FIRST_NAME + " AS TEXT)", '?');
				args[i++] = prepareWildCard(filter.getValue());
				
			} else if(filter.getSearchType() == BasicTaskSearch.BY_CREATOR_LAST_NAME) {
				
				where.orLike("CAST (t3." + UserTable.LAST_NAME + " AS TEXT)", '?');
				args[i++] = prepareWildCard(filter.getValue());
				
			}
		}*/
		
		/*final List<Filter> byIds = filters.stream().
			    filter(p -> p.getSearchType() == BasicTaskSearch.BY_TASK_ID 
			             || p.getSearchType() == BasicTaskSearch.BY_EQPT_ID).
			    collect(Collectors.toList());
		
		filters.removeAll(byIds);
		
		for(final Filter filter : byIds) {
			
			if(filter.getSearchType() == BasicTaskSearch.BY_TASK_ID) {
				
				where.or("CAST (t1." + TaskTable.ID + " AS TEXT)", Operator.EQ, '?');
				args[i++] = filter.getValue();
				
			} else if(filter.getSearchType() == BasicTaskSearch.BY_EQPT_ID) {
				
				where.orLike("CAST (t6." + EquipmentTable.ID + " AS TEXT)", '?');
				args[i++] = prepareWildCard(filter.getValue());
			}
		}*/
		
		/*final List<Filter> locName = filters.stream().
			    filter(p -> p.getSearchType() == BasicTaskSearch.BY_ROOM_LOC_N 
			             || p.getSearchType() == BasicTaskSearch.BY_EQPT_LOC_N).
			    collect(Collectors.toList());
		
		filters.removeAll(locName);
		
		for(final Filter filter : locName) {
			
			if(filter.getSearchType() == BasicTaskSearch.BY_ROOM_LOC_N) {
				
				where.orLike("CAST (t7." + RoomTable.NUMBER + " AS TEXT)", '?');
				args[i++] = prepareWildCard(filter.getValue());
				
			} else if(filter.getSearchType() == BasicTaskSearch.BY_EQPT_LOC_N) {
				
				where.orLike("CAST (t6." + EquipmentTable.LOCAL_NAME + " AS TEXT)", '?');
				args[i++] = prepareWildCard(filter.getValue());
				
			}
		}
				*/
		for(final Filter filter : filters) {
			
			Assert.notNull(filter.getSearchType(), "Filtered field cannot be null value");
			Assert.notNull(filter.getValue(), "Filtered field cannot be null value");
			
			if(filter.getSearchType() == BasicTaskSearch.BY_TYPE) {
				
				where.andLike("CAST (t4." + TaskTypeTable.NAME + " AS TEXT)", '?');
				args[i++] = prepareWildCard(filter.getValue());
				
			} else if(filter.getSearchType() == BasicTaskSearch.BY_STATUS) {
				
				where.andLike("CAST (t5." + TaskStatusTable.NAME + " AS TEXT)", '?');
				args[i++] = prepareWildCard(filter.getValue());
				
			} 
		}
		
		select.where(where);
		
		return new SearchQueryHolder(select.toSql(), args);
	}
	
	private String prepareWildCard(final Object arg) {
		StringBuilder sb = new StringBuilder(String.valueOf(arg).length() + 2);
		return sb.append('%').append(String.valueOf(arg)).append('%').toString();
	}
	
}
