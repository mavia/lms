package ukma.baratheons.lms.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import ukma.baratheons.lms.dao.TaskStatusDao;
import ukma.baratheons.lms.dao.TaskTypeDao;
import ukma.baratheons.lms.service.SearchTaskProcessor.BasicTaskSearch;
import ukma.baratheons.lms.service.SearchTaskProcessor.Filter;
import ukma.baratheons.lms.util.EmailUtils;
import ukma.baratheons.lms.util.LevenshteinDistance;

@Component
public final class InputTaskSearchProcessorImpl extends CachedInputSearchProcessor<Filter> {
	
	private static double matchCoeff = .61;
	
	public static final Pattern ALL_DIGITS = Pattern.compile("^[0-9]+$");
	public static final Pattern HAS_DIGITS = Pattern.compile("(.*)[0-9](.*)");
	
	private boolean cacheEmailEnabled = true;
	
	private boolean cacheQueryEnabled = true;
	
	private int emailCacheSize = 1000;
	
	private int queryCacheSize = 1000;
	
	private Set<String> cachedEmails = null;
	private Map<String [], List<Filter>> cachedQueries = null;
	
	@Autowired
	private TaskTypeDao taskTypeDao;
	
	@Autowired
	private TaskStatusDao taskStatusDao;
	
	private List<String> taskTypes;
	private List<String> taskStatus;
	
	/**
	 * Tries to reload and refill cash collections
	 * */
	public void reload() {
		
		if(taskTypes == null)
			taskTypes = new ArrayList<String>();
		
		if(taskStatus == null)
			taskStatus = new ArrayList<String>();
		
		//taskTypeDao.getAll().forEach(item -> taskTypes.add(item.getName()));
		//taskStatusDao.getAll().forEach(item -> taskStatus.add(item.getName()));
	}
	
	public List<Filter> processInput(final String[] searchQueryArgs) {
		if(searchQueryArgs == null)
			throw new IllegalArgumentException("searchQueryArgs == null");
		
		List<Filter> cache = getCache(searchQueryArgs);
		
		if(cache != null) return cache;
		
		final List<Filter> filters = new ArrayList<Filter>(searchQueryArgs.length);
		
		if(taskTypes == null | taskStatus == null) {
			reload();
		}
		
		Assert.notNull(taskTypes);
		Assert.notNull(taskStatus);
		
		String taskTypeNames [] = taskTypes.toArray(new String[taskTypes.size()]);
		String taskStatusNames [] = taskStatus.toArray(new String[taskStatus.size()]);
		
		boolean isTaskType = false;
		boolean isTaskStatus = false;
		
		for(final String input : searchQueryArgs) {//add other cases
			
			isTaskType = matchesAny(input, taskTypeNames);
			isTaskStatus = matchesAny(input, taskStatusNames);
			
			// supposes input string is email address
			if(assumeEmail(input)) {
				
				filters.add(new Filter(BasicTaskSearch.BY_STAFF_EMAIL, input));
				filters.add(new Filter(BasicTaskSearch.BY_CREATOR_EMAIL, input));
				
			} else if(isTaskType || isTaskStatus) {
				
				if(isTaskType) {// supposes input string is task type name
					filters.add(new Filter(BasicTaskSearch.BY_TYPE, bestMatch(input, taskTypeNames)));
				}
				
				if(isTaskStatus) {// supposes input string is task status
					filters.add(new Filter(BasicTaskSearch.BY_STATUS, bestMatch(input, taskStatusNames)));
				}
				
			} else {// supposes input is about room, equipment or task
					
				if(assumePrincipal(input)) {
					
					filters.add(new Filter(BasicTaskSearch.BY_CREATOR_FIRST_NAME, input));
					filters.add(new Filter(BasicTaskSearch.BY_CREATOR_LAST_NAME, input));
					
					filters.add(new Filter(BasicTaskSearch.BY_STAFF_FIRST_NAME, input));
					filters.add(new Filter(BasicTaskSearch.BY_STAFF_LAST_NAME, input));
					
				} else if(assumeNumID(input)) {
					
					filters.add(new Filter(BasicTaskSearch.BY_TASK_ID, input));
					filters.add(new Filter(BasicTaskSearch.BY_EQPT_ID, input));
				}
				
				filters.add(new Filter(BasicTaskSearch.BY_ROOM_LOC_N, input));
				filters.add(new Filter(BasicTaskSearch.BY_EQPT_LOC_N, input));
				
			}
			
		}
		
		if(isCacheQueryEnabled()) {
			getCachedQueries().put(searchQueryArgs, filters);
		}
		
		return filters;
	}

	@Override
	public List<Filter> processInput(final String searchQuery) {
		return processInput(CachedInputSearchProcessor.splitInput(searchQuery));
	}
	
	/**
	 * Returns true if input string is name or surname
	 * @param input - given string
	 * */
	private boolean assumePrincipal(final String input) {
	
		for(final String email : EmailUtils.EMAILS) {
			if(input.toLowerCase().contains(email.toLowerCase())) return false;
		}
		
		if(isCacheEmailEnabled()) {
			for(final String cachedEmail : getCachedEmails()) {
				if(input.toLowerCase().contains(cachedEmail.toLowerCase())) return false;
			}
		}
		
		return !input.matches(HAS_DIGITS.toString());
	}
	
	/**
	 * Returns true if input string is number or set of numbers
	 * @param input - given string
	 * */
	private boolean assumeNumID(final String input) {
		return input.matches(ALL_DIGITS.toString());
	}
	
	/**
	 * Returns true if input string is email
	 * @param input - given string
	 * */
	private boolean assumeEmail(final String input) {
		
		boolean result = EmailUtils.isValidEmail(input);
		
		if(result && isCacheEmailEnabled()) {
			getCachedEmails().add(input.substring(input.indexOf('@')).toLowerCase());
		}
		
		return result;
	}
	
	/**
	 * Analyzes input string and returns boolean value which describes if any 
	 * string from array matches input string
	 * @param input - string to analyze
	 * @param toCmp - strings to compare
	 * */
	private boolean matchesAny(final String input, final String [] toCmp) {
		return matchesAny(input, toCmp, getMinMatchCoeff());
	}
	
	/**
	 * Analyzes input string and returns boolean value which describes if any 
	 * string from array matches input string
	 * @param input - string to analyze
	 * @param toCmp - strings to compare
	 * @param minMatch - minimal match coefficient. Should be greater than 0 and less than 1
	 * */
	private boolean matchesAny(final String input, final String [] toCmp, double minMatch) {
		
		double matchCoeff = -1;
		
		for(final String cmp : toCmp) {
			
			Assert.notNull(cmp, "compare str is null");
			
			if(input.equalsIgnoreCase(cmp)) {
				return true;
			}
			
			matchCoeff = LevenshteinDistance.editdist(cmp.toLowerCase(), input.toLowerCase());
			matchCoeff = (input.length() - matchCoeff) / input.length();
			
			if(matchCoeff >= minMatch) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns string which best matches to the array of input strings
	 * @param input - string to analyze
	 * @param toCmp - strings to compare
	 * */
	private String bestMatch(final String input, final String [] toCmp) {
		return bestMatch(input, toCmp, getMinMatchCoeff());
	}
	
	/**
	 * Returns string which best matches to the array of input strings
	 * @param input - string to analyze
	 * @param toCmp - strings to compare
	 * @param minMatch - minimal match coefficient. Should be greater than 0 and less than 1
	 * */
	private String bestMatch(final String input, final String [] toCmp, double minMatch) {
		
		if(input == null)
			throw new IllegalArgumentException("input == null");
		
		if(toCmp == null)
			throw new IllegalArgumentException("toCmp == null");
		
		if(toCmp.length == 0)
			throw new IllegalArgumentException("emty to compare array");
		
		double bestCoeff = -1.0;
		final StringBuilder bestMatch = new StringBuilder("");
		
		for(final String cmp : toCmp) {
			
			if(input.equalsIgnoreCase(cmp)) return cmp;
			
			if(minMatch >= 1.0) continue;
			
			double currCoeff = LevenshteinDistance.editdist(cmp.toLowerCase(), input.toLowerCase());

			currCoeff = (input.length() - currCoeff) / input.length();
			
			if(bestCoeff <= minMatch 
					&& (bestCoeff < 0 || currCoeff > bestCoeff)) {
				
				bestCoeff = currCoeff;
				bestMatch.setLength(0);
				bestMatch.append(cmp);
			}
		}
		
		return bestMatch.length() == 0 ? null : bestMatch.toString();
	}
	
	/**
	 * Returns boolean value which describes if system 
	 * has done the same query before
	 * */
	@Override
	public List<Filter> getCache(final String[] args) {
		
		if(!isCacheQueryEnabled()) return null;
		
		final Set<String[]> temp = getCachedQueries().keySet();
		final String queries [] = temp.toArray(new String[temp.size()]);
		
		for(String[] cache : cachedQueries.keySet()) {
			if(Arrays.equals(args, queries)) {
				return cachedQueries.get(cache);
			}
		}
		
		return null;
	}
	
	public static void setMinMatchCoeff(double coeff) {
		matchCoeff = coeff;
	}
	
	public static double getMinMatchCoeff() {
		return matchCoeff;
	}
	
	private Set<String> getCachedEmails() {
		
		if(isCacheEmailEnabled()) {
			cachedEmails = new HashSet<String>(emailCacheSize);
		}
		
		return cachedEmails;
	}
	
	private Map<String[], List<Filter>> getCachedQueries() {
		
		if(isCacheQueryEnabled()) {
			cachedQueries = new HashMap<String [], List<Filter>>(queryCacheSize);
		}
		
		return cachedQueries;
	}

	
	public boolean isCacheEmailEnabled() {
		return cacheEmailEnabled;
	}

	public void setCacheEmailEnabled(boolean cacheEmailEnabled) {
		this.cacheEmailEnabled = cacheEmailEnabled;
	}

	public boolean isCacheQueryEnabled() {
		return cacheQueryEnabled;
	}

	public void setCacheQueryEnabled(boolean cacheQueryEnabled) {
		this.cacheQueryEnabled = cacheQueryEnabled;
	}

	public int getEmailCacheSize() {
		return emailCacheSize;
	}

	public void setEmailCacheSize(int emailCacheSize) {
		this.emailCacheSize = emailCacheSize;
	}

}
