package ukma.baratheons.lms.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ukma.baratheons.lms.anorm.Operator;
import ukma.baratheons.lms.dao.*;
import ukma.baratheons.lms.entity.*;
import ukma.baratheons.lms.query.Table;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by user on 12.11.2015.
 */

@Aspect
@Service
public class TaskManagementServiceImpl implements TaskManagementService {

	@Autowired
	private TaskDao taskDao;

	@Autowired
	private CommentDao commentDao;

	@Autowired
	private RoomDao roomDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private EquipmentDao eqptDao;

	@Autowired
	private PasswordGeneratorService passServie;

	@Autowired
	private TaskTypeDao taskTypeDao;

	@Autowired
	private TaskStatusDao taskStatusDao;

	@Autowired
	private UnconfirmedDao unconfirmedDao;

	@Autowired
	private ConfirmationService confirmationService;

	@Autowired
	private UserAdministrationService userAdministrationService;

	@Autowired
	private NotificationService notificationService;
	private static final Logger LOGGER = LogManager
			.getLogger(TaskManagementServiceImpl.class);

	private Timestamp dateTime;
	private Date expDate;
	private Date currDate;

	private void setTime() {
		Calendar calendar = Calendar.getInstance();
		this.dateTime = new java.sql.Timestamp(calendar.getTime().getTime());
		this.currDate = new java.sql.Date(calendar.getTime().getTime());
		calendar.add(Calendar.DATE, 7);
		this.expDate = new Date(calendar.getTime().getTime());
	}

	@Override
	public void edit(Task task, String... fieldsToUpdate) throws SQLException {
		taskDao.update(task, fieldsToUpdate);
	}

	/**
	 * Cancel task by Client.
	 * 
	 * @param task
	 *            Task, which should change status to 'Canceled'.
	 * @throws SQLException 
	 * */
	@Override
	public void editTaskByUser(Task task) throws SQLException {
		LOGGER.info("Edit task by User {USER ID: " + task.getUser().getId()
				+ "; TASK ID: " + task.getId() + "; CREATION DATE: "
				+ task.getCreationTime() + "}");

		TaskStatus taskStatus = taskStatusDao
				.getTaskStatusById(StaticConstant.TaskStatus.CANCELED);

		task.setTaskStatus(taskStatus);
		
		User staff = userDao.getUserById(new ukma.baratheons.lms.anorm.Filter(
		Table.UserTable.TABLE_NAME, Table.UserTable.ID, Operator.EQ,

		task.getStaff().getId()));
		taskDao.update(task);
		if(staff.isReceiveEmail()) notificationService.sendTaskStatusChangedEmail(task, staff);
	}

	/**
	 * Cancel task by Staff member.
	 * 
	 * @param task
	 *            Task, which should change status to 'Canceled'.
	 * @throws SQLException 
	 * */
	@Override
	public void cancelTaskByStaff(Task task) throws SQLException {
			LOGGER.info("Cancel task with id: "+task.getId()+
						" by staff: "+task.getStaff().getId());
			
		TaskStatus taskStatus = taskStatusDao
				.getTaskStatusById(StaticConstant.TaskStatus.CANCELED);

		User user = userDao.getUserById(new ukma.baratheons.lms.anorm.Filter(
				Table.UserTable.TABLE_NAME, Table.UserTable.ID, Operator.EQ,
				task.getUser().getId()));
		
		task.setTaskStatus(taskStatus);

		taskDao.update(task);
		if(user.isReceiveEmail()) notificationService.sendTaskStatusChangedEmail(task, user);
		int penal = user.getPenaltyScore();
		user.setPenaltyScore(penal + 1);
		
		userDao.update(user);
	}

	/**
	 * Done task by Staff member.
	 * 
	 * @param id
	 *            id of task, which should change status to 'Completed'.
	 * @throws SQLException 
	 * */
	@Override
	public void doneTaskByStaff(long id) throws SQLException {
		TaskStatus taskStatus = taskStatusDao
				.getTaskStatusById(StaticConstant.TaskStatus.COMPLETED);

		Task task = taskDao.getTasksById(id);
		
		LOGGER.info("Done task with id: "+task.getId()+
				" by staff: "+task.getStaff().getId());
		
		task.setTaskStatus(taskStatus);
		setTime();
		task.setCompletionTime(currDate);
		User user = userDao.getUserById(new ukma.baratheons.lms.anorm.Filter(
				Table.UserTable.TABLE_NAME, Table.UserTable.ID, Operator.EQ, task.getUser()
				.getId()));

		taskDao.update(task);
		if(user.isReceiveEmail()) notificationService.sendTaskStatusChangedEmail(task, user);

		List<Task> dublicateTasks = taskDao.getTasksByParentId(task.getId());
		for (Task t : dublicateTasks) {
			if (t.getId() != t.getParentId()) {
				t.setTaskStatus(taskStatus);
				t.setCompletionTime(currDate);
				User u = userDao
						.getUserById(new ukma.baratheons.lms.anorm.Filter(
								Table.UserTable.TABLE_NAME, Table.UserTable.ID, Operator.EQ, t
										.getUser().getId()));

					taskDao.update(t);
				if(u.isReceiveEmail()) notificationService.sendTaskStatusChangedEmail(t, u);
			}
		}
	}

	/**
	 * Resume task by Administrator.
	 * 
	 * @param task
	 *            Task, which should change status to 'In Progress'.
	 * @throws SQLException 
	 * */
	@Override
	public void editTaskByAdmin(Task task) throws SQLException {
		
		LOGGER.info("Edit  task with id: "+task.getId()+
				" by admin");
		
		TaskStatus taskStatus = taskStatusDao
				.getTaskStatusById(StaticConstant.TaskStatus.IN_PROGRESS);

		long parentId = task.getId();
		if (task.getId() != task.getParentId())
			parentId = task.getParentId();

		List<Task> dublicateTasks = taskDao.getTasksByParentId(parentId);

		for (Task t : dublicateTasks) {
			t.setTaskStatus(taskStatus);
			t.setCompletionTime(null);
			
			taskDao.update(t);
				
			User u = userDao.getUserById(new ukma.baratheons.lms.anorm.Filter(
					Table.UserTable.TABLE_NAME, Table.UserTable.ID, Operator.EQ, t.getUser()
							.getId()));

			if(u.isReceiveEmail()) notificationService.sendTaskStatusChangedEmail(t, u);
		}
	}

	/**
	 * Refuse task by Staff member.
	 * 
	 * @param taskId
	 *            ID of task, which should change status to 'Open'.
	 * @throws SQLException 
	 * */
	@Override
	public void refuseTaskByStaff(long taskId) throws SQLException {

		Task task = getTasksById(taskId);

		TaskStatus taskStatus = taskStatusDao
				.getTaskStatusById(StaticConstant.TaskStatus.OPEN);
		notificationService.sendTaskRefusedByStaffEmail(task, task.getStaff(), userAdministrationService.getChiefAdmin());
		if (task.getTaskStatus().getId() == StaticConstant.TaskStatus.OPEN) {
			
			task.setStaff(null);
			taskDao.update(task, Table.TaskTable.STAFF);
		} else {
			long parentId = task.getId();
			if (task.getId() != task.getParentId())
				parentId = task.getParentId();
			List<Task> dublicateTasks = taskDao.getTasksByParentId(parentId);
			for (Task t : dublicateTasks) {
				t.setTaskStatus(taskStatus);
				t.setStaff(null);
				t.setParentId(null);
					taskDao.update(t, Table.TaskTable.STATUS,
							Table.TaskTable.STAFF, Table.TaskTable.PARENT_TASK);
			}
		}
	}

	@Override
	public List<Task> getRefusedTasks() {
		return taskDao.getRefusedTasks(new ukma.baratheons.lms.anorm.Filter(
				Table.TaskTable.TABLE_NAME, Table.TaskTable.STAFF, Operator.EQ,
				null));
	}

	/**
	 * Add comment to current task by user.
	 * 
	 * @param comment
	 *            Comment, which should be added to task.
	 * @param task
	 *            comment will be added to it.
	 * @throws SQLException 
	 * */
	@Override
	public void addComment(Comment comment, Task task) throws SQLException {
		comment.getTask().setId(task.getId());
		setTime();
		comment.setDate(dateTime);
		comment.setVisibility(1);
		try {
			commentDao.create(comment);
			LOGGER.info("Create comment '"+comment.getText()+
						"' for task with id: "+task.getId());
		} catch (SQLException e) {
			LOGGER.info("Failed create comment for task with id: "+task.getId()+
						" Exception: "+e.toString());
			throw e;
		}

	}

	@Override
	public Task getTasksById(long id) {
		Task task = taskDao.getTasksById(id);
		if (task.getParentId() == null)
			task.setParentId(task.getId());
		return task;
	}

	@Override
	public ArrayList<Task> getTasksByStaff(long staffId) {
		ArrayList<Task> tasks = (ArrayList<Task>) taskDao
				.getTasksByStaff(staffId);
		return tasks;
	}

	@Override
	public ArrayList<Task> getTasksByUser(String email) {
		User user = userDao.getUserByEmail(email);
		ArrayList<Task> tasks = (ArrayList<Task>) taskDao.getTasksByUser(user
				.getId());
		return tasks;
	}

	@Override
	public ArrayList<Task> getTasksByStatus(TaskStatus status) {
		ArrayList<Task> tasks = (ArrayList<Task>) taskDao
				.getTasksByStatus(status.getId());
		return tasks;
	}

	@Override
	public ArrayList<Task> getTaskByType(Task type) {
		ArrayList<Task> tasks = (ArrayList<Task>) taskDao.getTasksByType(type
				.getId());
		return tasks;
	}

	@Override
	public ArrayList<Task> getAllTasks() {
		ArrayList<Task> tasks = (ArrayList<Task>) taskDao.getAllTasks();
		return tasks;
	}

	@Override
	public List<Task> getAllTasksLazy() {
		return taskDao.getAllTasksLazy();
	}

	@Transactional
	@Override
	public void add(long roomId, long eqptId, long taskTypeId,
			String description, String email) throws SQLException {
		Equipment equipment = eqptDao.getEquipmentById(eqptId);
		TaskType taskType = taskTypeDao.getTaskTypeById(taskTypeId);
		User user;
		boolean isAuth = false;
		if (!"anonymousUser".equals(SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal())) {
			isAuth = true;
			org.springframework.security.core.userdetails.User us = (org.springframework.security.core.userdetails.User) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal();
			user = userDao.getUserByEmail(us.getUsername());
		} else
			user = userDao.getUserByEmail(email);
		User staff = userDao.getStaffByRoomId(roomId);
		TaskStatus taskStatus = taskStatusDao
				.getTaskStatusById(StaticConstant.TaskStatus.OPEN);
		ConfirmActionType confType = ConfirmActionType.new_task_with_reg;
		Task task = new Task();
		task.setTaskStatus(taskStatus);
		task.setTaskType(taskType);
		task.setEqpt(equipment);
		if (staff.isActive())task.setStaff(staff);
		setTime();
		task.setCreationTime(dateTime);
		if (user == null) {
			user = new User("");
			confType = ConfirmActionType.new_task;
			user.setEmail(email);
			user.setRegDate(currDate);
			user.setActive(false);
			user.setPassword(passServie.generate());
			user.setPenaltyScore(0);
			user.setRights(0);
			user.setRole(new Role(StaticConstant.Role.USER));
			long userId = userDao.addNew(user);
			user.setId(userId);
		}
		task.setUser(user);
		Unconfirmed unTask = null;
		task.setId(taskDao.addNew(task));

		if (isAuth) {
			task.setVisibility(true);
			if(task.getStaff().isReceiveEmail()) notificationService.sentNewTaskSubmittedEmail(task);
		} else if (!isAuth) {
			task.setVisibility(false);
			unTask = confirmationService.createUnconfirmed(email, confType,
					expDate, task.getId());
			try {
				unconfirmedDao.addNew(unTask);
				LOGGER.info("Add new unconfirmed");
			} catch (SQLException e) {
				LOGGER.error("Failed add new unconfirmed. Exception: "+e.toString());
				throw e;
			}
			notificationService.sendTaskConfirmationEmail(unTask, task.getId());
		}
		taskDao.update(task, Table.TaskTable.VISIBILITY);
		Comment comment = new Comment();
		comment.setDate(dateTime);
		comment.setVisibility(1);
		comment.setText(description);
		comment.setTask(task);
		comment.setUser(user);
		try {
			commentDao.create(comment);
			LOGGER.info("Add new comment: " + comment.getText());
		} catch (SQLException e) {
			LOGGER.error("Failed add new comment. Exception: "+e.toString());
			throw e;
		}
	}

	@Override
	public HashMap<Task, Comment> getSameTasks(Task task) {
		ArrayList<Task> tasks = (ArrayList<Task>) taskDao
				.getTasksByEquipment(task.getEqpt().getId());
		HashMap<Task, Comment> hashMap = new HashMap<Task, Comment>();

		if (tasks.size() > 0)
			for (Task t : tasks) {
				if (t.getId() != task.getId()
						&& (t.getTaskStatus().getId() == StaticConstant.TaskStatus.IN_PROGRESS || t
								.getTaskStatus().getId() == StaticConstant.TaskStatus.OPEN)) {
					List<Comment> com = commentDao.getCommentsByTask(t.getId());
					if (com != null && com.size() > 0) {
						Collections.sort(com);
						hashMap.put(t, com.get(0));
					}
				}
			}
		return hashMap;
	}

	@Override
	public void takeTask(long taskId, long taskTypeId, long parentId) throws SQLException {
		Task task = taskDao.getTasksById(taskId);
		TaskStatus status = taskStatusDao
				.getTaskStatusById(StaticConstant.TaskStatus.IN_PROGRESS);
		TaskType type = taskTypeDao.getTaskTypeById(taskTypeId);
		task.setTaskStatus(status);
		task.setTaskType(type);
		task.setParentId(parentId);
		taskDao.update(task);

	}


	@Override
	public void setTaskStaff(long taskId, User staff) throws SQLException {
		Task task = new Task();
		task.setId(taskId);
		task.setStaff(staff);
		taskDao.update(task, Table.TaskTable.STAFF);
		ArrayList<Task> dublicateTasks = (ArrayList<Task>) taskDao.getTasksByParentId(taskId);
		for (Task t:dublicateTasks){
			t.setStaff(staff);
			taskDao.update(t, Table.TaskTable.STAFF);
		}
	}

	@AfterReturning("execution(* ukma.baratheons.lms.dao.JdbcUserDao.update(ukma.baratheons.lms.entity.User)) && args(user)")
	public void freeTasks(User user) throws SQLException {
		if (user.getRole().getId() == StaticConstant.Role.LABORANT
				&& user.isActive() == false) {
			TaskStatus statusOpen = taskStatusDao
					.getTaskStatusById(StaticConstant.TaskStatus.OPEN);
			TaskStatus statusInProgress = taskStatusDao
					.getTaskStatusById(StaticConstant.TaskStatus.IN_PROGRESS);
			ArrayList<Task> freeTask = (ArrayList<Task>) taskDao
					.getTasksByStaffAndStatus(user.getId(),
							statusInProgress.getId());
			for (Task t : freeTask) {
				t.setTaskStatus(statusOpen);
				t.setStaff(null);
				taskDao.update(t);
			}
		}
	}

	@Override
	public ArrayList<Task> getFreeTasks() {
		ArrayList<Task> tasks = (ArrayList<Task>) taskDao
				.getTasksByActiveAndStatus(false, StaticConstant.TaskStatus.OPEN);
		return tasks;
	}

	@Override
	public ArrayList<TaskType> getTaskTypes() {
		ArrayList<TaskType> types = (ArrayList<TaskType>) taskTypeDao.getAll();
		return types;
	}
}
