package ukma.baratheons.lms.service;

import java.sql.SQLException;
import java.util.List;

import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.entity.Equipment;
import ukma.baratheons.lms.entity.EquipmentType;

public interface EquipmentService {

	public void add(Equipment eq) throws SQLException;
	
	public void edit(Equipment equipment) throws SQLException;
	
	public List<Equipment> getEquipmentByRoom(int roomId);
	
	public List<Equipment> getEquipmentLazy();
	
	public List<Equipment> getEquipmentEager(Filter filter);
	
	public List<EquipmentType> getEquipmentTypes();
	
}
