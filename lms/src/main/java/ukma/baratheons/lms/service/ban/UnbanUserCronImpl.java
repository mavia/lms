package ukma.baratheons.lms.service.ban;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.dao.UserDao;
import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.service.UserAdministrationService;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by pitermiller on 28.11.15.
 */
@Service
public class UnbanUserCronImpl implements UnbanUserCron{

    @Autowired
    UserDao userDao;

    @Autowired
    UserAdministrationService userService;
   @Scheduled(cron ="0 0 12 ? * SAT " )//will run every Saturday at 12pm
   public void checkForUnban() throws SQLException {
        List<User> userList = userDao.getAllUsersWithPenalty();
        for(User user: userList){
            user.setPenaltyScore(user.getPenaltyScore() - 1);
            System.out.println(user.getPenaltyScore());
            userService.update(user);
        }
    }
}
