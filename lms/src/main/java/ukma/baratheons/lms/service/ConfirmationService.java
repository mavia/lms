package ukma.baratheons.lms.service;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;

import ukma.baratheons.lms.entity.ConfirmActionType;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.Unconfirmed;

public interface ConfirmationService {
	
	Unconfirmed createUnconfirmed(String email, ConfirmActionType action_type, Date exp_date, long key);

	String generateUrl(String email, String action_type, Date exp_date);

	String generateUrl(String email, String action_type, Date exp_date, long task_id);
	
	void confirm(Unconfirmed unconfirmed,long key) throws SQLException;

	boolean check(String hash, long key);

}
