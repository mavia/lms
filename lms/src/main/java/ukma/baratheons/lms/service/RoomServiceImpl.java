package ukma.baratheons.lms.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.dao.RoomDao;
import ukma.baratheons.lms.dao.UnconfirmedDao;
import ukma.baratheons.lms.entity.Room;

@Service
public class RoomServiceImpl implements RoomService {
	
	@Autowired
	private RoomDao roomDao;
	
	@Autowired
	private UnconfirmedDao ud;
	
	@Override
	public void create(Room room) throws SQLException  {
			roomDao.create(room);
	}

	@Override
	public void update(Room room) throws SQLException {
			roomDao.update(room);
	}

	@Override
	public List<Room> getRoomsEager(Filter filter) {
		return roomDao.getRoomsEager(filter);
	}
	
	

	@Override
	public List<Room> getRoomsLazy(Filter filter) {
		return roomDao.getRoomsLazy(filter);
	}

	@Override
	public boolean delete(Room room) throws SQLException {
		roomDao.delete(room);
		return true;
	}

	@Override
	public List<Room> getAllRooms() {
		return roomDao.getAllRooms();
	}

}
