package ukma.baratheons.lms.service;

public class StaticConstant {

	public static class TaskStatus{
		public static final long OPEN = 11;
		
		public static final long CANCELED = 13;
		
		public static final long IN_PROGRESS = 12;
		
		public static final long COMPLETED = 14;
	}
	
	public static class Role{
		public static final long ADMIN = 1;
		
		public static final long LABORANT = 2;
		
		public static final long USER = 3;
	}
	
}
