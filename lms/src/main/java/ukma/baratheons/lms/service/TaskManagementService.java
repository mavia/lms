package ukma.baratheons.lms.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ukma.baratheons.lms.entity.*;

public interface TaskManagementService {

	public void edit(Task task, String...fieldsToUpdate) throws SQLException;

	public void editTaskByUser(Task task) throws SQLException;

	public void cancelTaskByStaff(Task task) throws SQLException;

	public void doneTaskByStaff(long id) throws SQLException;

	public void editTaskByAdmin(Task task) throws SQLException;
	
	public void refuseTaskByStaff(long taskId) throws SQLException;
	
	public void addComment(Comment comment, Task task) throws SQLException;

	public Task getTasksById(long id);

	public ArrayList<Task> getTasksByStaff(long staffId);

	public ArrayList<Task> getTasksByUser(String email);

	public ArrayList<Task> getTasksByStatus(TaskStatus status);

	public ArrayList<Task> getTaskByType(Task type);

	public ArrayList<Task> getAllTasks();

	public List<Task> getAllTasksLazy();

	public void add(long roomId, long eqptId, long taskTypeId,
			String description, String email) throws SQLException;

	public HashMap<Task, Comment> getSameTasks(Task task);
	
	public List<Task> getRefusedTasks();

	public void takeTask(long taskId, long taskTypeId, long parentId) throws SQLException;

	public void setTaskStaff(long taskId, User staff) throws SQLException;

	public ArrayList<Task> getFreeTasks();

	public ArrayList<TaskType> getTaskTypes();
}
