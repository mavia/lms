package ukma.baratheons.lms.service;

import java.util.List;

import ukma.baratheons.lms.entity.Filter;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.User;

/**
 * This interface provides
 * service to search tasks
 * @version 1.0
 * @author Max Oliynick
 * */

public interface SearchTaskService {
	
	/**
	 * Returns list of all tasks using given filters.
	 * Note that search result depends on the user rights in the system
	 * @param user - current user
	 * @param filters - list of filters to apply
	 * */
	public List<Task> getTasks(final User user, final List<Filter> filters);

}
