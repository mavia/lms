package ukma.baratheons.lms.service;

import java.sql.SQLException;

public interface ResetPasswordService {
	
	public void reset(String email) throws SQLException;
	
}
