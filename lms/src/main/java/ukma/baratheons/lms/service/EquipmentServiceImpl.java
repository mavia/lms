package ukma.baratheons.lms.service;

import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.anorm.Operator;
import ukma.baratheons.lms.dao.EquipmentDao;
import ukma.baratheons.lms.dao.EquipmentTypeDao;
import ukma.baratheons.lms.entity.Equipment;
import ukma.baratheons.lms.entity.EquipmentType;
import ukma.baratheons.lms.query.Table;

@Service
public class EquipmentServiceImpl implements EquipmentService{

	@Autowired
	private EquipmentDao equipmentDao;
	
	@Autowired
	private EquipmentTypeDao equipmentTypeDao;

	private static final Logger LOGGER = LogManager.getLogger(EquipmentServiceImpl.class);

	@Override
	public void add(Equipment equipment) throws SQLException {
		equipmentDao.create(equipment);
		LOGGER.info("Create new equipment: room = "+equipment.getRoom()+
					" type = " + equipment.getEqptType() +
					" local number = " + equipment.getLocalNumber());
	}

	@Override
	public void edit(Equipment equipment) {
		try {
			equipmentDao.update(equipment);
			LOGGER.info("Update equipment with id: " + equipment.getId());
		} catch (SQLException e) {
			LOGGER.error("Failed update task with id: " + equipment.getId() +
					" Exception: " + e.toString());
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Equipment> getEquipmentByRoom(int roomId){
		return equipmentDao.getEquipmentByRoom(new Filter(Table.EquipmentTable.TABLE_NAME, Table.EquipmentTable.ROOM, Operator.EQ, roomId));
	}

	@Override
	public List<Equipment> getEquipmentLazy() {
		return equipmentDao.getEquipmentLazy();
	}

	@Override
	public List<Equipment> getEquipmentEager(Filter filter) {
		return equipmentDao.getEquipmentEager(filter);
	}

	@Override
	public List<EquipmentType> getEquipmentTypes() {
		return equipmentTypeDao.getAll();
	}

}
