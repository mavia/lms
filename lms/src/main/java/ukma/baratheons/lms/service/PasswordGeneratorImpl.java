package ukma.baratheons.lms.service;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public final class PasswordGeneratorImpl implements PasswordGeneratorService {

	/**
	 * 
	 */
	private static final int charactersSize = 100;

	private static char[] characters = new char[charactersSize];

	private static int charactersCount = 0;

	private int passwordSize = 15;

	public PasswordGeneratorImpl() {
		initCharacters();
	}

	private static char[] initCharacters() {
		int i = 0;

		// add 0-9
		for (int j = 48; j < 58; ++i, ++j, ++charactersCount) {
			characters[i] = (char) j;
		}

		// add @ + a-z
		for (int j = 64; j < 91; ++i, ++j, ++charactersCount) {
			characters[i] = (char) j;
		}

		// add A-Z
		for (int j = 97; j < 123; ++i, ++j, ++charactersCount) {
			characters[i] = (char) j;
		}

		return characters;
	}

	public String generate() {

		Random rnd = new Random();

		String password = "";

		for (int i = 0; i < passwordSize; ++i) {
			password += characters[rnd.nextInt(charactersCount)];
		}

		return password;
	}

}