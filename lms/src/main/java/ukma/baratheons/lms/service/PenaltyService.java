package ukma.baratheons.lms.service;

import ukma.baratheons.lms.entity.User;

public interface PenaltyService {

	public void decreasePenalties();
	
	public void increasePenalties(User user);
	
	public void isUserBanned(User user);
	
}
