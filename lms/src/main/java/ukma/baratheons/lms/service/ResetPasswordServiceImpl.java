/**
 * 
 */
package ukma.baratheons.lms.service;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.dao.UnconfirmedDao;
import ukma.baratheons.lms.dao.UserDao;
import ukma.baratheons.lms.entity.ConfirmActionType;
import ukma.baratheons.lms.entity.Unconfirmed;

/**
 * @author Nikolay
 *
 */
@Service
public class ResetPasswordServiceImpl implements ResetPasswordService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ukma.baratheons.lms.service.ResetPasswordService#reset()
	 */
	@Autowired
	private UnconfirmedDao unconfDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private ConfirmationService confirmationService;

	private Date expDate;

	private void setTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 7);
		this.expDate = new Date(calendar.getTime().getTime());
	}
	@Override
	public void reset(String email){
		final int key = 10;
		setTime();
		final Unconfirmed unc = confirmationService
				.createUnconfirmed(email,ConfirmActionType.reset_password, expDate, key);
		try {
			unconfDao.addNew(unc);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// TODO Auto-generated method stub
				notificationService.sendResetPasswordEmail(unc, key);
			}
		}).start();
		

	}

}
