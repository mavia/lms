package ukma.baratheons.lms.service;

import java.sql.SQLException;
import java.util.List;

import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.entity.Room;

public interface RoomService {

	public List<Room> getRoomsEager(Filter filter);
	
	public List<Room> getRoomsLazy(Filter filter);
	
	public void create(Room room) throws SQLException;
	
	public void update(Room room) throws SQLException;

	public boolean delete(Room room) throws SQLException;
	
	public List<Room>getAllRooms();

}
