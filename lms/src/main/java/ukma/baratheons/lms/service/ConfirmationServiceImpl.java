package ukma.baratheons.lms.service;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ukma.baratheons.lms.dao.LogDao;
import ukma.baratheons.lms.dao.TaskDao;
import ukma.baratheons.lms.dao.UnconfirmedDao;
import ukma.baratheons.lms.dao.UserDao;
import ukma.baratheons.lms.entity.ConfirmActionType;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.Unconfirmed;
import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.util.MD5;

/**
 * 
 * @author Nikolay
 *
 */
@Service
public class ConfirmationServiceImpl implements ConfirmationService {

	@Autowired
	private UnconfirmedDao unconfDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private TaskDao taskDao;

	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private PasswordGeneratorService passwordGeneratorService;

	private static final Logger LOGGER = LogManager.getLogger(ConfirmationServiceImpl.class);

	public Unconfirmed createUnconfirmed(String email,
			ConfirmActionType action_type, Date exp_date, long key) {
		Unconfirmed unc = new Unconfirmed();
		unc.setAction_type(action_type);
		unc.setEmail(email);
		unc.setExp_date(exp_date);
		unc.setHash(generateUrl(email, action_type.toString(), exp_date, key));
		return unc;
	}

	@Override
	public String generateUrl(String email, String action_type,
			Date exp_date) {

		return MD5.getHash(email + "&" + action_type + "&" + exp_date);

	}

	@Override
	public String generateUrl(String email, String action_type,
			Date exp_date, long key) {
		// TODO Auto-generated method stub
		return MD5.getHash(email + "&" + action_type + "&" + exp_date + "&"
				+ key);
	}

	@Override
	public void confirm(Unconfirmed unc, long key) throws SQLException {
		User user = userDao.getUserByEmail(unc.getEmail());
		if (unc.getAction_type() == ConfirmActionType.new_task_with_reg
				|| unc.getAction_type() == ConfirmActionType.new_task) {
			
			Task task = taskDao.getTasksById(key);
			task.setVisibility(true);
			try {
				taskDao.update(task);
				LOGGER.info("Update task with id: "+task.getId());
			} catch (SQLException e) {
				LOGGER.error("Failed update task with id: " + task.getId() +
						" Exception: " + e.toString());
				e.printStackTrace();
			}
			unconfDao.deleteByHash(unc.getHash());
			if (unc.getAction_type() == ConfirmActionType.new_task) {
				user.setActive(true);
				user.setRights(user.getRole().getDefRights());
				notificationService.sendUserConfirmedEmail(user);
				user.setPassword(MD5.getHash(user.getPassword()));
			}
			
			try {
				userDao.update(user);
				LOGGER.info("Update user with id: "+user.getId());
			} catch (SQLException e) {
				LOGGER.error("Failed update user with id: " + user.getId() +
						" Exception: " + e.toString());
				e.printStackTrace();
			}

			notificationService.sentNewTaskSubmittedEmail(task);
		} else if (unc.getAction_type() == ConfirmActionType.reset_password) {
			unconfDao.deleteByHash(unc.getHash());
			String password = passwordGeneratorService.generate();
			user.setPassword(MD5.getHash(password));
			try {
				userDao.update(user);
				LOGGER.info("Update user with id: "+user.getId());
			} catch (SQLException e) {
				LOGGER.error("Failed update user with id: " + user.getId() +
						" Exception: " + e.toString());
				e.printStackTrace();
			}
			notificationService.sendUserPaswordChangedEmail(user, password);
		}

	}

	@Override
	public boolean check(String hash, long key) {
		Unconfirmed unc = unconfDao.getByHash(hash);
		
		return unc != null
				&& hash.equals(generateUrl(unc.getEmail(), unc.getAction_type()
						.toString(), unc.getExp_date(), key))
				&& (new Timestamp(Calendar.getInstance().getTime().getTime()))
						.compareTo(unc.getExp_date()) <= 0;
	}

}
