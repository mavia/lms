package ukma.baratheons.lms.service.reports;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;
import ukma.baratheons.lms.entity.Room;
import ukma.baratheons.lms.entity.Task;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class RoomsListExcelView extends AbstractExcelView {

    @Override
    protected void buildExcelDocument(Map model, HSSFWorkbook workbook,
                                      HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HSSFSheet excelSheet = workbook.createSheet("Rooms Report");
        setExcelHeader(excelSheet);


        List<Room> roomList = (List<Room>) model.get("roomList");
        setExcelRows(excelSheet,roomList);

    }

    public void setExcelHeader(HSSFSheet excelSheet) {
        HSSFRow excelHeader = excelSheet.createRow(0);
        excelHeader.createCell(0).setCellValue("Room Number");
        excelHeader.createCell(1).setCellValue("Malfunctions Count");
        excelSheet.autoSizeColumn(0);
        excelSheet.autoSizeColumn(1);
    }

    public void setExcelRows(HSSFSheet excelSheet, List<Room> roomList){
        int record = 1;
        for (Room room : roomList) {
            HSSFRow excelRow = excelSheet.createRow(record++);
            excelRow.createCell(0).setCellValue(room.getNumber());
            excelRow.createCell(1).setCellValue(room.getMalfunctionsCount());
            excelSheet.autoSizeColumn(0);
            excelSheet.autoSizeColumn(1);
        }
    }
}
