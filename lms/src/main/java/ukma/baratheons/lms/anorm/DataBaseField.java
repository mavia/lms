package ukma.baratheons.lms.anorm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation designates the annotated field to be linked to a given column
 * @author DmytroProskurnia
 * */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DataBaseField {

	String value();

}
