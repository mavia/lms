package ukma.baratheons.lms.anorm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation designates the class to be a representation of a DataBase entity
 * @author DmytroProskurnia
 * */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Entity {

	String value();
	
}
