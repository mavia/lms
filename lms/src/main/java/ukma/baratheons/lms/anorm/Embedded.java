package ukma.baratheons.lms.anorm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation designates the annotated field represents a java Object
 * of the specified class
 * @author DmytroProskurnia
 * */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface Embedded {
	Class value();
	boolean nullable();
}
