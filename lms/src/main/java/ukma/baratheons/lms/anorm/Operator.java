package ukma.baratheons.lms.anorm;

/**
 * This enum defines logical operators that can be used when building SQL queries
 * @author DmytroProskurnia
 *
 */

public enum Operator {
	EQ("="), LESS("<"), GREATER(">"), LEQ("<="), GREQ(">="), NEQ("!=");
	
	String operator;
	
	Operator(String operator){
		this.operator=operator;
	}
}
