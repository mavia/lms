
package ukma.baratheons.lms.anorm;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * This class generates SQL queries for creating, updating and deleting DataBase
 * entities
 * 
 * @author DmytroProskurnia
 * */

public class AnORMWriter {

	private JdbcTemplate jdbcTemplate;

	public AnORMWriter(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * This method provides means for inserting entities into a DataBase
	 * 
	 * @param entity
	 *            java object to be created
	 * @return inserted Entity's Id
	 * @throws SQLException
	 */
	public Object insert(Object entity) throws SQLException {
		if (!entity.getClass().isAnnotationPresent(Entity.class))
			throw new IllegalArgumentException();
		int paremetersCount = 0;
		StringBuilder query = new StringBuilder();
		query.append("INSERT INTO "
				+ entity.getClass().getAnnotation(Entity.class).value() + " (");
		Field[] fields = entity.getClass().getDeclaredFields();
		for (Field f : fields) {
			if (f.isAnnotationPresent(Base.class)) {
				query.append(f.getAnnotation(DataBaseField.class).value()
						+ ", ");
				paremetersCount++;
			}
		}
		query.delete(query.length() - 2, query.length());
		query.append(") VALUES (");
		for (int i = 0; i < --paremetersCount;)
			query.append("?,");
		query.append("?)");
		PreparedStatement ps = jdbcTemplate
				.getDataSource()
				.getConnection()
				.prepareStatement(query.toString(),
						Statement.RETURN_GENERATED_KEYS);
		Field id = null;
		for (Field f : fields) {
			if (f.isAnnotationPresent(Id.class))
				id = f;
			if (f.isAnnotationPresent(Base.class)) {
				try {
					f.setAccessible(true);
					Object value = null;
					if (f.isAnnotationPresent(Embedded.class)||f.isAnnotationPresent(Enumeration.class))
						value = getId(f.get(entity));
					else
						value = f.get(entity);
					ps.setObject(++paremetersCount, value);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		ps.executeUpdate();
		Object idValue = null;
		try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
			if (generatedKeys.next()) {
				idValue = generatedKeys.getObject(1);
			} else {
				throw new SQLException("Creating user failed, no ID obtained.");
			}
		}
		System.out.println(ps.toString());
		ps.close();
		try {
			id.setAccessible(true);
			id.set(entity, idValue);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return idValue;
	}

	/**
	 * This method provides means for updating entities in a DataBase
	 * 
	 * @param entity
	 *            java object to be updated
	 * @return a String representation of an SQL query for updating argument
	 *         Object in a DataBase
	 * @throws SQLException 
	 */
	public void update(Object entity, String... fieldsToUpdate) throws SQLException {
		if (!entity.getClass().isAnnotationPresent(Entity.class))
			throw new IllegalArgumentException();
		StringBuilder query = new StringBuilder();
		query.append(("UPDATE " + entity.getClass().getAnnotation(Entity.class)
				.value())
				+ " SET ");
		Field[] fields = entity.getClass().getDeclaredFields();
		int paremetersCount = 0;
		Object[] values = new Object[fields.length];
		Field id = null;
		for (Field f : fields) {
			if (f.isAnnotationPresent(Id.class))
				id = f;
			if (f.isAnnotationPresent(DataBaseField.class)
					&& !f.isAnnotationPresent(Transient.class)) {
				boolean update = true;
				if (fieldsToUpdate != null && fieldsToUpdate.length > 0) {
					update = false;
					for (String s : fieldsToUpdate)
						if (s.equals(f.getAnnotation(DataBaseField.class)
								.value())) {
							update = true;
							break;
						}
				}
				if (!update)
					continue;
				try {
					f.setAccessible(true);
					Object value = null;
					if (f.isAnnotationPresent(Embedded.class))
						value = getId(f.get(entity));
					else
						value = f.get(entity);
					values[paremetersCount++] = value;
					query.append(f.getAnnotation(DataBaseField.class).value()
							+ " = ? ,");
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		query.setCharAt(query.length() - 1, ' ');
		query.append(" WHERE " + id.getAnnotation(DataBaseField.class).value()
				+ " = ?");
		try {
			id.setAccessible(true);
			values[paremetersCount] = id.get(entity);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			PreparedStatement ps = jdbcTemplate.getDataSource().getConnection()
					.prepareStatement(query.toString());
			for (int i = 0; i < paremetersCount; i++)
				ps.setObject(i + 1, values[i]);
			ps.setObject(paremetersCount + 1, values[paremetersCount]);
			ps.executeUpdate();
			ps.close();
	}
	
	public void delete(Object object) throws SQLException {
		for (Field f : object.getClass().getDeclaredFields())
			if (f.isAnnotationPresent(Id.class))
				try {
					f.setAccessible(true);
					PreparedStatement ps = jdbcTemplate
							.getDataSource()
							.getConnection()
							.prepareStatement(
									"DELETE FROM "
											+ object.getClass()
													.getAnnotation(Entity.class)
													.value()
											+ " WHERE "
											+ f.getAnnotation(
													DataBaseField.class)
													.value() + "= ?");
					ps.setObject(1, f.get(object));
					ps.executeUpdate();
					ps.close();
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}

	/**
	 * This method deletes all rows matching the passed Filter(s) 
	 * @param filter
	 * @throws SQLException
	 */
	public void delete(Filter... filter) throws SQLException {
		if (filter.length < 1)
			return;
		StringBuilder query = new StringBuilder();
		query.append("DELETE FROM " + filter[0].getTable() + " WHERE ");
		for (Filter f : filter)
			query.append(f.getField() + f.getOperator().operator + " ?,");
		query.deleteCharAt(query.length() - 1);
		PreparedStatement ps = jdbcTemplate.getDataSource().getConnection()
				.prepareStatement(query.toString());
		for (int i = 0; i < filter.length; i++)
			ps.setObject(i + 1, filter[i].getValue());
		ps.executeUpdate();
		ps.close();
	}

	private Object getId(Object object) {
		if (object == null)
			return null;
		Field[] fields = object.getClass().getDeclaredFields();
		for (Field f : fields)
			if (f.isAnnotationPresent(Id.class))
				try {
					f.setAccessible(true);
					Object o = f.get(object);
					if (o == null)
						o = insert(object);
					return o;
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		return null;
	}
}