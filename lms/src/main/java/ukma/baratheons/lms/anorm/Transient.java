package ukma.baratheons.lms.anorm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation designates that the value of the annotated DataBaseField shall not be
 * passed to DataBase 
 * @author DmytroProskurnia
 * */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Transient {

}
