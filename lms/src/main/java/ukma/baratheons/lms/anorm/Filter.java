package ukma.baratheons.lms.anorm;

public class Filter {
	
	private String table;
	
	private String field;
	
	private Operator operator;
	
	private Object value;
	
	public Filter() {
		
	}
	
	public Filter(String table, String column, Operator operator, Object value) {
		this.setTable(table);
		this.setField(column);
		this.setOperator(operator);
		this.setValue(value);
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}
	
}
