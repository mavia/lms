package ukma.baratheons.lms.anorm;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation designates the annotated DataBaseField to be the DataBase Key 
 * @author DmytroProskurnia
 * */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Id {

}
