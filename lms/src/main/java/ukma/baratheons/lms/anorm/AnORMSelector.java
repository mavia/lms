package ukma.baratheons.lms.anorm;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;

/**
 * This class provides means for generating SQL queries on Annotated java
 * objects
 * 
 * @author DmytroProskurnia
 * */

public class AnORMSelector<T> {

	private JdbcTemplate jdbcTemplate;
	private Map<String, HashMap<String, Field>> map = new HashMap<>();
	private Map<String, WhereClause> where = new HashMap<>();
	private StringBuilder query = new StringBuilder();
	private Map<String, String> tableNames = new HashMap<>();
	private Set<String> groupBy;
	private StringBuilder tail;
	private Class<?> clazz;
	int ParametersCount = 0;
	ArrayList<Object> parameters = new ArrayList<>();;

	/**
	 * Constructor
	 * 
	 * @param jdbcTemplate
	 * @param clazz
	 *            Class of java Object which is meant to be created
	 */
	public AnORMSelector(JdbcTemplate jdbcTemplate, Class<?> clazz) {
		if (!clazz.isAnnotationPresent(Entity.class))
			throw new IllegalArgumentException();
		this.clazz = clazz;
		this.jdbcTemplate = jdbcTemplate;
		prepareFields(clazz, clazz.getAnnotation(Entity.class).value());
	}

	private void prepareFields(Class<?> clazz, String prefix) {
		if (!clazz.isAnnotationPresent(Entity.class))
			return;
		tableNames.put(clazz.getAnnotation(Entity.class).value(), prefix);
		Field[] fields = clazz.getDeclaredFields();
		HashMap<String, Field> list = new HashMap<>();
		map.put(clazz.getAnnotation(Entity.class).value(), list);
		for (Field f : fields) {
			if (f.isAnnotationPresent(DataBaseField.class)) {
				list.put(f.getAnnotation(DataBaseField.class).value(), f);
				if (f.isAnnotationPresent(Embedded.class))
					prepareFields(f.getAnnotation(Embedded.class).value(),
							prefix
									+ f.getAnnotation(DataBaseField.class)
											.value());
			}
		}
	}

	public AnORMSelector<T> groupBy(String table, String field) {
		if (groupBy == null)
			groupBy = new LinkedHashSet<>();
		groupBy.add(tableNames.get(table) + field);
		return this;
	}

	/**
	 * Method is used for specifying fields which to include in or,
	 * alternatively, exclude from the ResultSet
	 * 
	 * @param tableName
	 *            name of the SQL table
	 * @param neededFields
	 *            set true if only the following fields are to be included; set
	 *            false if the following fields are to be excluded
	 * @param dataBaseFields
	 *            fields to apply the neededFields boolean to
	 */

	public void selectFields(String tableName, boolean neededFields,
			String... dataBaseFields) {
		HashMap<String, Field> fields = map.get(tableName);
		if (neededFields) {
			HashMap<String, Field> set = new HashMap<>();
			for (String s : dataBaseFields)
				set.put(s, fields.get(s));
			map.put(tableName, set);
		} else
			for (String s : dataBaseFields)
				fields.remove(s);
	}

	/**
	 * Method is used to set conditions on the results
	 * 
	 * @return WhereClause object, which all(Filter filter) and or(Filter
	 *         filter) methods can be called on for adding conditions.
	 */
	public WhereClause where(Filter filter) {
		if (filter == null)
			return null;
		WhereClause whereClause = new WhereClause(filter.getTable(),
				filter.getField(), filter.getOperator(), filter.getValue());
		where.put(filter.getTable(), whereClause);
		return whereClause;
	}

	/**
	 * @return query result
	 */
	public List<T> select() {
		generateQuery();
		return jdbcTemplate.query(new PrepStatCreator(),
				new AnORMExtractor<T>());
	}

	private String generateQuery() {
		String tableName = clazz.getAnnotation(Entity.class).value();
		query.append("SELECT ");
		addFields(clazz, tableName);
		query.setCharAt(query.length() - 1, ' ');
		query.append("FROM " + (tableName) + " ");
		searchEmbedded(clazz, tableName, JoinType.inner);
		if (tail != null)
			query.append(tail.toString());
		setCondititons(tableName, " WHERE ");
		if (groupBy != null) {
			query.append(" GROUP BY");
			for (String s : groupBy)
				query.append(" " + s + ",");
		}
		query.deleteCharAt(query.length() - 1);
		return query.toString();
	}

	/**
	 * Sets conditions on the query result, if there are any
	 * 
	 * @param tableName
	 * @param conj
	 *            conjunction to use before the condition
	 */
	private void setCondititons(String tableName, String conj) {
		if (!where.containsKey(tableName))
			return;
		query.append(conj + " " + where.get(tableName).toString());
	}

	/**
	 * This method generates columns to be selected in the query
	 * 
	 * @param clazz
	 * @param prefix
	 */
	private void addFields(Class<?> clazz, String prefix) {
		Collection<Field> fields = map.get(
				clazz.getAnnotation(Entity.class).value()).values();
		for (Field f : fields) {
			if (f.isAnnotationPresent(DataBaseField.class)) {
				query.append(prefix + "."
						+ f.getAnnotation(DataBaseField.class).value() + " "
						+ prefix + f.getAnnotation(DataBaseField.class).value()
						+ ",");
				if (f.isAnnotationPresent(Embedded.class))
					addFields(f.getAnnotation(Embedded.class).value(), prefix
							+ f.getAnnotation(DataBaseField.class).value());
			}
		}
	}

	/**
	 * This method adds JOIN on a table of an Embedded object
	 * 
	 * @param clazz
	 *            Embedded object class
	 * @param tableName
	 *            Embedded object table name
	 * @param joinType
	 *            type of JOIN with the table of the Embedded object
	 */
	private void addJoin(Class<?> clazz, String tableName, JoinType joinType) {
		Field[] fields = clazz.getDeclaredFields();
		for (Field f : fields)
			if (f.isAnnotationPresent(Id.class)) {
				query.append(tableName + "."
						+ f.getAnnotation(DataBaseField.class).value() + " ");
				break;
			}
		setCondititons(clazz.getAnnotation(Entity.class).value(), " AND ");
		searchEmbedded(clazz, tableName, joinType);
	}

	/**
	 * This method scans for Embedded objects and calls addJoin(..) method if
	 * there are any
	 * 
	 * @param clazz
	 *            Object class
	 * @param tableName
	 *            Object table name
	 * @param joinType
	 *            type of JOIN currently used
	 */
	private void searchEmbedded(Class<?> clazz, String tableName,
			JoinType joinType) {
		Collection<Field> fields = map.get(
				clazz.getAnnotation(Entity.class).value()).values();
		for (Field f : fields)
			if (f.isAnnotationPresent(Embedded.class)) {
				JoinType currentJoinType = joinType;
				if (joinType == JoinType.inner)
					if (f.getAnnotation(Embedded.class).nullable())
						currentJoinType = JoinType.left;
				else{
					currentJoinType = joinType;
				}
				query.append(currentJoinType.joinType);
				query.append("JOIN "
						+ ((Entity) (f.getAnnotation(Embedded.class).value()
								.getAnnotation(Entity.class))).value() + " "
						+ tableName
						+ f.getAnnotation(DataBaseField.class).value() + " ON "
						+ tableName + "."
						+ f.getAnnotation(DataBaseField.class).value() + "=");
				addJoin(f.getAnnotation(Embedded.class).value(), tableName
						+ f.getAnnotation(DataBaseField.class).value(),
						currentJoinType);
			}
	}

	/**
	 * This inner class is used to set conditions on the query result
	 * 
	 * @author Dimitri
	 *
	 */
	public class WhereClause {

		private StringBuilder sb;
		private String tableName;

		public WhereClause(String tableName, String columnName,
				Operator operator, Object value) {
			this.tableName = tableNames.get(tableName);
			sb = new StringBuilder();
			where.put(tableName, this);
			setCondition("", columnName, operator, value);
		}

		private void setCondition(String conj, String columnName,
				Operator operator, Object value) {
			sb.append(conj + " " + tableName + "." + columnName);
			if (value != null) {
				sb.append(operator.operator + " ? ");
				parameters.add(ParametersCount++, value);
			} else {
				if (operator.operator.equals("="))
					sb.append(" IS NULL ");
				else
					sb.append(" IS NOT NULL ");
			}
		}

		public WhereClause and(Filter filter) {
			setCondition(" AND ", filter.getField(), filter.getOperator(),
					filter.getValue());
			return this;
		}

		public WhereClause or(Filter filter) {
			setCondition(" OR ", filter.getField(), filter.getOperator(),
					filter.getValue());
			return this;
		}

		public String toString() {
			return sb.toString();
		}
	}

	private class AnORMExtractor<T> implements RowMapper<T> {

		private HashMap<Class<?>, HashMap<Object, Object>> cache = new HashMap<>();

		AnORMExtractor() {
			initializeMap(clazz);
		}

		private void initializeMap(Class<?> clazz) {
			Collection<Field> fields = map.get(
					clazz.getAnnotation(Entity.class).value()).values();
			for (Field f : fields) {
				if (f.isAnnotationPresent(Embedded.class)) {
					cache.put(f.getAnnotation(Embedded.class).value(),
							new HashMap<>());
					initializeMap(f.getAnnotation(Embedded.class).value());
				}
			}
		}

		public T mapRow(ResultSet rs, int rowNum) throws SQLException {
			return (T) mapObject(rs, rowNum, clazz,
					clazz.getAnnotation(Entity.class).value());
		}

		private Object mapObject(ResultSet rs, int rowNum, Class<?> clazz,
				String tableName) {
			Object obj = null;
			try {
				obj = clazz.newInstance();
				if (!obj.getClass().isAnnotationPresent(Entity.class))
					throw new IllegalArgumentException();
				Collection<Field> fields = map.get(
						clazz.getAnnotation(Entity.class).value()).values();
				// scanning for values for the fields in the ResultSet row
				for (Field f : fields) {
					if (f.isAnnotationPresent(DataBaseField.class)) {
						f.setAccessible(true);
						// if the field represents an Embedded object
						if (f.isAnnotationPresent(Embedded.class)) {
							Object embObj = null;
							try {
								// checking if the embedded object has been
								// mapped and therefore cached already
								embObj = cache
										.get(f.getAnnotation(Embedded.class)
												.value())
										.get(rs.getObject(tableName
												+ f.getAnnotation(
														DataBaseField.class)
														.value()));
							} catch (SQLException e) {
								// e.printStackTrace();
							}
							// recursive call to map an embedded object
							if (embObj == null) {
								embObj = mapObject(
										rs,
										rowNum,
										f.getAnnotation(Embedded.class).value(),
										tableName
												+ f.getAnnotation(
														DataBaseField.class)
														.value());
								if (embObj != null)
									cache.get(embObj.getClass())
											.put(rs.getObject(tableName
													+ f.getAnnotation(
															DataBaseField.class)
															.value()), embObj);
							}
							f.set(obj, embObj);
							// if the field represent java enum type
						} else if (f.isAnnotationPresent(Enumeration.class)) {
							f.set(obj, Enum.valueOf((Class<Enum>) f.getType(),
									rs.getString(tableName
											+ f.getAnnotation(
													DataBaseField.class)
													.value())));
						} else
							// mapping a field of a primitive type
							try {
								Object value = rs.getObject(tableName
										+ f.getAnnotation(DataBaseField.class)
												.value());
								if (value == null
										&& f.isAnnotationPresent(Id.class))
									return null;
								f.set(obj, value);
							} catch (IllegalArgumentException | SQLException e) {
								e.printStackTrace();
							}
					}
				}
			} catch (InstantiationException | IllegalAccessException
					| SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return obj;
		}
	}

	private enum JoinType {
		inner(" INNER "), left(" LEFT "), right(" RIGHT "), outer(" OUTER ");

		String joinType;

		JoinType(String type) {
			this.joinType = type;
		}
	}

	private class PrepStatCreator implements PreparedStatementCreator {

		@Override
		public PreparedStatement createPreparedStatement(Connection con)
				throws SQLException {
			PreparedStatement ps = con.prepareStatement(query.toString());
			for (int i = 0; i < ParametersCount; i++) {
				ps.setObject(i + 1, parameters.get(i));
			}
			System.out.println(ps.toString());
			return ps;
		}

	}
}
