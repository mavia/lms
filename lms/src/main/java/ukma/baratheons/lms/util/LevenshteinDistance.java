package ukma.baratheons.lms.util;

public class LevenshteinDistance {
	
	/**
	 * Calculates levenshtein distance
	 * @param str1 - the first string to compare
	 * @param str2 - the second string to compare
	 * */
	public static int editdist(final String str1, final String str2) {
		
		if(str1 == null)
			throw new IllegalArgumentException("str1 == null");
		
		if(str2 == null)
			throw new IllegalArgumentException("str2 == null");
		
		int m = str1.length(), n = str2.length();
		int[] D1;
		int[] D2 = new int[n + 1];

		for(int i = 0; i <= n; i ++)
			D2[i] = i;

		for(int i = 1; i <= m; i ++) {
			D1 = D2;
			D2 = new int[n + 1];
			for(int j = 0; j <= n; j ++) {
				if(j == 0) D2[j] = i;
				else {
					int cost = (str1.charAt(i - 1) != str2.charAt(j - 1)) ? 1 : 0;
					if(D2[j - 1] < D1[j] && D2[j - 1] < D1[j - 1] + cost)
						D2[j] = D2[j - 1] + 1;
					else if(D1[j] < D1[j - 1] + cost)
						D2[j] = D1[j] + 1;
					else
						D2[j] = D1[j - 1] + cost;
				}
			}
		}
		return D2[n];
	}

}
