package ukma.baratheons.lms.util;

import java.util.regex.Pattern;

/**
 * This class holds useful utilities
 * to work with email addresses
 * @author Max Oliynick
 * */

public class EmailUtils {
	
	/**Holds list of the most famous email domens*/
	public static final String [] EMAILS = new String [] {
		"gmail.com",
		"outlook.com",
		"ukr.net",
		"mail.ru",
		"hotmail.ru",
		"rambler.ru",
		"i.ua",
		"yandex.ru"
	};
	
	/**
	 * email regular expression to check email
	 * for validity
	 * */
	public static final Pattern EMAIL_PATTERN = Pattern.compile("^[\\p{L}\\p{N}\\._%+-]+@[\\p{L}\\p{N}\\.\\-]+\\.[\\p{L}]{2,}$");
	
	/**
	 * Indicates whether given email is valid or not
	 * @param email - email to check
	 * @return true if given email is valid and false
	 * in another case
	 * */
	public static boolean isValidEmail(final String email) {
		if(email == null) return false;
		
		return email.matches(EMAIL_PATTERN.toString());
	}
}
