package ukma.baratheons.lms.util;

/**
 * This class represents an
 * utility class to simplify
 * bit operations
 * @author Max Oliynick
 * */
public class BitUtils {
	
	/**
	 * Holds bitmasks for int type
	 * */
	public static class BitMask {
		/**Represents one bit*/
		public static final int BIT = 0x1;
		/**Represents the first byte in the number*/
		public static final int BYTE_1 = 0xFF;
		/**Represents the second byte in the number*/
		public static final int BYTE_2 = BYTE_1 << 8;
		/**Represents the third byte in the number*/
		public static final int BYTE_3 = BYTE_2 << 8;
		/**
		 * Represents the fourth byte in the number
		 * without with its sign
		 * */
		public static final int BYTE_4 = BYTE_3 << 7;
		/**
		 * Represents the fourth byte in the number
		 * together with its sign
		 * */
		public static final int BYTE_4_S = BYTE_3 << 8;
	}
	
	/**
	 * Checks whether bit on the specified position is
	 * set to 1
	 * @param pos - position to check
	 * @param number - number to check
	 * @return true if bit is set to 1 and
	 * false in another case
	 * */
	public static boolean isBitSet(int pos, int number) {
		return (number >> ((pos % 32) - 1) & 1) == 1;
	}
	
	/**
	 * Sets bit on the specified position to 1
	 * @param pos - position to set
	 * @param number - given number
	 * */
	public static int setBit(int pos, int number) {
		return number | (BitMask.BIT << (pos % 32) - 1);
	}
	
	/**
	 * Sets bytes using given mask
	 * @param mask - mask to use
	 * @param number - given number
	 * */
	public static int setByMask(int mask, int number) {
		return number | mask;
	}
	
	/**
	 * Unsets bytes using given mask
	 * @param mask - mask to use
	 * @param number - given number
	 * */
	public static int unsetByMask(int mask, int number) {
		return number &~ mask;
	}
	
	/**
	 * Sets bit on the specified position to 0
	 * @param pos - position to set
	 * @param number - given number
	 * */
	public static int unsetBit(int pos, int number) {
		return number & ~(BitMask.BIT << (pos % 32) - 1);
	}

}