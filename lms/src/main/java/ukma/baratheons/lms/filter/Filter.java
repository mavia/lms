package ukma.baratheons.lms.filter;

import java.util.HashMap;
import java.util.Map;

import ukma.baratheons.lms.query.Table;

public abstract class Filter {

	public static final HashMap<String, String> user = new HashMap<String, String>();
	public static final HashMap<String, String> room = new HashMap<String, String>();

	static{
		user.put("userId", Table.UserTable.ID);
		user.put("name", Table.UserTable.FIRST_NAME);
	}
	
	public static String getQueryTail(Map<String,String> fields, Map<String, String> values){
		if(values==null||values.size()<1)return null;
		StringBuilder queryTail = new StringBuilder();
		queryTail.delete(0, queryTail.length());
		 for(String key:values.keySet()){
			 switch(key.substring(0,4)){
			 case "less":
				 if(fields.containsKey(key.substring(4)))
					 queryTail.append(" "+fields.get(key.substring(4))+" < '"+values.get(key)+"' AND ");
				 break;
			 case "more":
				 if(fields.containsKey(key.substring(4)))
				 	queryTail.append(" "+fields.get(key.substring(4))+" > '"+values.get(key)+"' AND ");
				 break;
			 default:
				 if(fields.containsKey(key))
					 queryTail.append(" "+fields.get(key)+" = '"+values.get(key)+"' AND ");
			 }
		 }
		 if(queryTail.length()==0)return null;
		 return queryTail.toString().substring(0, queryTail.length()-4);
	 }
}