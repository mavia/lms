package ukma.baratheons.lms.query;

import java.util.List;

public interface SearchEngine <R> {

	public List<R> search(int rights, final String query);
	
}
