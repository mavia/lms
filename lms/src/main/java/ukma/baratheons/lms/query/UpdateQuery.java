package ukma.baratheons.lms.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ukma.baratheons.lms.query.Join.JoinType;
import ukma.baratheons.lms.query.Logical.Operator;

/**
 * Class which represents an UPDATE SQL query
 * */
public class UpdateQuery implements QueryStatement {
	
	private List<String> tables;
	private List<String> from;
	private CSVConjunction set;
	private Where where;
	private List<Join> joins;
	
	/**
	 * Constructs an UpdateQuery object. This
	 * query will update specified tables
	 * @param tables - tables to update. Note
	 * that this parameter shouldn't be null
	 * or empty, in another case {@link IllegalArgumentException}
	 * will be thrown
	 * */
	public UpdateQuery(final String... tables) {
		
		if(tables == null) {
			throw new IllegalArgumentException("tables == null");
		}
		
		if(tables.length == 0) {
			throw new IllegalArgumentException("tables weren't specified");
		}
		
		this.tables = Arrays.asList(tables);
	}
	
	/**
	 * Specifies which tables to select from
	 * 
	 * @param from - tables to select from. Note
	 * that this value shouldn't be null of 
	 * {@link IllegalArgumentException} will
	 * be thrown
	 * @return {@linkplain UpdateQuery} 
	 */
	public UpdateQuery from(String... from) {
		if(from == null)
			throw new IllegalArgumentException("from == null");
		
		this.from = Arrays.asList(from);
		return this;
	}
	
	/**
	 * Appends set statement to this query. The sql
	 * representation is {@code "column=value"}.
	 * Note that both of these parameter shouldn't be null,
	 * in another case {@link IllegalArgumentException} will
	 * be thrown
	 * @param columnn - column to add
	 * @param value - value to add
	 * @return {@linkplain UpdateQuery} 
	 * */
	public UpdateQuery set(final String columnn, final Object value) {
		
		if(set == null) {
			set = new CSVConjunction(columnn, value);
		} else {
			set.add(columnn,value);
		}
		
		return this;
	}
	

	/**
	 * Appends set statement to this query. The sql
	 * representation is {@code "column=value"}.
	 * Value parameter can be specified later using prepared
	 * statement
	 * @param columnn - column to add
	 * @return {@linkplain UpdateQuery} 
	 * */
	public UpdateQuery set(final String column) {
		set(column, String.valueOf('?'));
		return this;
	}
	
	/**
	 * Appends INNER JOIN statement to this query.
	 * Note that none of given parameters can be 
	 * null, or an {@linkplain IllegalArgumentException}
	 * will be thrown
	 * 
	 * @param table table to commit join with
	 * @param column1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param column2 column from the second table in join
	 * @return {@linkplain UpdateQuery} 
	 */
	public UpdateQuery innerJoin(final String table, final String column1, final Operator operator, final String column2) {
		
		appendJoin(table, null, column1, operator, column2, JoinType.INNER);
		return this;
	}
	
	/**
	 * Appends INNER JOIN statement to this query.
	 * Note that none of given parameters except
	 * alias can be 
	 * null, or an {@linkplain IllegalArgumentException}
	 * will be thrown
	 * 
	 * @param table table to commit join with
	 * @param column1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param column2 column from the second table in join
	 * @param alias - table's alias
	 * @return {@linkplain UpdateQuery} 
	 */
	public UpdateQuery innerJoin(final String table, final String alias,
			final String column1, final Operator operator,
			final String column2) {
		appendJoin(table, alias, column1, operator, column2, JoinType.INNER);
		return this;
	}

	/**
	 * Appends LEFT JOIN statement to this query.
	 * Note that none of given parameters can be 
	 * null, or an {@linkplain IllegalArgumentException}
	 * will be thrown
	 * 
	 * @param table - table to commit join with
	 * @param column1 - column from the first table in join
	 * @param operator - operator for join comparison
	 * @param column2 - column from the second table in join
	 * @return {@linkplain UpdateQuery} 
	 */
	public UpdateQuery leftJoin(final String table, final String column1, final Operator operator, final String column2) {
		appendJoin(table, null, column1, operator, column2, JoinType.LEFT);
		return this;
	}
	
	/**
	 * Appends LEFT JOIN statement to this query.
	 * Note that none of given parameters except
	 * alias can be 
	 * null, or an {@linkplain IllegalArgumentException}
	 * will be thrown
	 * 
	 * @param table - table to commit join with
	 * @param column1 - column from the first table in join
	 * @param operator - operator for join comparison
	 * @param column2 - column from the second table in join
	 * @param alias - table's alias
	 * @return UpdateQuery
	 */
	public UpdateQuery leftJoin(final String table, final String alias,
			final String column1, final Operator operator,
			final String column2) {
		appendJoin(table, alias, column1, operator, column2, JoinType.LEFT);
		return this;
	}
	
	/**
	 * Appends RIGHT JOIN statement to this query.
	 * Note that none of given parameters 
	 * null, or an {@linkplain IllegalArgumentException}
	 * will be thrown
	 * 
	 * @param table - table to commit join with
	 * @param column1 - column from the first table in join
	 * @param operator - operator for join comparison
	 * @param column2 - column from the second table in join
	 * @return UpdateQuery
	 */
	public UpdateQuery rightJoin(final String table, final String column1, final Operator operator, final String column2) {
		appendJoin(table, null, column1, operator, column2, JoinType.RIGHT);
		return this;
	}

	/**
	 * Appends RIGHT JOIN statement to this query.
	 * Note that none of given parameters except
	 * alias can be 
	 * null, or an {@linkplain IllegalArgumentException}
	 * will be thrown
	 * 
	 * @param table - table to commit join with
	 * @param column1 - column from the first table in join
	 * @param operator - operator for join comparison
	 * @param column2 - column from the second table in join
	 * @param alias - table's alias
	 * @return UpdateQuery
	 */
	public UpdateQuery rightJoin(final String table, final String alias,
			final String column1, final Operator operator,
			final String column2) {
		appendJoin(table, alias, column1, operator, column2, JoinType.RIGHT);
		return this;
	}
	
	/**
	 * Appends FULL OUTER JOIN statement to this query.
	 * Note that none of given parameters can be 
	 * null, or an {@linkplain IllegalArgumentException}
	 * will be thrown
	 * 
	 * @param table - table to commit join with
	 * @param column1 - column from the first table in join
	 * @param operator - operator for join comparison
	 * @param column2 - column from the second table in join
	 * @return UpdateQuery
	 */
	public UpdateQuery fullJoin(final String table, final String column1, final Operator operator, final String column2) {
		appendJoin(table, null, column1, operator, column2, JoinType.FULL);
		return this;
	}
	
	/**
	 * Appends FULL OUTER JOIN statement to this query.
	 * Note that none of given parameters except
	 * alias can be 
	 * null, or an {@linkplain IllegalArgumentException}
	 * will be thrown
	 * 
	 * @param table - table to commit join with
	 * @param column1 - column from the first table in join
	 * @param operator - operator for join comparison
	 * @param column2 - column from the second table in join
	 * @param alias - table's alias
	 * @return UpdateQuery
	 */
	public UpdateQuery fullJoin(final String table, final String alias,
			final String column1, final Operator operator,
			final String column2) {
		appendJoin(table, alias, column1, operator, column2, JoinType.FULL);
		return this;
	}

	private void appendJoin(final String table, final String alias,
			final String column1, final Operator operator,
			final String column2, final JoinType type) {
		
		if(table == null)
			throw new IllegalArgumentException("table == null");
		
		if(column1 == null)
			throw new IllegalArgumentException("column1 == null");
		
		if(column2 == null)
			throw new IllegalArgumentException("column2 == null");
		
		if(operator == null)
			throw new IllegalArgumentException("operator == null");
		
		if(type == null)
			throw new IllegalArgumentException("type == null");
		

		if (joins == null) {
			joins = new ArrayList<Join>();
		}

		joins.add(new Join(table, alias, type, new Logical(column1, operator, column2)));
	}
	
	public UpdateQuery where(final String columnn, final Operator operator,final Object value) {
		where = new Where(new Logical(columnn, operator, value));
		return this;
	}
	
	public UpdateQuery andWhere(final String columnn, final Operator operator,final Object value) {
		where.and(columnn, operator, value);
		return this;
	}
	
	public UpdateQuery orWhere(final String columnn, final Object value) {
		where.or(columnn, Operator.EQ, value);
		return this;
	}

	@Override
	public String toSql() {
		
		StringBuilder sql = new StringBuilder("UPDATE");
		
		for(String table : tables) {
			sql.append(' ').append(table).append(',');
		}
		
		sql.setCharAt(sql.length() - 1, ' ');
		
		sql.append("SET").append(set.toSql());
		
		if(from != null) {
			sql.append("FROM");
			for(String fr : from) {
				sql.append(' ').append(fr).append(',');
			}
			
			sql.setCharAt(sql.length() - 1, ' ');
		}
		
		if(joins != null) {
			for(Join join : joins) {
				sql.append(join.toSql()).append(' ');
			}
		}
		
		if(where != null) {
			sql.append("WHERE ").append(where.toSql());
		}
		
		return sql.toString();
	}

}