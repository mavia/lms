package ukma.baratheons.lms.query;

import ukma.baratheons.lms.query.Logical.Operator;

/**
 * Constructs SQL 'WHERE' statement
 * @author Max Oliynick
 * */

public class WhereBuilder {
	
	/**
	 * Constructs an empty 'WHERE' statement
	 * */
	public static Where where() {
		return new Where();
	}
	
	/**
	 * Constructs 'WHERE val1 operator val2' statement using logical expression such as
	 * 'WHERE salary >= 100' where salary is val1, >= is operator,
	 * 100 is val2
	 * @param var1 - the first value
	 * @param operator - logical operator
	 * @param var2 - the second value
	 * @return {@link Where}
	 * */
	public static Where where(final Object var1, final Operator operator, final Object var2) {
		return new Where(new Logical(var1, operator, var2));
	}
	
	/**
	 * Constructs 'WHERE val1 operator val2' statement using logical expression such as
	 * 'WHERE salary >= '100'' where salary is val1, >= is operator,
	 * 100 is val2, ' around 100 is character wrapper
	 * @param var1 - the first value
	 * @param operator - logical operator
	 * @param var2 - the second value
	 * @param wrappper - an optional wrapper to use
	 * @return {@link Where}
	 * */
	public static Where where(final Object var1, final Operator operator, final Object var2, final Character wrappper) {
		return new Where(new Logical(var1, operator, var2, wrappper));
	}
	
	/**
	 * Constructs 'WHERE var IN (arg1, arg2, arg3)' statement using 'IN' expression such as
	 * 'WHERE salary IN (100, 200, 300)' where salary is var, 100, 200, 300 - given set
	 * @param var - given variable
	 * @param args - given set of arguments
	 * @return {@link Where}
	 * */
	public static Where where(final String var, final Object...args) {
		return new Where(new In(var, args));
	}
	
	/**
	 * Constructs 'WHERE var IN (selectQuery)' statement using 'IN' expression such as
	 * 'WHERE salary IN (SELECT * FROM "salaries")' where salary is var, SELECT * FROM "salaries"
	 * is given query
	 * @param var - given variable
	 * @param query - given select query
	 * @return {@link Where}
	 * */
	public static Where where(final String var, final SelectQuery query) {
		return new Where(new In(var, query));
	}
	
	/**
	 * Constructs 'WHERE var LIKE pattern' statement using 'LIKE' expression such as
	 * 'WHERE name LIKE '%kma%'' where name is var, '%kma%' "
	 * is given search pattern
	 * @param var - given variable
	 * @param pattern - given pattern
	 * @return {@link Where}
	 * */
	public static Where where(final String var, final Object pattern) {
		return new Where(new Like(var, String.valueOf(pattern)));
	}

}
