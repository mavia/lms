package ukma.baratheons.lms.query;

import java.util.Arrays;
import java.util.List;

import org.springframework.util.Assert;


/**
 * Class which represents a INSERT SQL query
 * */
public class InsertQuery implements QueryStatement {
	
	private final Alias alias;
	private List<String> columns;
	private List<Object> values;
	
	private Character wrapper;
	
	public InsertQuery(final String table) {
		this(table, null);
	}
	
	public InsertQuery(final String table, final Character vWrapper) {
		if(table == null)
			throw new IllegalArgumentException("table == null");
		
		this.wrapper = vWrapper;
		this.alias = new Alias(table, null, '\"');
	}
	
	public InsertQuery columns(final String... columns) {
		
		if(this.columns == null) {
			this.columns = Arrays.asList(columns);
			
		} else {
			this.columns.addAll(Arrays.asList(columns));
			
		}
		
		return this;
	}
	
	public InsertQuery values(final Object... values) {
		
		if(this.values == null) {
			this.values = Arrays.asList(values);
			
		} else {
			this.values.addAll(Arrays.asList(values));
			
		}
		
		return this;
	}

	@Override
	public String toSql() {
		
		StringBuilder sql = new StringBuilder("INSERT INTO ");
		sql.append(alias.toSql());
		
		if(columns != null) {
			sql.append(" (");
			for(String column : columns) {
				sql.append(column).append(',');
			}
			sql.setCharAt(sql.length() - 1, ')');
		}
		
		Assert.notNull(values, "insert values weren't specified!");
		
		sql.append(" VALUES");
		sql.append(" (");
		
		for(Object value : values) {
			if(wrapper == null)
				sql.append(value).append(',');
			else
				sql.append(wrapper).append(value).append(wrapper).append(',');
		}
		sql.setCharAt(sql.length() - 1, ')');
		
		return sql.toString();
	}

}



