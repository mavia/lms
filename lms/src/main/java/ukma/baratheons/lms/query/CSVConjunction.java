package ukma.baratheons.lms.query;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

/**
 * This class represents simple comma-separated values.
 * Example: "....table="test", email="example@mail.com"..."
 * and so on
 * */
public class CSVConjunction implements QueryStatement {
	
	private static char WRAPPER = '\'';
	
	public static void setDefaultTableWrapper(char newWrapper) {
		WRAPPER = newWrapper;
	}
	
	public static char getDefaulTableWrapper() {
		return WRAPPER;
	}
	
	private static class ValKeeper {
		
		private final String column;
		private final String value;
		
		public ValKeeper(final String column, final String value) {
			this.column = column;
			this.value = value;
		}
		
		/**
		 * Copy constructor
		 * @param another - object to copy
		 * */
		public ValKeeper(final ValKeeper another) {
			
			Assert.notNull(another.column);
			Assert.notNull(another.value);
			
			this.column = another.column;
			this.value = another.value;
		}
		
		public String getColumn() {
			return column;
		}

		public String getValue() {
			return value;
		}
		
	}

	private List<ValKeeper> csv;
	
	/**
	 * Builds a new object with specified column and value. Note
	 * that none if these parameter can be null,
	 * in another case {@link IllegalArgumentException}
	 * will be raised
	 * @param column - column to append
	 * @param value - value to append
	 * */
	public CSVConjunction(final String column, final Object value) {
		add(column, String.valueOf(value));
	}
	
	/**
	 * Builds a new object with specified columns and values. Note
	 * that parameter cannot be null,
	 * in another case {@link IllegalArgumentException}
	 * will be raised
	 * @param cvs - list of input columns and its values
	 * */
	public CSVConjunction(final List<ValKeeper> cvs) {
		
		if(cvs == null)
			throw new IllegalArgumentException("cvs == null");
		
		this.csv = new ArrayList<ValKeeper>(cvs.size());
		//cvs.forEach(item -> this.csv.add(new ValKeeper(item)));
	}
	
	public CSVConjunction(final CSVConjunction another) {
		this.csv = new ArrayList<ValKeeper>(another.csv.size());
		
		//another.csv.forEach(item -> csv.add(new ValKeeper(item)));
	}
	
	/**
	 * Adds one comma separated operator to sql query.
	 * Note that none if these parameter cannot be null,
	 * in another case {@link IllegalArgumentException}
	 * will be raised
	 * @param column - column to append
	 * @param value - value to append
	 * @return ComplexCSVConjunction
	 * */
	public CSVConjunction add(final String column, final Object value) {
		
		if(column == null)
			throw new IllegalArgumentException("column == null");
		
		if(value == null)
			throw new IllegalArgumentException("value == null");
		
		if(csv == null) {
			csv = new ArrayList<ValKeeper>(1);
		}
		
		csv.add(new ValKeeper(column, String.valueOf(value)));
		
		return this;
	}
	
	@Override
	public String toSql() {

		final StringBuilder sql = new StringBuilder();

		for (ValKeeper keeper : csv) {
			sql.append(' ').append(keeper.getColumn()).
			append('=').append(WRAPPER).
			append(keeper.getValue()).
			append(WRAPPER).append(',');
		}

		sql.setCharAt(sql.length() - 1, ' ');

		return sql.toString();
	}
	
}
