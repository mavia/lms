package ukma.baratheons.lms.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ukma.baratheons.lms.query.Logical.Operator;

/**
 * This class represents an sql 'WHERE' operator
 * operators
 * */
public class Where implements QueryStatement {
	
	/**represents common sql conjunctions*/
	public static enum Conjunction {
		AND, OR
	}

	private List<QueryStatement> logicals;

	private List<Conjunction> conjunctions;
	
	private SubSqlAlias subAlias;
	
	private List<In> in;

	public Where() {}
	
	public Where(final In in) {
		insertIn(in);
	}

	public Where(final Logical log) {
		insertLog(log);
	}
	
	public Where(final Like like) {
		insertLog(like);
	}
	
	/**
	 * Copy constructor
	 * @param another - object to copy
	 * */
	public Where(final Where another) {
		
		if(another.logicals != null) {
			this.logicals = new ArrayList<>(another.logicals.size());
			
			// FIXME should be a proper copying
			for(final QueryStatement item : another.logicals) {
				
				if(item instanceof Logical) {
					logicals.add(new Logical((Logical) item));
					
				} else if(item instanceof Like) {
					logicals.add(new Like((Like) item));
				}
			}
			
		}
		
		if(another.conjunctions != null) {
			this.conjunctions = new ArrayList<Conjunction>(another.conjunctions.size());
			//another.conjunctions.forEach(item -> conjunctions.add(item));
		}
		
		if(another.subAlias != null) {
			this.subAlias = new SubSqlAlias(another.subAlias);
		}
		
		if(another.in != null) {
			this.in = new ArrayList<In>(another.in.size());
			//another.in.forEach(item -> in.add(new In(item)));
		}
		
	}

	public Where and(final Object val1, final Operator operator, final Object val2) {
		return and(val1, operator, val2, null);
	}
	
	public Where or(final Object val1, final Operator operator, final Object val2) {
		return insertConj(Conjunction.OR).insertLog(new Logical(val1, operator, val2, null));
	}
	
	public Where and(final Object val1, final Operator operator, final Object val2, final Character wrapper) {
		return insertConj(Conjunction.AND).insertLog(new Logical(val1, operator, val2, wrapper));
	}

	public Where or(final Object val1, final Operator operator, final Object val2, final Character wrapper) {
		return insertConj(Conjunction.OR).insertLog(new Logical(val1, operator, val2, wrapper));
	}
	
	public Where in(final Conjunction conj, final In in) {
		return insertConj(conj).insertIn(in);
	}
	
	public Where like(final String target, final Object pattern) {
		return insertLog(new Like(target, String.valueOf(pattern)));
	}
	
	public Where andLike(final String target, final Object pattern) {
		return insertConj(Conjunction.AND).like(target, pattern);
	}
	
	public Where orLike(final String target, final Object pattern) {
		return insertConj(Conjunction.OR).like(target, pattern);
	}

	public Where subSql(final QueryStatement subSql, final String alias) {
		subAlias = new SubSqlAlias(subSql, alias);
		return this;
	}
	
	private Where insertLog(final QueryStatement log) {
		
		if(log == null)
			throw new IllegalArgumentException("log == null");
		
		if(logicals == null) {
			logicals = new ArrayList<>();
		}
		
		logicals.add(log);
		
		return this;
	}
	
	private Where insertIn(final In in) {
		
		if(in == null)
			throw new IllegalArgumentException("in == null");
		
		if(this.in == null) {
			this.in = Arrays.asList(in);
			
		} else {
			this.in.add(in);
		}
		
		return this;
	}
	
	private Where insertConj(final Conjunction conj) {
		
		if(conj == null)
			throw new IllegalArgumentException("conj == null");
		
		if(conjunctions == null) {
			conjunctions = new ArrayList<>();
		} 
		
		conjunctions.add(conj);
		
		return this;
	}

	@Override
	public String toSql() {
		
		StringBuilder sql = new StringBuilder();
		
		if(logicals != null) {
			
			sql.append(logicals.get(0).toSql()).append(' ');
		
			for (int i = 1; i < logicals.size(); i++) {
				sql.append(conjunctions.get(i - 1).toString()).append(' ').append(logicals.get(i).toSql()).append(' ');
			}
			sql.setLength(sql.length() - 1);
		}
		
		if(subAlias != null) {
			sql.append(subAlias.toSql());
		}
		
		if(in != null) {
			for(In in : in) {
				sql.append(in.toSql());
			}
		}
		
		return sql.toString();
	}

}