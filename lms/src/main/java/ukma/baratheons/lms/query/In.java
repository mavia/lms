package ukma.baratheons.lms.query;

import java.util.Arrays;

/**
 * This class represents sql in statement
 * */

public class In implements QueryStatement {
	
	private final String variable;
	private SelectQuery query;
	private String [] args;
	
	/**
	 * Constructs sql 'IN' statement using
	 * sub query
	 * @param var - variable before 'IN' statement
	 * @param query - sub query
	 * */
	public In(final String var, final SelectQuery query) {
		
		if(var == null)
			throw new IllegalArgumentException("var == null");
		
		if(query == null)
			throw new IllegalArgumentException("query == null");
		
		this.variable = var;
		this.query = query;
	}
	
	/**
	 * Constructs sql 'IN' statement using
	 * sub query
	 * @param var - variable before 'IN' statement
	 * @param args - arguments set
	 * */
	public In(final String var, Object...args) {
		
		if(var == null)
			throw new IllegalArgumentException("var == null");
		
		if(args == null)
			throw new IllegalArgumentException("args == null");
		
		this.variable = var;
		this.args = new String[args.length];
		
		for(int i = 0; i < args.length; ++i) {
			this.args[i] = String.valueOf(args[i]);
		}
	}
	
	/**
	 * Copy constructor
	 * @param another - object to copy
	 * */
	public In(final In another) {
		this.variable = another.variable == null ? null : new String(another.variable);
		this.query = another.query == null ? null : new SelectQuery(another.query);
		
		if(another.args != null)
			this.args = Arrays.copyOf(another.args, another.args.length);
	}

	@Override
	public String toSql() {
		
		StringBuilder sql = new StringBuilder(variable);
		
		if(args == null) {
			sql.append(" IN (").append(query.toSql()).append(')');
			
		} else {
			
			sql.append(" IN (");
			
			for(String arg : args) {
				sql.append('\'').append(arg).append("\',");
			}
			
			sql.setCharAt(sql.length() - 1, ' ');
			sql.append(')');
			
		}
		
		return sql.toString();
	}

}
