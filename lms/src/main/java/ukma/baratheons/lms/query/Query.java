package ukma.baratheons.lms.query;

public class Query {

	/**
	 * @param columns - columns to select
	 * @see {@link SelectQuery}, {@link UpdateQuery}, {@link InsertQuery}, {@link DeleteQuery}
	 * @return SelectQuery
	 * */
	public static SelectQuery select(String... columns) {
		return new SelectQuery(columns.length == 0 ? new String[]{"*"} : columns);
	}
	
	/**
	 * @param tables - tables to update
	 * @see {@link SelectQuery}, {@link UpdateQuery}, {@link InsertQuery}, {@link DeleteQuery}
	 * @return UpdateQuery
	 * */
	public static UpdateQuery update(String... tables) {
		return new UpdateQuery(tables);
	}
	
	/**
	 * @param table - table to insert in
	 * @see {@link SelectQuery}, {@link UpdateQuery}, {@link InsertQuery}, {@link DeleteQuery}
	 * @return InsertQuery
	 * */
	public static InsertQuery insert(String table) {
		return new InsertQuery(table);
	}
	
	/**
	 * @param table - table to delete from
	 * @see {@link SelectQuery}, {@link UpdateQuery}, {@link InsertQuery}, {@link DeleteQuery}
	 * @return DeleteQuery
	 * */
	public static DeleteQuery delete(String table) {
		return new DeleteQuery(table);
	}
	
}
