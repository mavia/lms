package ukma.baratheons.lms.query;

/**
 * Builds select sql query
 * 
 * @author Max Oliynick
 * */

public class SelectQueryBuilder {
	
	/**
	 * Creates SELECT query with specified columns to select
	 * 
	 * @param columns names of columns to select
	 */
	public static SelectQuery selectQuery(String...columns) {
		return new SelectQuery(columns);
	}

}
