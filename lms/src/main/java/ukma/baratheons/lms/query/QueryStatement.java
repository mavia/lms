package ukma.baratheons.lms.query;

/**
 * This interface represents
 * an abstract sql query statement
 * */
public interface QueryStatement {

	/**
	 * Returns full sql representation
	 * of a sql query
	 * */
	public String toSql();
	
}
