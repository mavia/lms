package ukma.baratheons.lms.query;


/**
 * This class represents logical operator
 * in sql query
 * */
public class Logical implements QueryStatement {
	
	/**Represents common operator types in sql*/
	public static enum Operator {
		EQ("="), LT("<"), GT(">"), NEQ("!="), LE("<="), GE(">=");
		private String sql;

		private Operator(String name) {
			this.sql = name;
		}

		@Override
		public String toString() {
			return sql;
		}
	}

	private final String val1, val2;

	private final Operator operator;
	private final Character wrapper;

	/**
	 * Builds logical object with given parameters
	 * @param val1 - the first value
	 * @param operator - operator type
	 * @param val2 - the seconds value
	 * */
	public Logical(Object val1, Operator operator, Object val2) {
		this(val1, operator, val2, null);
	}
	
	/**
	 * Builds logical object with given parameters
	 * @param val1 - the first value
	 * @param operator - operator type
	 * @param val2 - the seconds value
	 * */
	public Logical(Object val1, Operator operator, Object val2, final Character wrapper) {
		this.val1 = new String(val1.toString());
		this.operator = operator;
		this.val2 = new String(val2.toString());
		this.wrapper = wrapper;
	}

	/**
	 * Copy constructor
	 * @param another - object to copy
	 * */
	public Logical(final Logical another) {
		this.val1 = another.val1;
		this.val2 = another.val2;
		this.operator = another.operator;
		this.wrapper = another.wrapper;
	}
	
	@Override
	public String toSql() {
		StringBuilder sql = new StringBuilder(val1.toString());
		sql.append(' ').append(operator);
		
		if(wrapper == null) {
			sql.append(val2);
		} else {
			sql.append(wrapper).append(val2).append(wrapper);
		}
		
		return sql.toString();
	}
	
}
