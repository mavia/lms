package ukma.baratheons.lms.query;

/**
 * This class represents sub sql query
 * */

public class SubSqlAlias extends Alias {

	/**
	 * Builds a new SubSqlAlias instance with given parameters
	 * @param subSql sub query
	 * @param alias alias to use
	 * */
	public SubSqlAlias(final QueryStatement subSql, final String alias) {
		super(subSql.toSql(), alias);
	}

	/**
	 * Copy constructor
	 * @param another - object to copy
	 * */
	public SubSqlAlias(final SubSqlAlias another) {
		super(another.target, another.alias);
	}

	@Override
	public String toSql() {
		
		StringBuilder sql = new StringBuilder();
		
		sql.append('(').append(super.target).
		append(") AS ").append(super.alias);
		
		return sql.toString();
	}
	
}
