package ukma.baratheons.lms.query;

public class Table {

	public static class UserTable {
		public static final String TABLE_NAME = "users";
		public static final String ID = "user_id";
		public static final String EMAIL = "email";
		public static final String FIRST_NAME = "first_name";
		public static final String LAST_NAME = "last_name";
		public static final String LAST_ONLINE = "last_online";
		public static final String REGISTRATION_DATE = "reg_date";
		public static final String PASSWORD = "password";
		public static final String IS_ACTIVE = "is_active";
		public static final String PENALTY_SCORE = "penalty_score";
		public static final String RIGHTS = "rights";
		public static final String ROLE = "role_id";
		public static final String RECEIVE_EMAIL = "receive_email";
		public static final String BAN_END_TIME = "ban_end_time";
	}

	public static class TaskTable {
		public static final String PARENT_TASK = "parent_id";
		public static final String TABLE_NAME = "task";
		public static final String ID = "task_id";
		public static final String STATUS = "task_status_id";
		public static final String TYPE = "task_type_id";
		public static final String EQUIPMENT = "eqpt_id";
		public static final String CREATOR = "user_id";
		public static final String STAFF = "staff_id";
		public static final String CREATION_TIME = "creation_time";
		public static final String COMPLETION_TIME = "completion_time";
		public static final String URGENCY = "urgency";
		public static final String VISIBILITY = "visibility";
	}

	public static class UserRoleTable {
		public static final String TABLE_NAME = "role";
		public static final String ID = "role_id";
		public static final String ROLE_NAME = "namel";
		public static final String DEFAULT_RIGHTS = "def_rights";
	}

	public static class TaskStatusTable {
		public static final String TABLE_NAME = "task_status";
		public static final String ID = "task_status_id";
		public static final String NAME = "namel";
	}

	public static class TaskTypeTable {
		public static final String TABLE_NAME = "task_type";
		public static final String ID = "task_type_id";
		public static final String NAME = "namel";
	}

	public static class EquipmentTable {
		public static final String TABLE_NAME = "equipment";
		public static final String ID = "eqpt_id";
		public static final String LOCAL_NAME = "local_num";
		public static final String IS_DECOMMISSIONED = "is_decommissioned";
		public static final String TYPE = "eqpt_type_id";
		public static final String ROOM = "room_id";
	}

	public static class EquipmentTypeTable {
		public static final String TABLE_NAME = "equipment_type";
		public static final String ID = "eqpt_type_id";
		public static final String NAME = "namel";
	}

	public static class RoomTable {
		public static final String TABLE_NAME = "room";
		public static final String ID = "room_id";
		public static final String NUMBER = "number";
		public static final String STAFF = "user_id";
	}

	public static class CommentTable {
		public static final String TABLE_NAME = "comment";
		public static final String ID = "cmnt_id";
		public static final String TEXT = "text";
		public static final String DATE = "date";
		public static final String VISIBILITY = "visibility";
		public static final String USER = "user_id";
		public static final String TASK = "task_id";
	}

	public static class LogTable {
		public static final String TABLE_NAME = "log";
		public static final String ID = "log_id";
		public static final String ACTION = "action_type";
		public static final String DATE = "date";
		public static final String TEXT = "expression";
		public static final String USER = "user_id";
	}

	public static class BanTimeTable {
		public static final String TABLE_NAME = "ban_time";
		public static final String ID = "ban_id";
		public static final String PENALTY_SCORE = "penalty_score";
		public static final String DATE = "date";
		public static final String USER = "user_id";
	}

	public static class UnconfirmedTable {
		public static final String TABLE_NAME = "unconfirmed";
		public static final String ID = "unconf_id";
		public static final String HASH = "hash";
		public static final String EMAIL = "email";
		public static final String EXPIRATION_DATE = "exp_date";
		public static final String TYPE = "type";
		public static final String ROLE = "role_id";
	}
}
