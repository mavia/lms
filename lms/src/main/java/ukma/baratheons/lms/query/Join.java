package ukma.baratheons.lms.query;

import java.util.ArrayList;
import java.util.List;

import ukma.baratheons.lms.query.Logical.Operator;
import ukma.baratheons.lms.query.Where.Conjunction;

/**
 * Class which represents a JOIN SQL
 * */
public class Join implements QueryStatement {
	
	/**Represents sql join type*/
	public static enum JoinType {
		INNER, LEFT, RIGHT, FULL
	}

	private Alias alias;
	private SubSqlAlias subAlias;
	
	private final JoinType type;

	private List<Conjunction> conjunctions;
	private List<Logical> conditions;
	
	public Join(final String table, final JoinType type, final Logical condition) {
		this(table, null, type, condition);
	}

	public Join(final String table, final String alias,
			final JoinType type, final Logical condition) {
		this.alias = new Alias(table, alias, '\"');
		this.type = type;
		
		appendCondition(condition);
	}
	
	public Join(final QueryStatement sub, final String alias,
			final JoinType type, final Logical condition) {
		this.subAlias = new SubSqlAlias(sub, alias);
		this.type = type;
		
		appendCondition(condition);
	}
	
	/**
	 * Copy constructor
	 * @param another - object to copy
	 * */
	public Join(final Join another) {
		
		if(another.alias != null)
			this.alias = new Alias(another.alias);
		
		if(another.subAlias != null)
			this.subAlias = new SubSqlAlias(another.subAlias);
		
		this.type = another.type;
		
		if(another.conjunctions != null) {
			conjunctions = new ArrayList<Conjunction>(another.conjunctions.size());
			//another.conjunctions.forEach(item -> conjunctions.add(Conjunction.valueOf(item.toString())));
		}
		
		if(another.conditions != null) {
			conditions = new ArrayList<Logical>(another.conditions.size());
			//another.conditions.forEach(item -> conditions.add(new Logical(item)));
		}
		
	}
	
	public Join andOn(final Object col1, final Operator operator, final Object col2) {
		return appendConjunction(Conjunction.AND).appendCondition(new Logical(col1, operator, col2));
	}
	
	public Join orOn(final Object col1, final Operator operator, final Object col2) {
		return appendConjunction(Conjunction.OR).appendCondition(new Logical(col1, operator, col2));
	}
	
	private Join appendCondition(final Logical logical) {
		
		if(logical == null) {
			throw new IllegalArgumentException("logical == null");
		}
		
		if(conditions == null) {
			conditions = new ArrayList<Logical>(1);
		}
			
		conditions.add(logical);
		
		return this;
	}
	
	private Join appendConjunction(final Conjunction conj) {
		
		if(conj == null) {
			throw new IllegalArgumentException("conj == null");
		}
		
		if(conjunctions == null) {
			conjunctions = new ArrayList<Conjunction>(1);
		}
		
		conjunctions.add(conj);
		
		return this;
	}

	@Override
	public String toSql() {
		StringBuilder sql = new StringBuilder(type.toString());
		sql.append(" JOIN ");
		
		if(subAlias != null) {
			sql.append(subAlias.toSql()).
			append(" ON ").append(conditions.get(0).toSql()).append(' ');
			
			for (int i = 1; i < conditions.size(); i++) {
				sql.append(conjunctions.get(i - 1).toString()).append(' ').append(conditions.get(i).toSql()).append(' ');
			}
			
			sql.setLength(sql.length() - 1);
			
			
		} else {
			sql.append(alias.toSql()).append(" ON ").
			append(conditions.get(0).toSql()).append(' ');
			
			for (int i = 1; i < conditions.size(); i++) {
				sql.append(conjunctions.get(i - 1).toString()).append(' ').append(conditions.get(i).toSql()).append(' ');
			}
			
			sql.setLength(sql.length() - 1);
		}
		
		return sql.toString();
	}

}