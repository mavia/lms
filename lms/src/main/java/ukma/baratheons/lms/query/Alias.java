package ukma.baratheons.lms.query;

/***
 * This class represents an SQL
 * alias for table, table's 
 * field and etc
 * @author Max Oliynick
 */

public class Alias implements QueryStatement{
	
	/**Target table or table's field*/
	protected final String target;
	/**Alias for target*/
	protected final String alias;
	
	protected Character wrapper;
	
	/**
	 * Construct an sql alias using given parameters. Note that
	 * in this situation default wrapper {@literal "} will be used
	 * @param target - target table, table's field or etc
	 * @param alias - given alias. Can be null
	 * */
	public Alias(final String target, final String alias) {
		this(target, alias, null);
	}
	
	/**
	 * Construct an sql alias using given parameters.
	 * @param target - target table, table's field or etc
	 * @param alias - given alias. Can be null
	 * @param wrapper - wrapper to use
	 * */
	public Alias(final String target, final String alias, final Character wrapper) {
		this.target = target;
		this.alias = alias;
		this.wrapper = wrapper;
	}
	
	/**
	 * Copy constructor
	 * @param another - object to copy
	 * */
	public Alias(final Alias another) {
		target = another.target;
		alias = another.alias;
		wrapper = another.wrapper;
	}

	/**Returns specified target*/
	public String getTarget() {
		return target;
	}

	/**Returns specified alias*/
	public String getAlias() {
		return alias;
	}

	@Override
	public String toSql() {
		StringBuilder sql = new StringBuilder();
		
		if(wrapper == null && target != null) {
			sql.append(target);
		} else if(wrapper != null && target != null){
			sql.append(wrapper).append(target).append(wrapper);
		}
		
		if(alias != null) {
			if(wrapper == null) {
				sql.append(" AS ").append(alias);
				
			} else {
				sql.append(" AS ").append(wrapper).append(alias).append(wrapper);
			}
		}
		
		return sql.toString();
	}
	
	

}
