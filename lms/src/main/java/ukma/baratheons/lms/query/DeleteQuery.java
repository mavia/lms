package ukma.baratheons.lms.query;

import ukma.baratheons.lms.query.Logical.Operator;

/**
 * Class which represents a DELETE SQL query
 * */
public class DeleteQuery implements QueryStatement {

	private final String from;
	private Where where;
	
	public DeleteQuery(final String from) {
		if(from == null)
			throw new IllegalArgumentException("from == null");
		
		this.from = from;
	}
	
	public DeleteQuery where(final String column, final Operator operator, final Object value) {
		where = new Where(new Logical(column, operator, value));
		return this;
	}
	
	public DeleteQuery andWhere(final String column, final Operator operator, final Object value) {
		where.and(column, operator, value);
		return this;
	}
	
	public DeleteQuery orWhere(final String column, final Operator operator, final Object value) {
		where.or(column, operator, value);
		return this;
	}
	
	@Override
	public String toSql() {
		
		final StringBuilder sql = new StringBuilder("DELETE FROM ");
		sql.append(from);
		
		if(where != null) {
			sql.append(" WHERE ").append(where.toSql()).append(' ');
		}
		
		return sql.toString();
	}

	
}
