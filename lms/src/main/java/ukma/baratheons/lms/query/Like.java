package ukma.baratheons.lms.query;

/**
 * This class represents SQL 'LIKE' statement
 * */
public class Like implements QueryStatement {
	
	private final String pattern;
	private final String target;
	
	public Like(final String target, final String pattern) {
		
		if(target == null)
			throw new IllegalArgumentException("target == null");
		
		if(pattern == null)
			throw new IllegalArgumentException("pattern == null");
		
		this.target = target;
		this.pattern = pattern;
	}
	
	public Like(final Like another) {
		this.target = another.target;
		this.pattern = another.pattern;
	}
	
	public String getTarget() {
		return target;
	}

	public String getPattern() {
		return pattern;
	}

	@Override
	public String toSql() {
		
		StringBuilder sql = new StringBuilder(target);
		sql.append(" LIKE ").append(pattern);
		
		return sql.toString();
	}

}
