package ukma.baratheons.lms.query;

import ukma.baratheons.lms.query.Join.JoinType;
import ukma.baratheons.lms.query.Logical.Operator;

public class JoinBuilder {
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join innerJoinSub(Join subQuery, String alias, Object col1, Operator operator, Object col2) {
		return createJoin(subQuery, alias, col1, operator, col2, JoinType.INNER, null);
	}
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join innerJoinSub(Join subQuery, String alias, Object col1, Operator operator, Object col2, Character wrapper) {
		return createJoin(subQuery, alias, col1, operator, col2, JoinType.INNER, wrapper);
	}

	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join innerJoin(String table, Object col1, Operator operator, Object col2) {
		return innerJoin(table, null, col1, operator, col2, null);
	}
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join innerJoin(String table, String alias, Object col1, Operator operator, Object col2) {
		return innerJoin(table, alias, col1, operator, col2, null);
	}
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join innerJoin(String table, Object col1, Operator operator, Object col2, Character wrapper) {
		return innerJoin(table, null, col1, operator, col2, wrapper);
	}
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param alias table alias to use
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join innerJoin(String table, String alias, Object col1, Operator operator, Object col2, Character wrapper) {
		return createJoin(table, alias, col1, operator, col2, JoinType.INNER, wrapper);
	}
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join leftJoinSub(Join subQuery, String alias, Object col1, Operator operator, Object col2) {
		return createJoin(subQuery, alias, col1, operator, col2, JoinType.LEFT, null);
	}
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join leftJoinSub(Join subQuery, String alias, Object col1, Operator operator, Object col2, Character wrapper) {
		return createJoin(subQuery, alias, col1, operator, col2, JoinType.LEFT, wrapper);
	}

	/**
	 * Appends LEFT JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join leftJoin(String table, Object col1, Operator operator, Object col2) {
		return leftJoin(table, null, col1, operator, col2, null);
	}
	
	/**
	 * Appends LEFT JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join leftJoin(String table, String alias, Object col1, Operator operator, Object col2) {
		return leftJoin(table, alias, col1, operator, col2, null);
	}
	
	/**
	 * Appends LEFT JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join leftJoin(String table, Object col1, Operator operator, Object col2, Character wrapper) {
		return leftJoin(table, null, col1, operator, col2, wrapper);
	}
	
	/**
	 * Appends LEFT JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param alias table alias to use
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join leftJoin(String table, String alias, Object col1, Operator operator, Object col2, Character wrapper) {
		return createJoin(table, alias, col1, operator, col2, JoinType.LEFT, wrapper);
	}
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join rightJoinSub(Join subQuery, String alias, Object col1, Operator operator, Object col2) {
		return createJoin(subQuery, alias, col1, operator, col2, JoinType.RIGHT, null);
	}
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join rightJoinSub(Join subQuery, String alias, Object col1, Operator operator, Object col2, Character wrapper) {
		return createJoin(subQuery, alias, col1, operator, col2, JoinType.RIGHT, wrapper);
	}
	
	/**
	 * Appends RIGHT JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join rightJoin(String table, Object col1, Operator operator, Object col2) {
		return rightJoin(table, null, col1, operator, col2, null);
	}
	
	/**
	 * Appends RIGHT JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param alias table alias to use
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join rightJoin(String table,Object col1, Operator operator, Object col2, Character wrapper) {
		return rightJoin(table, null, col1, operator, col2, wrapper);
	}
	
	/**
	 * Appends RIGHT JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join rightJoin(String table, String alias, Object col1, Operator operator, Object col2) {
		return rightJoin(table, alias, col1, operator, col2, null);
	}

	/**
	 * Appends RIGHT JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param alias table alias to use
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join rightJoin(String table, String alias, Object col1, Operator operator, Object col2, Character wrapper) {
		return createJoin(table, alias, col1, operator, col2, JoinType.RIGHT, wrapper);
	}
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join fullJoinSub(Join subQuery, String alias, Object col1, Operator operator, Object col2) {
		return createJoin(subQuery, alias, col1, operator, col2, JoinType.FULL, null);
	}
	
	/**
	 * Appends INNER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join fullJoinSub(Join subQuery, String alias, Object col1, Operator operator, Object col2, Character wrapper) {
		return createJoin(subQuery, alias, col1, operator, col2, JoinType.FULL, wrapper);
	}
	
	/**
	 * Appends FULL OUTER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join fullJoin(String table, Object col1, Operator operator, Object col2) {
		return fullJoin(table, null, col1, operator, col2, null);
	}
	
	/**
	 * Appends FULL OUTER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join fullJoin(String table, Object col1, Operator operator, Object col2, Character wrapper) {
		return fullJoin(table, null, col1, operator, col2, wrapper);
	}
	
	/**
	 * Appends FULL OUTER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param alias table alias to use
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join fullJoin(String table, String alias, Object col1, Operator operator, Object col2) {
		return fullJoin(table, alias, col1, operator, col2, null);
	}
	
	/**
	 * Appends FULL OUTER JOIN statement to this query
	 * 
	 * @param table table to commit join with
	 * @param alias table alias to use
	 * @param col1 column from the first table in join
	 * @param operator operator for join comparison
	 * @param col2 column from the second table in join
	 * @return
	 */
	public static Join fullJoin(String table, String alias, Object col1, Operator operator, Object col2, Character wrapper) {
		return createJoin(table, alias, col1, operator, col2, JoinType.FULL, wrapper);
	}

	private static Join createJoin(String table, String alias, Object col1, Operator operator, Object col2, JoinType type, Character wrapper) {
		return new Join(table, alias, type, new Logical(col1, operator, col2, wrapper));
	}
	
	private static Join createJoin(Join sub, String alias, Object col1, Operator operator, Object col2, JoinType type, Character wrapper) {
		return new Join(sub, alias, type, new Logical(col1, operator, col2, wrapper));
	}
	
}
