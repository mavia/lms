package ukma.baratheons.lms.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.Assert;

import ukma.baratheons.lms.query.OrderBy.Ordering;

/**
 * Class which represents a SELECT SQL query
 */
public class SelectQuery implements QueryStatement {
	
	private Map<String, SelectQuery> subQueries;

	private List<Alias> columns;

	private List<Alias> from;

	private List<Join> joins;

	private Where where;

	private List<String> groupBy;

	private List<OrderBy> orderBy;
	
	private int limit;
	
	private int offset;
	
	private static char TABLE_WRAPPER = '\"';
	private static char VALUE_WRAPPER = '\'';
	
	public static void setDefaultTableWrapper(char newWrapper) {
		TABLE_WRAPPER = newWrapper;
	}
	
	public static char getDefaulTableWrapper() {
		return TABLE_WRAPPER;
	}
	
	public static void setDefaultValueWrapper(char newWrapper) {
		TABLE_WRAPPER = newWrapper;
	}
	
	public static char getDefaulValueWrapper() {
		return VALUE_WRAPPER;
	}

	/**
	 * Creates SELECT query with specified columns to select
	 * 
	 * @param columns names of columns to select
	 */
	public SelectQuery(String... columns) {
		
		if(columns == null) {
			throw new IllegalArgumentException("Need to specify columns for SELECT query");
		}
	
		if(columns.length == 0) {
			this.columns = Arrays.asList(new Alias("*", null));
		} else {
			this.columns = new ArrayList<>(columns.length);
			
			for(int i = 0; i < columns.length; ++i) {
				this.columns.add(new Alias(columns[i], null));
			}
			
		}
	}
	
	/**
	 * Copy constructor
	 * @param another - object to copy
	 * */
	public SelectQuery(final SelectQuery another) {
		
		if(another.subQueries != null) {
			this.subQueries = new HashMap<String, SelectQuery>(subQueries.size());
			//another.subQueries.forEach((k, v)->subQueries.put(k, new SelectQuery(v)));
		}
		
		if(another.columns != null) {
			this.columns = new ArrayList<Alias>(another.columns.size());
			//another.columns.forEach(item -> columns.add(new Alias(item)));
		}
		
		if(another.from != null) {
			this.from = new ArrayList<Alias>(another.from.size());
			//another.from.forEach(item -> from.add(new Alias(item)));
		}
		
		if(another.joins != null) {
			this.joins = new ArrayList<Join>(another.joins.size());
			//another.joins.forEach(item -> joins.add(new Join(item)));
		}
		
		if(another.where != null) {
			this.where = new Where(another.where);
		}
		
		if(another.groupBy != null) {
			this.groupBy = new ArrayList<String>(another.groupBy.size());
			//another.groupBy.forEach(item -> groupBy.add(new String(item)));
		}
		
		if(another.orderBy != null) {
			this.orderBy = new ArrayList<OrderBy>(another.orderBy.size());
			//another.orderBy.forEach(item -> orderBy.add(new OrderBy(item)));
		}
		
		this.limit = another.limit;
		this.offset = another.offset;
		
	}
	
	public SelectQuery column(String column, String alias) {
		return column(column, alias, null);
	}
	
	public SelectQuery column(String column, String alias, Character wrapper) {
		appendColumn(column, alias, wrapper);
		return this;
	}
	
	public SelectQuery join(final Join join) {
		
		if(join == null)
			throw new IllegalArgumentException("join == null");
		
		if(joins == null) {
			joins = new ArrayList<Join>(1);
		}
		
		joins.add(join);
		
		return this;
	}

	/**
	 * Specifies which tables to select from
	 * 
	 * @param from tables to select from
	 * @return
	 */
	public SelectQuery from(String... from) {
		
		if(from == null || from.length == 0) {
			throw new IllegalArgumentException("FROM clause cannot be empty");
		}
		
		if(this.from == null) {
			this.from = new ArrayList<>(from.length);
		}
		
		for(int i = 0; i < from.length; ++i) {
			this.from.add(new Alias(from[i], null));
		}
		
		return this;
	}
	
	/**
	 * Specifies which tables to select from
	 * 
	 * @param from tables to select from
	 * @return
	 */
	public SelectQuery from(String from, String alias) {
		return from(from, alias, null);
	}
	
	/**
	 * Specifies which tables to select from
	 * 
	 * @param from tables to select from
	 * @return
	 */
	public SelectQuery from(String from, String alias, Character wrapper) {
		appendFrom(from, alias, wrapper);
		return this;
	}
	
	
	
	public SelectQuery selectSub(final SelectQuery subQuery, final String alias) {
		
		if(subQuery == null)
			throw new IllegalArgumentException("subQuery == null");
		
		if(alias == null)
			throw new IllegalArgumentException("alias == null");
		
		appendSubQuery(alias, subQuery);
		return this;
	}

	/**
	 * Appends where condition for where statement to this query
	 * 
	 * @param where - given where condition
	 * @return
	 */
	public SelectQuery where(final Where where) {
		this.where = where;
		return this;
	}
	
	/**
	 * Appends GROUP BY statement to this query
	 * 
	 * @param groupBy column to group by
	 * @return
	 */
	public SelectQuery groupBy(String... groupBy) {
		this.groupBy = Arrays.asList(groupBy);
		return this;
	}

	/**
	 * Specifies ordering of the query result set
	 * 
	 * @param column column to order by
	 * @return
	 */
	public SelectQuery orderBy(String column) {
		if(orderBy == null) orderBy = new ArrayList<>();
		orderBy.add(new OrderBy(column));
		return this;
	}
	
	/**
	 * Specifies ordering of the query result set
	 * 
	 * @param column column to order by
	 * @param ordering ordering
	 * @return
	 */
	public SelectQuery orderBy(String column, Ordering ordering) {
		if(orderBy == null) orderBy = new ArrayList<>();
		orderBy.add(new OrderBy(column, ordering));
		return this;
	}
	
	/**
	 * Specifies ordering of the query result set
	 * 
	 * @param column column to order by in ascending order
	 * @return
	 */
	public SelectQuery orderByAsc(String column) {
		if(orderBy == null) orderBy = new ArrayList<>();
		orderBy.add(new OrderBy(column, Ordering.ASC));
		return this;
	}
	
	/**
	 * Specifies ordering of the query result set
	 * 
	 * @param column column to order by in descending order
	 * @return
	 */
	public SelectQuery orderByDesc(String column) {
		if(orderBy == null) orderBy = new ArrayList<>();
		orderBy.add(new OrderBy(column, Ordering.DESC));
		return this;
	}
	
	/**
	 * Specifies number of records to retrieve
	 * 
	 * @param limit
	 * @return
	 */
	public SelectQuery limit(int limit) {
		this.limit = limit;
		return this;
	}
	
	/**
	 * Specifies number of records to skip while retrieving
	 * 
	 * @param offset
	 * @return
	 */
	public SelectQuery offset(int offset) {
		this.offset = offset;
		return this;
	}
	
	/**
	 * Tries to reset current select
	 * SQL query. After calling this
	 * method a new select query
	 * can be created
	 * */
	public void reset() {
		columns.clear();
		from.clear();
		joins.clear();
		groupBy.clear();
		orderBy.clear();
		
		where = null;
		limit = offset = 0;
		
		ArrayList<?> temp = (ArrayList<?>) columns;
		temp.trimToSize();
		temp = (ArrayList<?>) from;
		temp.trimToSize();
		temp = (ArrayList<?>) joins;
		temp.trimToSize();
		temp = (ArrayList<?>) groupBy;
		temp.trimToSize();
		temp = (ArrayList<?>) orderBy;
		temp.trimToSize();
	}

	/**
	 * Converts this object to SQL String
	 */
	public String toSql() {
		StringBuilder sql = new StringBuilder("SELECT");
		
		for (Alias alias : columns) {
			
			sql.append(' ').append(alias.toSql()).append(',');
				
		}
		
		if(subQueries != null) {
			for(String alias : subQueries.keySet()) {
				sql.append('(').append(subQueries.get(alias).toSql()).append(") AS ").
				append(alias).append(", ");
			}
		}
		
		sql.setCharAt(sql.length() - 1, ' ');

		Assert.notNull(from, "Cannot omit FROM clause in SELECT statement");
		sql.append("FROM");
		for (QueryStatement fr : from) {
			sql.append(' ').append(fr.toSql()).append(',');
		}
		sql.setCharAt(sql.length() - 1, ' ');

		if (joins != null) {
			for (QueryStatement join : joins) {
				sql.append(join.toSql()).append(' ');
			}
		}

		if (where != null) {
			sql.append("WHERE ").append(where.toSql()).append(' ');
		}

		if (groupBy != null) {
			sql.append("GROUP BY");
			for (String gr : groupBy) {
				sql.append(' ').append(gr).append(',');
			}
			sql.setCharAt(sql.length() - 1, ' ');
		}
		
		if (orderBy != null) {
			sql.append("ORDER BY");
			for (QueryStatement ord : orderBy) {
				sql.append(' ').append(ord.toSql()).append(',');
			}
			sql.setCharAt(sql.length() - 1, ' ');
		}
		
		if(limit != 0) {
			sql.append("LIMIT '").append(limit).append("' ");
		}
		
		if(offset != 0) {
			sql.append("OFFSET '").append(offset).append("' ");
		}

		sql.setLength(sql.length() - 1);
		return sql.toString();
	}
	
	private void appendColumn(String column, String alias, Character wrapper) {
		if(column == null) {
			throw new IllegalArgumentException("column == null");
		}
		
		if(alias == null) {
			throw new IllegalArgumentException("alias == null");
		}
		
		this.columns.add(new Alias(column, alias, wrapper));
		
	}
	
	private void appendFrom(String from, String alias, Character wrapper) {
		
		if(this.from == null) {
			this.from = new ArrayList<>(1);
		}
		
		this.from.add(new Alias(from, alias, wrapper));
	}

	
	
	private void appendSubQuery(final String alias, final SelectQuery subQuery) {
		
		if(this.subQueries == null) {
			this.subQueries = new HashMap<>(1);
			this.subQueries.put(alias, subQuery);
			return;
		} 
		
		this.subQueries.put(alias, subQuery);
	}

}