package ukma.baratheons.lms.query;

/**
 * Class which represents a SQL ordering
 * */
public class OrderBy implements QueryStatement {
	
	/**Represents sql ordering type*/
	public static enum Ordering {
		ASC, DESC
	}
	
	private String column;
	
	private Ordering ordering;
	
	public OrderBy(final String col, final Ordering ord) {
		column = col;
		ordering = ord;
	}
	
	public OrderBy(final String col) {
		this(col, null);
	}
	
	public OrderBy(final OrderBy another) {
		this.column = another.column;
		this.ordering = another.ordering;
	}

	@Override
	public String toSql() {
		StringBuilder sql = new StringBuilder(column);

		if (ordering != null)
			sql.append(' ').append(ordering.toString());
		
		return sql.toString();
	}
	
}
