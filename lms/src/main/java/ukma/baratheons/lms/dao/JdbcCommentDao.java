package ukma.baratheons.lms.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.anorm.Operator;
import ukma.baratheons.lms.entity.Comment;
import ukma.baratheons.lms.query.Table;
import ukma.baratheons.lms.query.Table.EquipmentTable;
import ukma.baratheons.lms.query.Table.TaskTable;
import ukma.baratheons.lms.query.Table.CommentTable;
import ukma.baratheons.lms.query.Table.UserTable;

@Repository
public class JdbcCommentDao implements CommentDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	

	@Override
	public void create(Comment comment) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.insert(comment);
	}

	@Override
	public void update(Comment comment) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.update(comment);
	}
	
	@Override
	public void deleteCommentByTask(Timestamp date) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.delete(date);
	}
	
	@Override
	public List<Comment>getCommentsByEqpt(long id){

		AnORMSelector<Comment> select = new AnORMSelector<Comment>(jdbcTemplate,Comment.class);
		select.where(new Filter(TaskTable.TABLE_NAME, TaskTable.EQUIPMENT, ukma.baratheons.lms.anorm.Operator.EQ, id))
			.and(new Filter(TaskTable.TABLE_NAME, TaskTable.STATUS, ukma.baratheons.lms.anorm.Operator.EQ, 11))
			.and(new Filter(TaskTable.TABLE_NAME, TaskTable.VISIBILITY, ukma.baratheons.lms.anorm.Operator.EQ, true));
		select.selectFields(TaskTable.TABLE_NAME, true, TaskTable.EQUIPMENT, TaskTable.VISIBILITY, TaskTable.STATUS);
		select.selectFields(EquipmentTable.TABLE_NAME, true, EquipmentTable.ID);
		List<Comment> comments = select.select();
		return comments;	
	}
	
	public List<Comment>getCommentsByTask(long taskId){
		AnORMSelector<Comment> select = new AnORMSelector<Comment>(jdbcTemplate,Comment.class);
		select.where(new ukma.baratheons.lms.anorm.Filter(CommentTable.TABLE_NAME, CommentTable.TASK, ukma.baratheons.lms.anorm.Operator.EQ, taskId));
		select.selectFields(Table.UserTable.TABLE_NAME, true, UserTable.FIRST_NAME, UserTable.LAST_NAME);
		select.selectFields(Table.TaskTable.TABLE_NAME, true, TaskTable.ID);
		List<Comment> comments = select.select();
		if(comments.isEmpty()) return null;
		return comments;
	}
}