package ukma.baratheons.lms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.anorm.Operator;
import ukma.baratheons.lms.entity.*;
import ukma.baratheons.lms.query.Table;
import ukma.baratheons.lms.query.Table.RoomTable;
import ukma.baratheons.lms.query.Table.TaskTable;

@Repository
public class JdbcTaskDao implements TaskDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final String sql = "select "+ TaskTable.ID + ", " + TaskTable.PARENT_TASK+ ", B" + '.'+Table.TaskStatusTable.NAME + " as taskName, " +
			TaskTable.CREATION_TIME + ", " + TaskTable.COMPLETION_TIME + ", C" + '.'+ Table.TaskTypeTable.NAME + ", D" + '.'+Table.EquipmentTable.LOCAL_NAME+ ", " +
			"CONCAT(E."+Table.UserTable.FIRST_NAME+  ", ' ' ,"+" E."+Table.UserTable.LAST_NAME+") AS UserName, " +
			"concat(F."+Table.UserTable.FIRST_NAME+ ", ' ' ,"+ "F."+Table.UserTable.LAST_NAME+") AS StaffName FROM "+TaskTable.TABLE_NAME+
			" AS A INNER JOIN "+Table.TaskStatusTable.TABLE_NAME+" AS B ON A."+TaskTable.STATUS+"=B."+ Table.TaskStatusTable.ID+
			" INNER JOIN "+Table.TaskTypeTable.TABLE_NAME+" AS C ON A."+ TaskTable.TYPE+"=C."+ Table.TaskTypeTable.ID+
			" INNER JOIN "+Table.EquipmentTable.TABLE_NAME+" AS D ON A."+ TaskTable.EQUIPMENT+"=D."+ Table.EquipmentTable.ID+
			" INNER JOIN "+Table.UserTable.TABLE_NAME+" AS E ON A."+ TaskTable.CREATOR+"=E."+ Table.UserTable.ID+
			" INNER JOIN "+Table.UserTable.TABLE_NAME+" AS F ON A."+ TaskTable.STAFF+"=F."+ Table.UserTable.ID;

	@Override
	public long create(Task task) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		return (long) w.insert(task);
	}

	@Override
	public List<Task> getAllTasksLazy() {
		AnORMSelector<Task> select = new AnORMSelector<Task>(jdbcTemplate,
				Task.class);
		select.selectFields(RoomTable.TABLE_NAME, false, RoomTable.STAFF);
		List<Task> tasks = select.select();
		return tasks;
	}

	@Override
	public long addNew(Task task) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		return (long) w.insert(task);
	}
	
	@Override
	public List<Task> getRefusedTasks(Filter filter){
		AnORMSelector<Task> select = new AnORMSelector<Task>(jdbcTemplate,
				Task.class);
		select.where(filter);
		select.selectFields(Table.TaskTable.TABLE_NAME, false, Table.TaskTable.STAFF);
		List<Task> tasks = select.select();
		return tasks;
	}

	@Override
	public List<Task> getTasksByEquipment(long id) {
		AnORMSelector<Task> select = new AnORMSelector<Task>(jdbcTemplate,
				Task.class);
		select.where(new Filter(TaskTable.TABLE_NAME, TaskTable.EQUIPMENT,
				ukma.baratheons.lms.anorm.Operator.EQ, id));
		select.selectFields(Table.UserTable.TABLE_NAME, false,
				"countCompleted", "countCanceled");
		List<Task> tasks = select.select();
		return tasks;
	}

	@Override
	public List<Task> getTasksByStaffAndStatus(long staffId, long statusId) {
		AnORMSelector<Task> select = new AnORMSelector<Task>(jdbcTemplate,
				Task.class);
		select.where(
				new Filter(TaskTable.TABLE_NAME, TaskTable.STAFF,
						ukma.baratheons.lms.anorm.Operator.EQ, staffId)).and(
				new Filter(TaskTable.TABLE_NAME, TaskTable.STATUS,
						ukma.baratheons.lms.anorm.Operator.EQ, statusId));
		select.selectFields(Table.UserTable.TABLE_NAME, false,
				"countCompleted", "countCanceled", "countInProgress");
		List<Task> tasks = select.select();
		return tasks;
	}

	@Override
	public List<Task> getTasksByActiveAndStatus(boolean active, long statusId) {
		AnORMSelector<Task> select = new AnORMSelector<Task>(jdbcTemplate,
				Task.class);
		select.where(
				new Filter(Table.UserTable.TABLE_NAME, Table.UserTable.IS_ACTIVE, Operator.EQ, active));
		select.where(new Filter(TaskTable.TABLE_NAME, TaskTable.STATUS,
						ukma.baratheons.lms.anorm.Operator.EQ, statusId));
		select.selectFields(Table.TaskTable.TABLE_NAME, false, TaskTable.CREATOR);
		select.selectFields(Table.EquipmentTable.TABLE_NAME, true, Table.EquipmentTable.ID);
		System.out.println("ACTIVE TASK");
		List<Task> tasks = select.select();
		System.out.println("ACTIVE TASK:"+tasks==null);
		return tasks;
	}

	@Override
	public List<Task> getTasksByParentId(long id) {
		AnORMSelector<Task> select = new AnORMSelector<Task>(jdbcTemplate,
				Task.class);
		select.where(new Filter(TaskTable.TABLE_NAME, TaskTable.PARENT_TASK,
				ukma.baratheons.lms.anorm.Operator.EQ, id));
		select.selectFields(Table.UserTable.TABLE_NAME, false,
				"countCompleted", "countCanceled", "countInProgress");
		List<Task> tasks = select.select();
		return tasks;
	}

	@Override
	public void update(Task task, String...fieldsToUpdate) throws SQLException {
		AnORMWriter writer = new AnORMWriter(jdbcTemplate);
		writer.update(task, fieldsToUpdate);
	}

	@Override
	public boolean deleteUnvisibleTaskByDate(Timestamp dateTime) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.delete(new Filter(TaskTable.TABLE_NAME, TaskTable.CREATION_TIME,
				ukma.baratheons.lms.anorm.Operator.LESS, dateTime));
		return true;
	}

	@Override
	public Task getTasksById(long id) {
		AnORMSelector<Task> select = new AnORMSelector<Task>(jdbcTemplate,
				Task.class);
		select.where(new Filter(Table.TaskTable.TABLE_NAME, Table.TaskTable.ID,
				ukma.baratheons.lms.anorm.Operator.EQ, id));
		select.selectFields(Table.UserTable.TABLE_NAME, false,
				"countCompleted", "countCanceled", "countInProgress");
		List<Task> tasks = select.select();
		if (tasks.isEmpty())
			return null;
		return tasks.get(0);
	}

	@Override
	public List<Task> getTasksByStaff(Long staffId) {
		AnORMSelector<Task> select = new AnORMSelector<Task>(jdbcTemplate,
				Task.class);
		select.where(new Filter(Table.TaskTable.TABLE_NAME,
				Table.TaskTable.STAFF, ukma.baratheons.lms.anorm.Operator.EQ,
				staffId));
		select.selectFields(Table.UserTable.TABLE_NAME, false,
				"countCompleted", "countCanceled", "countInProgress");
		List<Task> tasks = select.select();
		return tasks;
	}

	@Override
	public List<Task> getTasksByUser(long userId) {
		AnORMSelector<Task> select = new AnORMSelector<Task>(jdbcTemplate,
				Task.class);
		select.where(new Filter(Table.TaskTable.TABLE_NAME,
				Table.TaskTable.CREATOR, ukma.baratheons.lms.anorm.Operator.EQ,
				userId));
		select.selectFields(Table.UserTable.TABLE_NAME, false,
				"countCompleted", "countCanceled", "countInProgress");
		List<Task> tasks = select.select();
		return tasks;
	}

	@Override
	public List<Task> getTasksByStatus(long id) {
		AnORMSelector<Task> s = new AnORMSelector<>(jdbcTemplate, Task.class);
		s.selectFields(Table.UserTable.TABLE_NAME, false, "countCompleted",
				"countCanceled", "countInProgress");
		s.where(new Filter(TaskTable.TABLE_NAME, TaskTable.STATUS, Operator.EQ,
				id));
		return s.select();
	}

	@Override
	public List<Task> getTasksByType(long id) {
		AnORMSelector<Task> s = new AnORMSelector<>(jdbcTemplate, Task.class);
		s.where(new Filter(TaskTable.TABLE_NAME, TaskTable.TYPE, Operator.EQ,
				id));
		s.selectFields(Table.UserTable.TABLE_NAME, false, "countCompleted",
				"countCanceled", "countInProgress");
		return s.select();
	}

	@Override
	public List<Task> getAllTasks() {
		AnORMSelector<Task> s = new AnORMSelector<>(jdbcTemplate, Task.class);
		s.selectFields(Table.UserTable.TABLE_NAME, false, "countCompleted",
				"countCanceled", "countInProgress");
		return s.select();

	}

	@Override
	public List<Task> getAllTasksCreatedInPeriod(final Date from, final Date to) {
		String addStr = "";
		if (to != null) {
			to.setHours(23);
			to.setMinutes(59);
			to.setSeconds(59);
		}
		if (from!=null) addStr += (" WHERE A."+TaskTable.CREATION_TIME+" >= ?");
		if (from!=null && to!=null) addStr += (" AND A." + TaskTable.CREATION_TIME +" <= ?");
		if (to!=null && from==null) addStr += (" WHERE A."+TaskTable.CREATION_TIME+" <= ?");
		String str = sql.concat(addStr);
		return (List<Task>) jdbcTemplate.query(str, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement preparedStatement) throws SQLException {
				if (from != null && to != null) {
					preparedStatement.setObject(1, new Timestamp(from.getTime()));
					preparedStatement.setObject(2, new Timestamp(to.getTime()));
				} else if (from != null) {
					preparedStatement.setObject(1, new Timestamp(from.getTime()));

				} else if(to!=null){
					preparedStatement.setObject(1, new Timestamp(to.getTime()));
				}
			}
		}, new TaskMapper());
	}

	private final static class TaskMapper implements RowMapper<Task> {

		@Override
		public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
			Task task = new Task();
			task.setId(rs.getLong(TaskTable.ID));
			Object o = rs.getObject(TaskTable.PARENT_TASK);
			if(o!=null)task.setParentId((long) o);
			task.setCreationTime(rs.getTimestamp(TaskTable.CREATION_TIME));
			task.setCompletionTime(rs.getDate(TaskTable.COMPLETION_TIME));
			task.setTaskStatus(new TaskStatus(rs.getString("taskName")));
			task.setTaskType(new TaskType(rs.getString(Table.TaskTypeTable.NAME)));
			task.setEqpt(new Equipment(rs.getString(Table.EquipmentTable.LOCAL_NAME)));
			task.setUser(new User(rs.getString("UserName")));
			task.setStaff(new User(rs.getString("StaffName")));
			return task;
		}
	}
	@Override
	public List<Task> queryRaw(final String sql, final RowMapper<List<Task>> mapper, final Object... args) {
		
		return jdbcTemplate.query(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				return con.prepareStatement(sql);
			}
		}, new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				
				for(int i = 0; i < args.length; ++i) {
					ps.setString(i + 1, String.valueOf(args[i]));
				}
				
			}
		}, new ResultSetExtractor<List<Task>>() {

			@Override
			public List<Task> extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				
				return mapper.mapRow(rs, -1);
			}
		});
	}
}
