package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.entity.EquipmentType;

@Repository
public class JdbcEquipmentTypeDao implements EquipmentTypeDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void create(EquipmentType equipmentType) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.insert(equipmentType);
	}

	@Override
	public void update(EquipmentType equipmentType) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.update(equipmentType);
	}

	@Override
	public List<EquipmentType> getAll() {
		AnORMSelector<EquipmentType>s=new AnORMSelector<>(jdbcTemplate, EquipmentType.class);
		return s.select();
	}
}