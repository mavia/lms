package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.anorm.Operator;
import ukma.baratheons.lms.entity.TaskType;
import ukma.baratheons.lms.query.Table;

@Repository
public class JdbcTaskTypeDao implements TaskTypeDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public void create(TaskType taskType) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.insert(taskType);
	}

	@Override
	public void update(TaskType taskType, String... fieldsToUpdate) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.update(taskType, fieldsToUpdate);
	}

	@Override
	public TaskType getTaskTypeById(long taskTypeId) {
		AnORMSelector<TaskType> s = new AnORMSelector<TaskType>(jdbcTemplate,
				TaskType.class);
		s.where(new Filter(Table.TaskTypeTable.TABLE_NAME,
				Table.TaskTypeTable.ID, Operator.EQ, taskTypeId));
		List<TaskType> tasklist = s.select();
		if (tasklist.isEmpty())
			return null;
		return tasklist.get(0);
	}

	@Override
	public List<TaskType> getAll() {
		AnORMSelector<TaskType> s = new AnORMSelector<TaskType>(jdbcTemplate,
				TaskType.class);
		List<TaskType> tasklist = s.select();
		return tasklist;
	}

}
