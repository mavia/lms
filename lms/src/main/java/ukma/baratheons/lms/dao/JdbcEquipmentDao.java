package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.anorm.Operator;
import ukma.baratheons.lms.entity.Equipment;
import ukma.baratheons.lms.query.Table;
import ukma.baratheons.lms.query.Table.UserTable;

@Repository
public class JdbcEquipmentDao implements EquipmentDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public boolean create(Equipment equipment) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.insert(equipment);
		return true;
	}

	@Override
	public void update(Equipment equipment, String... fieldsToUpdate) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.update(equipment, fieldsToUpdate);
	}

	public List<Equipment> getEquipmentByRoom(Filter filter) {
		AnORMSelector<Equipment> select = new AnORMSelector<Equipment>(
				jdbcTemplate, Equipment.class);
		select.where(filter).and(
				new Filter(Table.EquipmentTable.TABLE_NAME,
						Table.EquipmentTable.IS_DECOMMISSIONED, Operator.EQ,
						false));
		select.selectFields(Table.RoomTable.TABLE_NAME, false,
				Table.RoomTable.STAFF);
		return select.select();
	}

	@Override
	public List<Equipment> getEquipmentEager(Filter filter) {
		AnORMSelector<Equipment> selector = new AnORMSelector<>(jdbcTemplate,
				Equipment.class);
		selector.where(filter);
		selector.selectFields(Table.UserTable.TABLE_NAME, true, UserTable.ID,
				UserTable.FIRST_NAME, UserTable.LAST_NAME);
		return selector.select();
	}

	@Override
	public List<Equipment> getEquipmentLazy() {
		AnORMSelector<Equipment> selector = new AnORMSelector<>(jdbcTemplate,
				Equipment.class);
		selector.selectFields(Table.RoomTable.TABLE_NAME, true,
				Table.RoomTable.ID);
		return selector.select();
	}

	@Override
	public Equipment getEquipmentById(long eqptId) {
		AnORMSelector<Equipment> s = new AnORMSelector<Equipment>(jdbcTemplate,
				Equipment.class);
		s.where(new Filter(Table.EquipmentTable.TABLE_NAME,
				Table.EquipmentTable.ID, Operator.EQ, eqptId));
		List<Equipment> eql = s.select();
		if (eql.isEmpty())
			return null;
		return eql.get(0);
	}

}
