package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.entity.Log;

@Repository
public class JdbcLogDao implements LogDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void create(Log log) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.insert(log);
	}

	@Override
	public List<Log> getAll() {
		AnORMSelector<Log> s = new AnORMSelector<>(jdbcTemplate, Log.class);
		return s.select();
	}
}
