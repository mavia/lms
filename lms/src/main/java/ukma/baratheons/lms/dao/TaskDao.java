package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.entity.Task;

public interface TaskDao {

	public List<Task> getAllTasksCreatedInPeriod(Date from, Date to);
	
	public long create(Task task) throws SQLException;
	
	public void update(Task taskf,String...fieldsToUpdate) throws SQLException;

	public boolean deleteUnvisibleTaskByDate(Timestamp dateTime) throws SQLException;
	
	public Task getTasksById(long id);
	
	public List<Task> getTasksByParentId(long id);

	public List<Task> getTasksByStaff(Long staffId);
	
	public List<Task> getTasksByUser(long userId);

	public List<Task> getTasksByStatus(long id);

	public List<Task> getTasksByType(long id);
	
	public List<Task> getRefusedTasks(Filter filter);

	public List<Task> getAllTasks();
	
	public List<Task> getAllTasksLazy();

	public long addNew(Task task) throws SQLException;

	public List<Task> getTasksByEquipment(long id);

	public List<Task> getTasksByStaffAndStatus(long staffId, long statusId);

	public List<Task> getTasksByActiveAndStatus(boolean active, long statusId);
	
	public List<Task> queryRaw(final String sql, final RowMapper<List<Task>> mapper, Object... args);

}
