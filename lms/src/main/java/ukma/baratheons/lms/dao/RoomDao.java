package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.entity.Room;

public interface RoomDao {
 
	public List<Room> getRoomsEager(Filter filter);
	
	public List<Room> getRoomsLazy(Filter filter);
	
	public void create(Room room) throws SQLException;
	
	public void update(Room room) throws SQLException;

	public Room getRoomById(int roomId);

	public List<Room> getAllRooms() ;

	public List<Room> getAllRoomsInPeriod(Date from, Date to);

	public void delete(Room room) throws SQLException;
}