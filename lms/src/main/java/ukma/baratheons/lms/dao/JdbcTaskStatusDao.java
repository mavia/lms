package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.entity.TaskStatus;
import ukma.baratheons.lms.query.Table;
import ukma.baratheons.lms.anorm.Operator;

@Repository
public class JdbcTaskStatusDao implements TaskStatusDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void create(TaskStatus taskStatus) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.insert(taskStatus);
	}

	@Override
	public void update(TaskStatus taskStatus, String... fieldsToUpdate) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.update(taskStatus, fieldsToUpdate);
	}

	@Override
	public TaskStatus getTaskStatusById(long taskStatusId) {
		AnORMSelector<TaskStatus> s = new AnORMSelector<TaskStatus>(
				jdbcTemplate, TaskStatus.class);
		s.where(new Filter(Table.TaskStatusTable.TABLE_NAME,
				Table.TaskStatusTable.ID, Operator.EQ, taskStatusId));
		List<TaskStatus> tasklist = s.select();
		if (tasklist.isEmpty())
			return null;
		return tasklist.get(0);
	}

	@Override
	public TaskStatus getIdOfTaskStatus(String taskStatus) {
		AnORMSelector<TaskStatus> s = new AnORMSelector<TaskStatus>(
				jdbcTemplate, TaskStatus.class);
		s.where(new Filter(Table.TaskStatusTable.TABLE_NAME,
				Table.TaskStatusTable.NAME, Operator.EQ, taskStatus));
		List<TaskStatus> tasklist = s.select();
		if (tasklist.isEmpty())
			return null;
		return tasklist.get(0);
	}

	@Override
	public List<TaskStatus> getAll() {
		AnORMSelector<TaskStatus> s = new AnORMSelector<>(jdbcTemplate,
				TaskStatus.class);
		return s.select();
	}
}
