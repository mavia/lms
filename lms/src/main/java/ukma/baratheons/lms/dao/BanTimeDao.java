package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import ukma.baratheons.lms.entity.BanTime;

public interface BanTimeDao {
	
	public List<BanTime> getAll();
	
	public void update(BanTime banTime) throws SQLException;
	
	public void create(BanTime banTime) throws SQLException;
}
