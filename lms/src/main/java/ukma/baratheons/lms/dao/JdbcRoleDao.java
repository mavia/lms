package ukma.baratheons.lms.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.entity.Role;
import ukma.baratheons.lms.query.Table.UserRoleTable;

@Repository
public class JdbcRoleDao implements RoleDao{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Role> getAll() {
		AnORMSelector<Role> s = new AnORMSelector<>(jdbcTemplate, Role.class);
		return s.select();
	}

	@Override
	public Role get(long id) {
		AnORMSelector<Role> s = new AnORMSelector<>(jdbcTemplate, Role.class);
		s.where(new Filter(UserRoleTable.TABLE_NAME, UserRoleTable.ID, ukma.baratheons.lms.anorm.Operator.EQ, id));
		List<Role> res = s.select();
		if(res.isEmpty())return null;
		return res.get(0);
	}
	
	@Override
	public Role getByName(String name){
		AnORMSelector<Role> select = new AnORMSelector<Role>(jdbcTemplate, Role.class);
		select.where(new ukma.baratheons.lms.anorm.Filter(UserRoleTable.TABLE_NAME, UserRoleTable.ROLE_NAME, ukma.baratheons.lms.anorm.Operator.EQ, name));
		List<Role> roles = select.select();
		if(roles.isEmpty()) return null;
		return roles.get(0);
	}

}
