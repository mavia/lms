package ukma.baratheons.lms.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.entity.*;
import ukma.baratheons.lms.query.Table;
import ukma.baratheons.lms.query.Table.RoomTable;
import ukma.baratheons.lms.query.Table.TaskTable;
import ukma.baratheons.lms.query.Table.UserTable;

@Repository
public class JdbcRoomDao implements RoomDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public void create(Room room) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.insert(room);
	}

	@Override
	public void update(Room room) throws SQLException {
		AnORMWriter writer = new AnORMWriter(jdbcTemplate);
		writer.update(room);
	}

	@Override
	public List<Room> getRoomsEager(Filter filter) {
		AnORMSelector<Room> selector = new AnORMSelector<>(jdbcTemplate, Room.class);
		selector.selectFields(Table.UserTable.TABLE_NAME, false, "countCompleted", "countInProgress", "countCanceled");
		selector.where(filter);
		return selector.select();
	}

	public Room getRoomById(int roomId) {
		AnORMSelector<Room> s = new AnORMSelector<>(jdbcTemplate, Room.class);
		s.where(new Filter(RoomTable.TABLE_NAME, RoomTable.ID, ukma.baratheons.lms.anorm.Operator.EQ, roomId));
		s.selectFields(UserTable.TABLE_NAME, false, "countCompleted", "countInProgress", "countCanceled");
		List<Room> res = s.select();
		if(res.isEmpty())return null;
		return res.get(0);
	}

	@Override
	public List<Room> getAllRooms() {
		AnORMSelector<Room> selector= new AnORMSelector<>(jdbcTemplate, Room.class);
		selector.selectFields(Table.UserTable.TABLE_NAME, false, "countCompleted", "countInProgress", "countCanceled");
		List<Room> rooms = selector.select();
		return rooms;
	}

	@Override
	public List<Room> getAllRoomsInPeriod(final Date from, final Date to) {
		if(to!=null) {
			to.setHours(23);
			to.setMinutes(59);
			to.setSeconds(59);
		}
		String sql = "select room.number, count(task.task_id) AS MalfunctionsCount \n" +
				"from task \n" +
				"INNER JOIN equipment ON task.eqpt_id = equipment.eqpt_id \n" +
				"INNER JOIN room ON room.room_id = equipment.room_id ";
		if (from!=null) sql += (" WHERE task."+TaskTable.CREATION_TIME+" >= ?");
		if (from!=null && to!=null) sql += (" AND task." + TaskTable.CREATION_TIME +" <= ?");
		if (to!=null && from==null) sql += (" WHERE task."+TaskTable.CREATION_TIME+" <= ?");
		sql+= " GROUP BY room.number";
		return jdbcTemplate.query(sql, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement preparedStatement) throws SQLException {
				if (from != null && to != null) {
					preparedStatement.setObject(1, new Timestamp(from.getTime()));
					preparedStatement.setObject(2, new Timestamp(to.getTime()));


				} else if (from != null) {
					preparedStatement.setObject(1, new Timestamp(from.getTime()));

				} else if(to!=null){
					preparedStatement.setObject(1, new Timestamp(to.getTime()));
				}
			}
		}, new RoomMapper());
	}

	@Override
	public List<Room> getRoomsLazy(Filter filter) {
		AnORMSelector<Room> selector = new AnORMSelector<>(jdbcTemplate, Room.class);
		selector.where(filter);
		selector.selectFields(Table.UserTable.TABLE_NAME, true, Table.UserTable.ID);
		return selector.select();
	}


	private final static class RoomMapper implements RowMapper<Room> {

		@Override
		public Room mapRow(ResultSet rs, int rowNum) throws SQLException {
			Room room = new Room();
			room.setNumber(rs.getString(RoomTable.NUMBER));
			room.setMalfunctionsCount(rs.getInt("MalfunctionsCount"));
			return room;
		}
	}


	@Override
	public void delete(Room room) throws SQLException {
		AnORMWriter w= new AnORMWriter(jdbcTemplate);
		w.delete(room);
	}

}
