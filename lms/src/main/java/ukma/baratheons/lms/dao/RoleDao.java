package ukma.baratheons.lms.dao;

import java.util.List;

import ukma.baratheons.lms.entity.Role;

public interface RoleDao {
	
	public List<Role>getAll();
	
	public Role get(long id);
	
	public Role getByName(String name);
	
//	public int getDefaultRigths(int role);
}
