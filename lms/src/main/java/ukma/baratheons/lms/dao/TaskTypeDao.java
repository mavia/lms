package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import ukma.baratheons.lms.entity.TaskType;

public interface TaskTypeDao {
	
	public List<TaskType> getAll();
	
	public void create(TaskType taskType) throws SQLException;
	
	public void update(TaskType taskType, String...fieldsToUpdate) throws SQLException;

	public TaskType getTaskTypeById(long taskTypeId);
}
