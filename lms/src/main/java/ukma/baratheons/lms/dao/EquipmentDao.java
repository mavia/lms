package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.entity.Equipment;

public interface EquipmentDao {

	public List<Equipment> getEquipmentEager(Filter filter);

	public boolean create(Equipment equipment) throws SQLException;

	public void update(Equipment equipment, String... fieldsToUpdate) throws SQLException;

	public List<Equipment> getEquipmentByRoom(Filter filter);

	public List<Equipment> getEquipmentLazy();

	public Equipment getEquipmentById(long eqptId);
}
