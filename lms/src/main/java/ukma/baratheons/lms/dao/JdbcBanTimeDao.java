package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.entity.BanTime;

@Repository
public class JdbcBanTimeDao implements BanTimeDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<BanTime> getAll() {
		AnORMSelector<BanTime> s = new AnORMSelector<>(jdbcTemplate, BanTime.class);
		return s.select();
	}

	@Override
	public void update(BanTime banTime) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.update(banTime);
	}

	@Override
	public void create(BanTime banTime) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.insert(banTime);
	}
	
	
}