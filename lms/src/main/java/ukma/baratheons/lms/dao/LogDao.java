package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import ukma.baratheons.lms.entity.Log;

public interface LogDao {
	
	public List<Log>getAll();
	
	public void create(Log log) throws SQLException;
}
