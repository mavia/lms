package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.anorm.Operator;
import ukma.baratheons.lms.entity.Unconfirmed;
import ukma.baratheons.lms.query.Table.UnconfirmedTable;

/**
 * @author Nikolay
 *
 */
@Repository
public class JdbcUnconfirmedDao implements UnconfirmedDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ukma.baratheons.lms.dao.UnconfirmedDao#getByHash(java.lang.String)
	 */
	@Override
	public Unconfirmed getByHash(String hash) {
		AnORMSelector<Unconfirmed> select = new AnORMSelector<Unconfirmed>(
				jdbcTemplate, Unconfirmed.class);
		select.where(new Filter(UnconfirmedTable.TABLE_NAME,
				UnconfirmedTable.HASH, Operator.EQ, hash));
		List<Unconfirmed> unconfirmeds = select.select();
		if (unconfirmeds.isEmpty())
			return null;
		return unconfirmeds.get(0);
	}

	@Override
	public long addNew(Unconfirmed unconfirmed) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		return (long) w.insert(unconfirmed);
	}

	@Override
	public boolean update(Unconfirmed unconfirmed) {

		String sql = "UPDATE " + UnconfirmedTable.TABLE_NAME + " SET "
				+ UnconfirmedTable.HASH + " = ? , "
				+ UnconfirmedTable.EXPIRATION_DATE + " = ? , "
				+ UnconfirmedTable.EMAIL + " = ? , " + UnconfirmedTable.TYPE
				+ " = ? " + " WHERE " + UnconfirmedTable.ID + " = ?";

		Object[] args = new Object[] { unconfirmed.getHash(),
				unconfirmed.getExp_date(), unconfirmed.getEmail(),
				unconfirmed.getAction_type(), unconfirmed.getId() };

		jdbcTemplate.update(sql, args);
		return true;
	}

	@Override
	public boolean deleteAfterDate(Timestamp date) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.delete(new Filter(UnconfirmedTable.TABLE_NAME,
				UnconfirmedTable.EXPIRATION_DATE, Operator.GREATER, date));
		return true;
	}

	@Override
	public boolean deleteByHash(String hash) {

		String sql = "DELETE FROM " + UnconfirmedTable.TABLE_NAME + " WHERE "
				+ UnconfirmedTable.HASH + " = '" + hash + "'";

		jdbcTemplate.update(sql);
		return true;
	}

	/**
	 * @param args
	 */
	/*
	 * public static void main(String[] args) { /*Unconfirmed unconfirmed = new
	 * Unconfirmed();
	 * unconfirmed.setActionType(ConfirmActionType.registrate_user);
	 * unconfirmed.setEmail("admin@lms.ukma"); unconfirmed.setExpDate(new
	 * Timestamp(new Date().getTime()));
	 * unconfirmed.setHash(MD5.getHash(unconfirmed.getEmail() + "&" +
	 * unconfirmed.getActionType().ordinal() + "&" + unconfirmed.getExpDate()));
	 * ud.create(unconfirmed); System.out.println("1");
	 * 
	 * }
	 */

}

/*
 * @Repository public class JdbcUnconfirmedDao implements UnconfirmedDao {
 * 
 * @Autowired private JdbcTemplate jdbcTemplate;
 * 
 * @Override public Unconfirmed getByHash(String hash) { String sql =
 * "SELECT * FROM " + UnconfirmedTable.tableName + " INNER JOIN " +
 * UserRoleTable.tableName + " on unconfirmed.role_id=role.role_id" + " WHERE "
 * + UnconfirmedTable.hash + " = '" + hash + "'"; List<Unconfirmed> unconfirmeds
 * = jdbcTemplate.query(sql, new UnconfirmedMapper()); if
 * (unconfirmeds.isEmpty()) return null; return unconfirmeds.get(0); }
 * 
 * @Override public boolean addNew(Unconfirmed unconfirmed) {
 * 
 * String sql = "INSERT INTO " + UnconfirmedTable.tableName + " ( " +
 * UnconfirmedTable.hash + " , " + UnconfirmedTable.expirationDate + " , " +
 * UnconfirmedTable.email + " , " + UnconfirmedTable.type + " , " +
 * UnconfirmedTable.role_id + " ) VALUES (?,?,?,?,?)";
 * 
 * Object[] args = new Object[] { unconfirmed.getHash(),
 * unconfirmed.getExp_date(), unconfirmed.getEmail(),
 * unconfirmed.getAction_type(), unconfirmed.getRole().getId() };
 * 
 * jdbcTemplate.update(sql, args); return true; }
 * 
 * @Override public boolean update(Unconfirmed unconfirmed) {
 * 
 * String sql = "UPDATE " + UnconfirmedTable.tableName + " SET " +
 * UnconfirmedTable.hash + " = ? , " + UnconfirmedTable.expirationDate +
 * " = ? , " + UnconfirmedTable.email + " = ? , " + UnconfirmedTable.type +
 * " = ? " + UnconfirmedTable.role_id + " = ? " + " WHERE " +
 * UnconfirmedTable.id + " = ?";
 * 
 * Object[] args = new Object[] { unconfirmed.getHash(),
 * unconfirmed.getExp_date(), unconfirmed.getEmail(),
 * unconfirmed.getAction_type(), unconfirmed.getRole().getId(),
 * unconfirmed.getId() };
 * 
 * jdbcTemplate.update(sql, args); return true; }
 * 
 * @Override public boolean deleteAfterDate(Date date) { String sql =
 * "DELETE FROM " + UnconfirmedTable.tableName + " WHERE " +
 * UnconfirmedTable.expirationDate + " <= '" + date.toString() + "'";
 * jdbcTemplate.update(sql); return true; }
 * 
 * @Override public boolean deleteByHash(String hash) { String sql =
 * "DELETE FROM " + UnconfirmedTable.tableName + " WHERE " +
 * UnconfirmedTable.hash + " = " + "'" + hash + "';"; jdbcTemplate.update(sql);
 * return true; }
 * 
 * private static final class UnconfirmedMapper implements
 * RowMapper<Unconfirmed> { public Unconfirmed mapRow(ResultSet rs, int rowNum)
 * throws SQLException { Unconfirmed uncfmd = new Unconfirmed();
 * uncfmd.setId(rs.getInt(UnconfirmedTable.id));
 * uncfmd.setHash(rs.getString(UnconfirmedTable.hash));
 * uncfmd.setExp_date(rs.getDate(UnconfirmedTable.expirationDate));
 * uncfmd.setEmail(rs.getString(UnconfirmedTable.email));
 * uncfmd.setAction_type(rs.getString(UnconfirmedTable.type));
 * uncfmd.setRole(new
 * Role(rs.getInt(UnconfirmedTable.role_id),rs.getString(UserRoleTable
 * .roleName), rs.getInt(UserRoleTable.defaultRights))); return uncfmd; } }
 * 
 * 
 * } >>>>>>> branch 'master' of https://github.com/Baratheons/LMS
 */