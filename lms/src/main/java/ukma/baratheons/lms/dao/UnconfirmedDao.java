package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import ukma.baratheons.lms.entity.Unconfirmed;

/**
 * @author Nikolay
 *
 */
public interface UnconfirmedDao {

    public Unconfirmed getByHash(String hash);

    public long addNew(Unconfirmed unconfirmed) throws SQLException;

    public boolean update(Unconfirmed unconfirmed);

    public boolean deleteAfterDate(Timestamp date) throws SQLException;
    
    public boolean deleteByHash(String hash);
}
