package ukma.baratheons.lms.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import ukma.baratheons.lms.anorm.AnORMSelector;
import ukma.baratheons.lms.anorm.AnORMWriter;
import ukma.baratheons.lms.anorm.Filter;
import ukma.baratheons.lms.anorm.Operator;
import ukma.baratheons.lms.entity.Room;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.query.Table.RoomTable;
import ukma.baratheons.lms.query.Table.TaskTable;
import ukma.baratheons.lms.query.Table.UserTable;
import ukma.baratheons.lms.service.StaticConstant;

@Repository
public class JdbcUserDao implements UserDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public User getUserByEmail(String email) {
		AnORMSelector<User> select = new AnORMSelector<User>(jdbcTemplate,
				User.class);
		select.where(new ukma.baratheons.lms.anorm.Filter(UserTable.TABLE_NAME,
				UserTable.EMAIL, ukma.baratheons.lms.anorm.Operator.EQ, email));
		List<User> users = select.select();
		if (users.isEmpty())
			return null;
		return users.get(0);
	}
	
	@Override
	public List<User> getAdmins(){
		AnORMSelector<User> select = new AnORMSelector<User>(jdbcTemplate,
				User.class);
		
		select.where(new ukma.baratheons.lms.anorm.Filter(UserTable.TABLE_NAME,
				UserTable.ROLE, ukma.baratheons.lms.anorm.Operator.EQ, StaticConstant.Role.ADMIN));
		
		List<User> users = select.select();
		
		return users;
	}

	@Override
	public User getUserById(ukma.baratheons.lms.anorm.Filter filter) {
		AnORMSelector<User> select = new AnORMSelector<User>(jdbcTemplate,
				User.class);
		select.where(filter);
		List<User> users = select.select();
		if (users.isEmpty())
			return null;
		return users.get(0);
	}

	@Override
	public boolean deleteByLastOnline(Timestamp dateTime) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.delete(new Filter(UserTable.TABLE_NAME, UserTable.LAST_ONLINE,
				Operator.LEQ, dateTime));
		return true;
	}

	@Override
	public boolean create(User user) throws SQLException {
		AnORMWriter writer = new AnORMWriter(jdbcTemplate);
		writer.insert(user);
		return true;
	}

	@Override
	public boolean update(User user) throws SQLException {
		AnORMWriter writer = new AnORMWriter(jdbcTemplate);
		writer.update(user);
		return true;
	}

	@Override
	public List<User> getUsers() {
		AnORMSelector<User> select = new AnORMSelector<User>(jdbcTemplate,
				User.class);
		select.selectFields(UserTable.TABLE_NAME, false, "completed",
				"canceled");
		List<User> users = select.select();
		return users;
	}

	@Override
	public List<User> getAllUsersWithPenalty() {
		AnORMSelector<User> s = new AnORMSelector<>(jdbcTemplate, User.class);
		s.selectFields(UserTable.TABLE_NAME, false, "completed", "canceled");
		s.where(new Filter(UserTable.TABLE_NAME, UserTable.PENALTY_SCORE,
				Operator.GREATER, 0));
		return s.select();
	}

	@Override
	public List<User> getUsersForNonChief(long idAdminRole) {
		AnORMSelector<User> select = new AnORMSelector<User>(jdbcTemplate,
				User.class);
		select.where(new ukma.baratheons.lms.anorm.Filter(UserTable.TABLE_NAME,
				UserTable.ROLE, Operator.NEQ, idAdminRole));
		select.selectFields(UserTable.TABLE_NAME, false, "completed",
				"canceled");
		List<User> users = select.select();
		return users;
	}

	@Override
	public List<User> getUsersFiltered(long filters) {
		AnORMSelector<User> select = new AnORMSelector<User>(jdbcTemplate,
				User.class);
		if (filters != 0)
			select.where(new ukma.baratheons.lms.anorm.Filter(
					UserTable.TABLE_NAME, UserTable.ROLE,
					ukma.baratheons.lms.anorm.Operator.EQ, filters));
		List<User> users = select.select();
		if (users.isEmpty())
			return null;
		return users;
	}

	@Override
	public void updateBan(User user) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		w.update(user, UserTable.BAN_END_TIME);
	}

	@Override
	public User getStaffById(long id) {
		AnORMSelector<User> s = new AnORMSelector<>(jdbcTemplate, User.class);
		s.where(new ukma.baratheons.lms.anorm.Filter(UserTable.TABLE_NAME,
				UserTable.ROLE, ukma.baratheons.lms.anorm.Operator.EQ, 2))
				.and(new ukma.baratheons.lms.anorm.Filter(UserTable.TABLE_NAME,
						UserTable.ID, ukma.baratheons.lms.anorm.Operator.EQ, id));
		List<User> staffs = s.select();
		if (staffs == null)
			return null;
		return staffs.get(0);
	}

	@Override
	public List<User> getStaffProductivityInPeriod(final Date from, final Date to) {
		if (to != null) {
			to.setHours(23);
			to.setMinutes(59);
			to.setSeconds(59);
		}
		String countCompleted = "Select A.user_id, count(B.task_id) \n" +
				"FROM users as A \n" +
				"INNER JOIN task as B ON A.user_id= B.staff_id \n" +
				"WHERE B.task_status_id='13'";
		if (from!=null) countCompleted += (" AND B."+TaskTable.CREATION_TIME+" >= ?" );
		if (from!=null && to!=null) countCompleted += (" AND B." + TaskTable.CREATION_TIME +" <= ?");
		if (to!=null && from==null) countCompleted += ("AND B."+TaskTable.CREATION_TIME+" <= ?");
		countCompleted+=" GROUP BY (A.user_id)\n" +
				"UNION\n" +
				"SELECT A.user_id, '0'\n" +
				"FROM users as A\n" +
				"INNER JOIN task as B ON A.user_id= B.staff_id\n" +
				"WHERE A.user_id NOT IN (SELECT C.user_id\n" +
				"                        FROM users as C\n" +
				"                        INNER JOIN task ON C.user_id= task.staff_id\n" +
				"                        WHERE task.task_status_id='13')\n" +
				"GROUP BY (A.user_id)";

		String countCancelled = "Select A.user_id, count(B.task_id) \n" +
				"FROM users as A \n" +
				"INNER JOIN task as B ON A.user_id= B.staff_id \n" +
				"WHERE B.task_status_id='10'";
		if (from!=null) countCancelled += (" AND B."+TaskTable.CREATION_TIME+" >= ?" );
		if (from!=null && to!=null) countCancelled += (" AND B." + TaskTable.CREATION_TIME +" <= ?" );
		if (to!=null && from==null) countCancelled += ("AND B."+TaskTable.CREATION_TIME+" <= ?" );
		countCancelled+=" GROUP BY (A.user_id)\n" +
				"UNION\n" +
				"SELECT A.user_id, '0'\n" +
				"FROM users as A\n" +
				"INNER JOIN task as B ON A.user_id= B.staff_id\n" +
				"WHERE A.user_id NOT IN (SELECT C.user_id\n" +
				"                        FROM users as C\n" +
				"                        INNER JOIN task ON C.user_id= task.staff_id\n" +
				"                        WHERE task.task_status_id='10')\n" +
				"GROUP BY (A.user_id)";

		String str="SELECT u.user_id, u.first_name, u.last_name, compl.count as completed, cancel.count as canceled\n" +
				"FROM users u\n" +
				"INNER JOIN ("+countCompleted+") as compl ON u.user_id = compl.user_id\n" +
				"INNER JOIN ("+countCancelled+") as cancel ON u.user_id = cancel.user_id";
		return (List<User>) jdbcTemplate.query(str, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement preparedStatement) throws SQLException {
				if(from!=null&&to!=null){
					preparedStatement.setObject(1, new Timestamp(from.getTime()));
					preparedStatement.setObject(2, new Timestamp(to.getTime()));
					preparedStatement.setObject(3, new Timestamp(from.getTime()));
					preparedStatement.setObject(4, new Timestamp(to.getTime()));

				}
				else if (from!=null){
					preparedStatement.setObject(1, new Timestamp(from.getTime()));
					preparedStatement.setObject(2, new Timestamp(from.getTime()));
				}
				else if(to!=null){
					preparedStatement.setObject(1, new Timestamp(to.getTime()));
					preparedStatement.setObject(2, new Timestamp(to.getTime()));
				}

			}
		}, new ProductivityMapper());
	}

	@Override
	public Long addNew(User user) throws SQLException {
		AnORMWriter w = new AnORMWriter(jdbcTemplate);
		return (Long) w.insert(user);
	}

	@Override
	public List<User> getStaffByActive(boolean b) {
		AnORMSelector<User> s = new AnORMSelector<>(jdbcTemplate, User.class);
		s.selectFields(UserTable.TABLE_NAME, true, UserTable.ID,
				UserTable.FIRST_NAME, UserTable.LAST_NAME);
		s.where(new ukma.baratheons.lms.anorm.Filter(UserTable.TABLE_NAME,
				UserTable.ROLE, ukma.baratheons.lms.anorm.Operator.EQ, 2)).and(
				new ukma.baratheons.lms.anorm.Filter(UserTable.TABLE_NAME,
						UserTable.IS_ACTIVE,
						ukma.baratheons.lms.anorm.Operator.EQ, b));
		List<User> staffs = s.select();
		if (staffs == null)
			return null;
		return staffs;
	}

	@Override
	public List<User> getStaff() {
		AnORMSelector<User> s = new AnORMSelector<>(jdbcTemplate, User.class);
		s.selectFields("users", true, UserTable.ID, UserTable.FIRST_NAME,
				UserTable.LAST_NAME);
		s.where(new ukma.baratheons.lms.anorm.Filter(UserTable.TABLE_NAME,
				UserTable.ROLE, ukma.baratheons.lms.anorm.Operator.EQ, 2));
		return s.select();
	}

	@Override
	public Long getUserIdByTask(long taskId) {
		AnORMSelector<Task> s = new AnORMSelector<>(jdbcTemplate, Task.class);
		s.where(new Filter(TaskTable.TABLE_NAME, TaskTable.ID, Operator.EQ,
				taskId));
		s.selectFields(TaskTable.TABLE_NAME, true, TaskTable.CREATOR);
		List<Task> res = s.select();
		if (res.isEmpty())
			return null;
		return res.get(0).getUser().getId();
	}

	@Override
	public User getStaffByRoomId(long roomId) {
		AnORMSelector<Room> s = new AnORMSelector<>(jdbcTemplate, Room.class);
		s.where(new Filter(RoomTable.TABLE_NAME, RoomTable.ID, Operator.EQ, roomId));
		List<Room> res = s.select();
		if (res.isEmpty())
			return null;
		return res.get(0).getUser();
	}

	private final static class ProductivityMapper implements RowMapper<User> {

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setId(rs.getLong(UserTable.ID));
			user.setFirstName(rs.getString(UserTable.FIRST_NAME));
			user.setLastName(rs.getString(UserTable.LAST_NAME));
			user.setCountCompleted(rs.getInt("completed"));
			user.setCountCanceled(rs.getInt("canceled"));
			return user;
		}
	}

}
