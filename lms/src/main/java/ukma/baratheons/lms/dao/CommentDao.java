package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import ukma.baratheons.lms.entity.Comment;

public interface CommentDao {
	
	public List<Comment>getCommentsByEqpt(long id);
	
	public List<Comment>getCommentsByTask(long taskId);

	public void create(Comment comment) throws SQLException;

	public void deleteCommentByTask(Timestamp date) throws SQLException;

	void update(Comment comment) throws SQLException;
}
