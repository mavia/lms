package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import ukma.baratheons.lms.entity.EquipmentType;

public interface EquipmentTypeDao {

	public List<EquipmentType> getAll();

	public void create(EquipmentType equipmentType) throws SQLException;

	public void update(EquipmentType equipmentType) throws SQLException;
}
