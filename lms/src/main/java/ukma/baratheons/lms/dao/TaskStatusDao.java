package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.util.List;

import ukma.baratheons.lms.entity.TaskStatus;

public interface TaskStatusDao {
	
	public List<TaskStatus>getAll();
	
	public void create(TaskStatus taskStatus) throws SQLException;
	
	public void update(TaskStatus taskStatus, String...fieldsToUpdate) throws SQLException;

	public TaskStatus getTaskStatusById(long taskStatusId);
	
	public TaskStatus getIdOfTaskStatus(String taskStatus);
}
