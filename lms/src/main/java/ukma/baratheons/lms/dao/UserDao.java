package ukma.baratheons.lms.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import ukma.baratheons.lms.entity.User;

public interface UserDao {
	
	public List<User> getUsers();

	public List<User> getAllUsersWithPenalty();
	
	public List<User> getUsersForNonChief(long idAdminRole);
	
	public boolean create(User user) throws SQLException;
	
	public boolean update(User user) throws SQLException;
	
	public void updateBan(User user) throws SQLException;
	
	public List<User> getUsersFiltered(long filters);
	
	public List<User> getStaff();
	
	public List<User> getAdmins();
	
	public User getUserByEmail(String email);
	
	public User getUserById(ukma.baratheons.lms.anorm.Filter filter);

	public boolean deleteByLastOnline(Timestamp dateTime) throws SQLException;

	public List<User> getStaffProductivityInPeriod(Date from, Date to);

	public User getStaffById(long id);

	public Long addNew(User user) throws SQLException;

	public List<User> getStaffByActive(boolean b);

	public Long getUserIdByTask(long taskId);

	public User getStaffByRoomId(long roomId);
}
