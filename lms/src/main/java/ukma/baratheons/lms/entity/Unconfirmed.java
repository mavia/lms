package ukma.baratheons.lms.entity;

import java.sql.Date;
import java.sql.Timestamp;

import ukma.baratheons.lms.anorm.Base;
import ukma.baratheons.lms.anorm.DataBaseField;
import ukma.baratheons.lms.anorm.Embedded;
import ukma.baratheons.lms.anorm.Entity;
import ukma.baratheons.lms.anorm.Enumeration;
import ukma.baratheons.lms.anorm.Id;
import ukma.baratheons.lms.anorm.Transient;
import ukma.baratheons.lms.query.Table;

@Entity(Table.UnconfirmedTable.TABLE_NAME)
public class Unconfirmed {

	@Id
	@DataBaseField(Table.UnconfirmedTable.ID)
	@Transient
	private Long id;
	
	@DataBaseField(Table.UnconfirmedTable.HASH)
	@Base
	@Transient
	private String hash;
	
	@DataBaseField(Table.UnconfirmedTable.EXPIRATION_DATE)
	@Transient
	@Base
	private Date exp_date;
	
	@DataBaseField(Table.UnconfirmedTable.EMAIL)
	@Base
	@Transient
	private String email;
	
	@DataBaseField(Table.UnconfirmedTable.TYPE)
	@Base
	@Transient
	@Enumeration
	private ConfirmActionType action_type;

	
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public Date getExp_date() {
		return exp_date;
	}

	public void setExp_date(Date exp_date) {
		this.exp_date = exp_date;
	}

	public ConfirmActionType getAction_type() {
		return action_type;
	}

	public void setAction_type(ConfirmActionType action_type) {
		this.action_type = action_type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	

}