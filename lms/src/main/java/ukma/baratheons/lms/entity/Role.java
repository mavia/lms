package ukma.baratheons.lms.entity;

import ukma.baratheons.lms.anorm.Base;
import ukma.baratheons.lms.anorm.DataBaseField;
import ukma.baratheons.lms.anorm.Entity;
import ukma.baratheons.lms.anorm.Id;
import ukma.baratheons.lms.anorm.Transient;
import ukma.baratheons.lms.query.Table;

@Entity(Table.UserRoleTable.TABLE_NAME)
public class Role {
    
	@Id
	@Transient
	@DataBaseField(Table.UserRoleTable.ID)
    private Long id;
	
	@DataBaseField(Table.UserRoleTable.ROLE_NAME)
	@Base
    private String name;
	
	@DataBaseField(Table.UserRoleTable.DEFAULT_RIGHTS)
	@Base
    private int defRights;

    public Role() {}

    public Role(Long id) {
        this.id = id;
    }

    public Role(String name, int defRights) {
        this.name = name;
        this.defRights = defRights;
    }

    public Role(long id, String name, int defRights) {
        this.id = id;
        this.name = name;
        this.defRights = defRights;
    }
    
    public Role(long id, String name){
    	this.name = name;
    	this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDefRights() {
        return defRights;
    }

    public void setDefRights(int defRights) {
        this.defRights = defRights;
    }
}
