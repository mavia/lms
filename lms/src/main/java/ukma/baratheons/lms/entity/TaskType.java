package ukma.baratheons.lms.entity;

import ukma.baratheons.lms.anorm.Base;
import ukma.baratheons.lms.anorm.DataBaseField;
import ukma.baratheons.lms.anorm.Entity;
import ukma.baratheons.lms.anorm.Id;
import ukma.baratheons.lms.anorm.Transient;
import ukma.baratheons.lms.query.Table;

@Entity(Table.TaskTypeTable.TABLE_NAME)
public class TaskType {

	@Id
	@Transient
	@DataBaseField(Table.TaskTypeTable.ID)
	private Long id;
	
	@Base
	@DataBaseField(Table.TaskTypeTable.NAME)
    private String name;

    public TaskType(){}

    public TaskType(String name) {
        this.setName(name);
    }

	public TaskType(Long id) {
		this.id = id;
	}

	public TaskType(long id, String name) {
        this.setId(id);
        this.setName(name);
    }

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskType other = (TaskType) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}

