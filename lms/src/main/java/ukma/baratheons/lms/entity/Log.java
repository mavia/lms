package ukma.baratheons.lms.entity;

import java.sql.Timestamp;

import ukma.baratheons.lms.anorm.Base;
import ukma.baratheons.lms.anorm.DataBaseField;
import ukma.baratheons.lms.anorm.Embedded;
import ukma.baratheons.lms.anorm.Entity;
import ukma.baratheons.lms.anorm.Id;
import ukma.baratheons.lms.anorm.Transient;
import ukma.baratheons.lms.query.Table;

@Entity(Table.LogTable.TABLE_NAME)
public class Log {

	@Id
	@Transient
	@DataBaseField(Table.LogTable.ID)
	private Long id;
	
	@Transient
	@DataBaseField(Table.LogTable.ACTION)
	private int action;
	
	@Transient
	@DataBaseField(Table.LogTable.DATE)
	private Timestamp date;
	
	@Transient
	@DataBaseField(Table.LogTable.TEXT)
	private String text;
	
	@Base
	@Transient
	@Embedded(value=User.class, nullable=false)
	@DataBaseField(Table.LogTable.USER)
	 private User user;
	
	public Log(){}
	
	public Log(long id, int action, Timestamp date, String text, User user) {
		this.id = id;
		this.action = action;
		this.date = date;
		this.text = text;
		this.user = user;
	}

	public Log(int action, Timestamp date, String text, User user) {
		this.action = action;
		this.date = date;
		this.text = text;
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public User getUser(){
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + action;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Log other = (Log) obj;
		if (action != other.action)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
	
}
