package ukma.baratheons.lms.entity;

import java.sql.Date;
import java.sql.Timestamp;

import ukma.baratheons.lms.anorm.Base;
import ukma.baratheons.lms.anorm.DataBaseField;
import ukma.baratheons.lms.anorm.Embedded;
import ukma.baratheons.lms.anorm.Entity;
import ukma.baratheons.lms.anorm.Id;
import ukma.baratheons.lms.anorm.Transient;
import ukma.baratheons.lms.query.Table;

@Entity(Table.UserTable.TABLE_NAME)
public class User {
	
	@Transient
	@Id
	@DataBaseField(Table.UserTable.ID)
	private Long id;
	
	@DataBaseField(Table.UserTable.EMAIL)
	@Base
	private String email;
	
	@DataBaseField(Table.UserTable.FIRST_NAME)
	@Base
	private String firstName;
	
	@DataBaseField(Table.UserTable.LAST_NAME)
	@Base
	private String lastName;
	
	@DataBaseField(Table.UserTable.LAST_ONLINE)
	@Base
	private Timestamp lastOnline;
	
	@DataBaseField(Table.UserTable.REGISTRATION_DATE)
	@Transient
	@Base
	private Date regDate;
	
	@DataBaseField(Table.UserTable.BAN_END_TIME)
	private Timestamp banEndTime;
	
	@DataBaseField(Table.UserTable.PASSWORD)
	@Base
	private String password;
	
	@Base
	@DataBaseField(Table.UserTable.IS_ACTIVE)
	private boolean active;
	
	@Base
	@DataBaseField(Table.UserTable.PENALTY_SCORE)
	private int penaltyScore;
	
	@DataBaseField(Table.UserTable.RIGHTS)
	@Base
	private int rights;
	
	@Embedded(value=Role.class, nullable=false)
	@Base
	@DataBaseField(Table.UserTable.ROLE)
	private Role role;
	
	@DataBaseField(Table.UserTable.RECEIVE_EMAIL)
	private boolean receiveEmail;

	private long countCompleted;
	
	private long countCanceled;

//	private int countInProgress;

	public User() {}

	public User(String firstName) {
		this.firstName = firstName;
		this.lastName = "";
	}

	public User(String email, String firstName, String lastName, Timestamp lastOnline, Date regDate,
				Timestamp banEndTime, String password, boolean isActive, int penaltyScore, int rights,
				boolean receiveEmail) {
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.lastOnline = lastOnline;
		this.regDate = regDate;
		this.banEndTime = banEndTime;
		this.password = password;
		this.active = isActive;
		this.penaltyScore = penaltyScore;
		this.rights = rights;
		this.role = new Role();
		this.receiveEmail = receiveEmail;
	}

	public User(String email, String firstName, String lastName, Timestamp lastOnline, Date regDate,
			Timestamp banEndTime, String password, boolean isActive, int penaltyScore, int rights,
			boolean receiveEmail, int roleId, String roleName) {
		this(email, firstName, lastName, lastOnline, regDate, banEndTime, password, isActive, penaltyScore, rights,
				receiveEmail);
		this.role.setId(roleId);
		this.role.setName(roleName);
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Timestamp getLastOnline() {
		return lastOnline;
	}

	public void setLastOnline(Timestamp lastOnline) {
		this.lastOnline = lastOnline;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date timestamp) {
		this.regDate = timestamp;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPenaltyScore() {
		return penaltyScore;
	}

	public void setPenaltyScore(int penaltyScore) {
		this.penaltyScore = penaltyScore;
	}

	public Timestamp getBanEndTime() {
		return banEndTime;
	}

	public void setBanEndTime(java.sql.Timestamp timestamp) {
		this.banEndTime = timestamp;
	}

	public int getRights() {
		return rights;
	}

	public void setRights(int rights) {
		this.rights = rights;
	}

	public Role getRole(){
		return role;
	}
	
	public void setRole(Role role){
		this.role = role;
	}

	public boolean isReceiveEmail() {
		return receiveEmail;
	}

	public void setReceiveEmail(boolean receiveEmail) {
		this.receiveEmail = receiveEmail;
	}

	public long getCountCompleted() {
		return countCompleted;
	}

	public void setCountCompleted(int countCompleted) {
		this.countCompleted = countCompleted;
	}

	public long getCountCanceled() {
		return countCanceled;
	}

	public void setCountCanceled(int countCanceled) {
		this.countCanceled = countCanceled;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (this.role.getId() != other.role.getId())
			return false;
		return true;
	}
	
	public String toString(){
		return firstName+" "+lastName+" "+email+" ";
	}
}
