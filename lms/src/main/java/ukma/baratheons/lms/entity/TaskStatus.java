package ukma.baratheons.lms.entity;

import ukma.baratheons.lms.anorm.Base;
import ukma.baratheons.lms.anorm.DataBaseField;
import ukma.baratheons.lms.anorm.Entity;
import ukma.baratheons.lms.anorm.Id;
import ukma.baratheons.lms.anorm.Transient;
import ukma.baratheons.lms.query.Table;

@Entity(Table.TaskStatusTable.TABLE_NAME)
public class TaskStatus {

	@Id
	@Transient
	@DataBaseField(Table.TaskStatusTable.ID)
    private Long id;
	
	@Base
	@DataBaseField(Table.TaskStatusTable.NAME)
    private String name;

    public TaskStatus() {}

    public TaskStatus(String name) {
        this.setName(name);
    }
    
    public TaskStatus(long id) {
    	this.setId(id);
    }

    public TaskStatus(long id, String name) {
        this.setId(id);
        this.setName(name);
    }

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskStatus other = (TaskStatus) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}

