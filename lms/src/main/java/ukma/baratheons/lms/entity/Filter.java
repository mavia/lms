package ukma.baratheons.lms.entity;

import ukma.baratheons.lms.query.Logical.Operator;

public class Filter {
	
	private String field;
	
	private Operator operator;
	
	private Object value;
	
	public Filter() {
		
	}
	
	public Filter(String field, Operator operator, Object value) {
		this.setField(field);
		this.setOperator(operator);
		this.setValue(value);
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
}
