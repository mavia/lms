package ukma.baratheons.lms.entity;

import ukma.baratheons.lms.anorm.Base;
import ukma.baratheons.lms.anorm.DataBaseField;
import ukma.baratheons.lms.anorm.Embedded;
import ukma.baratheons.lms.anorm.Entity;
import ukma.baratheons.lms.anorm.Id;
import ukma.baratheons.lms.anorm.Transient;
import ukma.baratheons.lms.query.Table;


@Entity(Table.EquipmentTable.TABLE_NAME)
public class Equipment {

	@Id
	@Transient
	@DataBaseField(Table.EquipmentTable.ID)
	private Long id;
	
	@Base
	@DataBaseField(Table.EquipmentTable.LOCAL_NAME)
	private String localNumber;
	
	@DataBaseField(Table.EquipmentTable.IS_DECOMMISSIONED)
	private boolean isDecommissioned;
	
	@DataBaseField(Table.EquipmentTable.TYPE)
	@Embedded(value=EquipmentType.class, nullable=false)
	@Base
	private EquipmentType eqptType;
	
	@DataBaseField(Table.EquipmentTable.ROOM)
	@Base
	@Embedded(value=Room.class, nullable=true)
	private Room room;
	
	public Equipment(){
	}

	public Equipment(String localNumber) {
		this.localNumber = localNumber;
	}

	public Equipment(long id, boolean isDecommissioned,
					 long eqptTypeId, Room room) {
		this.id = id;
		this.isDecommissioned = isDecommissioned;
		this.eqptType.setId(eqptTypeId);
		this.room = room;
	}

	public Equipment(boolean isDecommissioned,
			long eqptTypeId, Room room) {
		this.isDecommissioned = isDecommissioned;
		this.eqptType.setId(eqptTypeId);
		this.room = room;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLocalNumber() {
		return localNumber;
	}

	public void setLocalNumber(String localNumber) {
		this.localNumber = localNumber;
	}

	public boolean isDecommissioned() {
		return isDecommissioned;
	}

	public void setDecommissioned(boolean isDecommissioned) {
		this.isDecommissioned = isDecommissioned;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public void setEqptType(EquipmentType eqptType) {
		this.eqptType = eqptType;
	}
	
	public Room getRoom(){
		return room;
	}
	
	public EquipmentType getEqptType(){
		return eqptType;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipment other = (Equipment) obj;
		if (eqptType == null) {
			if (other.eqptType != null)
				return false;
		} else if (!eqptType.equals(other.eqptType))
			return false;
		if (localNumber == null) {
			if (other.localNumber != null)
				return false;
		} else if (!localNumber.equals(other.localNumber))
			return false;
		if (room == null) {
			if (other.room != null)
				return false;
		} else if (!room.getNumber().equals(other.room.getNumber()))
			return false;
		return true;
	}
	
}
