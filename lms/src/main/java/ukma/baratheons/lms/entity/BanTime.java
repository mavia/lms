package ukma.baratheons.lms.entity;

import java.util.Date;

public class BanTime {

	private Long id;
	private int penaltyScore;
	private Date date;
	private User user;

	public BanTime() {
	}

	public BanTime(int penaltyScore, Date date, User user) {
		super();
		this.penaltyScore = penaltyScore;
		this.date = date;
		this.user = user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getPenaltyScore() {
		return penaltyScore;
	}

	public void setPenaltyScore(int penaltyScore) {
		this.penaltyScore = penaltyScore;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + penaltyScore;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BanTime other = (BanTime) obj;
		if (penaltyScore != other.penaltyScore)
			return false;
		return true;
	}

	
}
