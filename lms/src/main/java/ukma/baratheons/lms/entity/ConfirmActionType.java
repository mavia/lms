package ukma.baratheons.lms.entity;

import ukma.baratheons.lms.anorm.Id;

public enum ConfirmActionType {
	
	
	registration_user("registration_user"), reset_password("reset_password"), new_task("new_task"), new_task_with_reg("new_task_with_reg");
	
	@Id
	private final String value;
	
	private ConfirmActionType(String value){
		this.value = value;
	}
	
	
}
