package ukma.baratheons.lms.entity;

import java.sql.Timestamp;

import ukma.baratheons.lms.anorm.Base;
import ukma.baratheons.lms.anorm.DataBaseField;
import ukma.baratheons.lms.anorm.Embedded;
import ukma.baratheons.lms.anorm.Entity;
import ukma.baratheons.lms.anorm.Id;
import ukma.baratheons.lms.anorm.Transient;
import ukma.baratheons.lms.query.Table;

@Entity(Table.CommentTable.TABLE_NAME)
public class Comment implements Comparable<Comment> {

	@DataBaseField(Table.CommentTable.ID)
	@Id
	@Transient
	private Long id;

	@Base
	@DataBaseField(Table.CommentTable.TEXT)
	private String text;

	@Transient
	@Base
	@DataBaseField(Table.CommentTable.DATE)
	private Timestamp date;

	@Base
	@DataBaseField(Table.CommentTable.VISIBILITY)
	private int visibility;

	@Base
	@Transient
	@Embedded(value=User.class, nullable=true)
	@DataBaseField(Table.CommentTable.USER)
	private User user;

	@Base
	@Transient
	@Embedded(value=Task.class, nullable=false)
	@DataBaseField(Table.CommentTable.TASK)
	private Task task;

	public Comment() {
	}

	public Comment(long id, String text, Timestamp date, int visibility, long userId, long taskId) {
		super();
		this.id = id;
		this.text = text;
		this.date = date;
		this.visibility = visibility;
		this.user.setId(userId);
		this.task.setId(taskId);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public int getVisibility() {
		return visibility;
	}

	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = (int) (prime * result + user.getId());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public int compareTo(Comment o) {
		return getDate().compareTo(o.getDate());
	}

}