package ukma.baratheons.lms.entity;


import ukma.baratheons.lms.anorm.Base;
import ukma.baratheons.lms.anorm.DataBaseField;
import ukma.baratheons.lms.anorm.Embedded;
import ukma.baratheons.lms.anorm.Entity;
import ukma.baratheons.lms.anorm.Id;
import ukma.baratheons.lms.anorm.Transient;
import ukma.baratheons.lms.query.Table;

@Entity(Table.RoomTable.TABLE_NAME)
public class Room {
	
	@Id
	@Transient
	@DataBaseField(Table.RoomTable.ID)
	private Long id;
	
	@Base
	@DataBaseField(Table.RoomTable.NUMBER)
	private String number="";
	
	@Base
	@Embedded(value=User.class, nullable=true)
	@DataBaseField(Table.RoomTable.STAFF)
	private User user;
	
	private int malfunctionsCount;
	
	public Room(){};
	
	public long getId() {
		return id;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public User getUser(){
		return user;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Room other = (Room) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}

	public int getMalfunctionsCount() {
		return malfunctionsCount;
	}

	public void setMalfunctionsCount(int malfunctionsCount) {
		this.malfunctionsCount = malfunctionsCount;
	}
}