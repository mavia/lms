package ukma.baratheons.lms.entity;

import java.sql.Date;
import java.sql.Timestamp;

import ukma.baratheons.lms.anorm.Base;
import ukma.baratheons.lms.anorm.DataBaseField;
import ukma.baratheons.lms.anorm.Embedded;
import ukma.baratheons.lms.anorm.Entity;
import ukma.baratheons.lms.anorm.Id;
import ukma.baratheons.lms.anorm.Transient;
import ukma.baratheons.lms.query.Table;

@Entity(Table.TaskTable.TABLE_NAME)
public class Task {

	@Id
	@Transient
	@DataBaseField(Table.TaskTable.ID)
    private Long id;
	
	@DataBaseField(Table.TaskTable.PARENT_TASK)
	@Base
    private Long parentId;
	
	@DataBaseField(Table.TaskTable.STATUS)
	@Embedded(value=TaskStatus.class, nullable=false)
	@Base
    private TaskStatus taskStatus;
	
	@DataBaseField(Table.TaskTable.TYPE)
	@Embedded(value=TaskType.class, nullable=false)
	@Base
    private TaskType taskType;
	
	@DataBaseField(Table.TaskTable.EQUIPMENT)
	@Embedded(value=Equipment.class, nullable=false)
	@Base
    private Equipment eqpt;
	
	@DataBaseField(Table.TaskTable.CREATOR)
	@Base
	@Transient
	@Embedded(value=User.class, nullable=false)
    private User user;
	
	@DataBaseField(Table.TaskTable.STAFF)
	@Base
	@Embedded(value=User.class, nullable=false)
    private User staff;
	
	@DataBaseField(Table.TaskTable.CREATION_TIME)
	@Base
	@Transient
    private Timestamp creationTime;
	
	@DataBaseField(Table.TaskTable.COMPLETION_TIME)
    private Date completionTime;
	
	@DataBaseField(Table.TaskTable.URGENCY)
    private boolean urgency;
	
	@DataBaseField(Table.TaskTable.VISIBILITY)
	@Base
    private boolean visibility;

	public Task() {}

    public Task(long id, long parentId, TaskStatus taskStatus, TaskType taskType,
                Equipment eqpt, User user, User staff, Timestamp creationTime,
                Date completionTime) {
        this.id = id;
        this.parentId = parentId;
        this.taskStatus = taskStatus;
        this.taskType = taskType;
        this.eqpt = eqpt;
        this.user = user;
        this.staff = staff;
        this.creationTime = creationTime;
        this.completionTime = completionTime;
    }

    public Task(TaskStatus taskStatus, TaskType taskType, Equipment eqpt, User user,
                Timestamp creationTime,
                boolean urgency, boolean visibility) {
        this.taskStatus = taskStatus;
        this.taskType = taskType;
        this.eqpt = eqpt;
        this.user = user;
        this.creationTime = creationTime;
        this.urgency = urgency;
        this.visibility = visibility;
    }

    public Task(long id, TaskStatus taskStatus, TaskType taskType, Equipment eqpt,
                User user, Timestamp creationTime,
                boolean urgency, boolean visibility) {
        this.id = id;
        this.taskStatus = taskStatus;
        this.taskType = taskType;
        this.eqpt = eqpt;
        this.user = user;
        this.creationTime = creationTime;
        this.urgency = urgency;
        this.visibility = visibility;
    }

    public Task(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public TaskStatus getTaskStatus(){
    	return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public TaskType getTaskType(){
    	return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }

    public Equipment getEqpt(){
    	return eqpt;
    }

    public void setEqpt(Equipment eqpt) {
        this.eqpt = eqpt;
    }

    public User getUser(){
    	return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getStaff(){
    	return staff;
    }

    public void setStaff(User staff) {
        this.staff = staff;
    }

    public Timestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    public Date getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(Date completionTime) {
        this.completionTime = completionTime;
    }

    public boolean isUrgency() {
        return urgency;
    }

    public void setUrgency(boolean urgency) {
        this.urgency = urgency;
    }

    public boolean isVisibility() {
        return visibility;
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationTime == null) ? 0 : creationTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (creationTime == null) {
			if (other.creationTime != null)
				return false;
		} else if (!creationTime.equals(other.creationTime))
			return false;
		if (!eqpt.equals(other.eqpt))
			return false;
		if (!taskStatus.equals(other.taskStatus))
			return false;
		if (!taskType.equals(other.taskType))
			return false;
		return true;
	}

}
