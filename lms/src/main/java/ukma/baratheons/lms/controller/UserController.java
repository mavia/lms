package ukma.baratheons.lms.controller;

import java.security.Principal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ukma.baratheons.lms.entity.ConfirmActionType;
import ukma.baratheons.lms.entity.Role;
import ukma.baratheons.lms.entity.Unconfirmed;
import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.service.ConfirmationService;
import ukma.baratheons.lms.service.RegistrationService;
import ukma.baratheons.lms.service.ResetPasswordService;
import ukma.baratheons.lms.service.StaticConstant;
import ukma.baratheons.lms.service.UserAdministrationService;
import ukma.baratheons.lms.service.ban.BanUser;
import ukma.baratheons.lms.util.BitUtils;
import ukma.baratheons.lms.util.EmailUtils;
import ukma.baratheons.lms.util.MD5;

@Controller
public class UserController {

	@Autowired
	private BanUser banUser;

	@Autowired
	private UserAdministrationService userService;

	@Autowired
	private RegistrationService registrationService;

	@Autowired
	private ConfirmationService confirmationService;

	@Autowired
	private ResetPasswordService resetPasswordService;

	private ModelAndView modelAndView = new ModelAndView();

	private static final Logger LOGGER = LogManager
			.getLogger(TaskController.class);

	@RequestMapping(path = "/create")
	public void createUser(User user) {
		try {
			userService.create(user);
			LOGGER.info("Create new user with email: " + user.getEmail());
		} catch (SQLException e) {
			LOGGER.info("Failed to create new user " + e.toString());
			e.printStackTrace();
		}
	}

	/**
	 * Page for authenticated users.
	 * 
	 * @param RequestMapping
	 *            URL of profile page
	 * @param principal
	 *            represent authenticated users
	 * @return name of profile page
	 * */
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String profilePage(Model model, Principal principal) {
		User currentUser = userService.getUserByEmail(principal.getName());

		if (currentUser.getBanEndTime() == null
				|| (currentUser.getBanEndTime() != null && currentUser
						.getBanEndTime()
						.before(new Timestamp((new java.util.Date()).getTime())))) {
			model.addAttribute("notBan", "-");
		}

		model.addAttribute("user", currentUser);
		LOGGER.info("Profile page for user with email: "
				+ currentUser.getEmail());
		return "profile";
	}

	/**
	 * Page for authenticated users, where they can change account information.
	 * 
	 * @param RequestMapping
	 *            URL of profile page
	 * @param id
	 *            ID of current user
	 * @param request
	 *            represent authenticated users
	 * @param admin
	 *            not NULL if admin edit profile of some user
	 * @param notMatch
	 *            not NULL if user enter wrong password
	 * @param emailError
	 *            not NULL if Admin enter password, which already exists in the
	 *            database
	 * @return name of edit page
	 * */
	@RequestMapping(value = "/profile/edit/id/{id}", method = RequestMethod.GET)
	public String editProfilePage(
			Model model,
			SecurityContextHolderAwareRequestWrapper request,
			@PathVariable long id,
			@RequestParam(value = "adminEdit", required = false) String admin,
			@RequestParam(value = "notMatch", required = false) String notMatch,
			@RequestParam(value = "emailError", required = false) String emailError) {
		User currentUser = userService.getUserByEmail(request.getRemoteUser());
		User myUser = userService.getUserById(id);
		if ((request.isUserInRole("admin") && admin != null)
				|| currentUser.getId() == id) {
			User user = userService.getUserById(id);
			if (admin != null) {
				if (myUser.getRole().getId() == StaticConstant.Role.ADMIN
						&& !BitUtils.isBitSet(1, currentUser.getRights()))
					return "redirect:../../../403";
				model.addAttribute("admin", "Admin can edit all user profile");
				if (BitUtils.isBitSet(1, currentUser.getRights()))
					model.addAttribute("chief", "chief");
			} else {
				user.setPassword(null);
			}

			if (notMatch != null) {
				model.addAttribute("notMatch", "Wrong password!!!");
			}

			if (emailError != null) {
				model.addAttribute("emailError", "Email " + emailError
						+ " already exists in the database!");
			}

			model.addAttribute("user", user);
			LOGGER.info("Edit profile page for user with email: "
					+ currentUser.getEmail());
			return "edit_profile";
		}

		return "redirect:../../../403";
	}

	/**
	 * Edit account information.
	 * 
	 * @param RequestMapping
	 *            URL of profile page
	 * @param id
	 *            ID of current user
	 * @param user
	 *            editable user
	 * @param request
	 *            represent authenticated users
	 * @param admin
	 *            not NULL if admin edit profile of some user
	 * @return name of profile page
	 * */
	@RequestMapping(value = "/profile/edit/id/{id}", method = RequestMethod.POST)
	public String editPage(Model model, @ModelAttribute("user") User user,
			HttpServletRequest req,
			SecurityContextHolderAwareRequestWrapper request,
			@PathVariable long id,
			@RequestParam(value = "adminEdit", required = false) String admin) {

		User user1 = userService.getUserByEmail(user.getEmail());

		if (admin == null) {
			if (user1.getPassword().equals(MD5.getHash(user.getPassword()))) {
				if (req.getParameter("pass").equals("")) {
					user.setPassword(user1.getPassword());
				} else {
					user.setPassword(MD5.getHash(req.getParameter("pass")));
				}
			} else {
				return "redirect:" + id + "?notMatch";
			}
		}

		user.setId(id);

		if (admin != null) {
			if (user1 != null && user1.getId() != id) {
				return "redirect:" + id + "?adminEdit&&emailError="
						+ user.getEmail();
			}
			try {
				userService.update(user);
			} catch (SQLException e) {
				LOGGER.error("Update user: " + id);
				model.addAttribute("title", "Oooops!");
				model.addAttribute("head", "500 Server Error");
				model.addAttribute("img", "/lms/resources/img/deny.png");
				model.addAttribute(
						"description",
						"Error, enable to confirm user email, please try again later");
				return "message";
			}
			return "redirect:../../../userAdmin";
		}
		try {
			userService.update(user);
		} catch (SQLException e) {
			LOGGER.error("Update user: " + id);
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute(
					"description",
					"Error, enable to change user password, please try again");
			return "message";
		}
		return "redirect:../..";
	}

	@RequestMapping(value = "/profile/edit/unbanUser/{id}", method = RequestMethod.GET)
	public String unban(SecurityContextHolderAwareRequestWrapper request,
			@PathVariable long id,
			@RequestParam(value = "adminEdit", required = false) String admin,
			Model model) {
		LOGGER.info("Try to unban user with id: " + id);
		if ((request.isUserInRole("admin") && admin != null)) {
			try {
				userService.unban(id);
			} catch (SQLException e) {
				LOGGER.error("Unbanning user id: " + id);
				model.addAttribute("title", "Oooops!");
				model.addAttribute("head", "500 Server Error");
				model.addAttribute("img", "/lms/resources/img/deny.png");
				model.addAttribute(
						"description",
						"Сталася помилка, не вдалося розбанити користувача. Спробуйте, будь ласка, ще раз");
				return "message";
			}
			return "redirect:/userAdmin";
		}
		return "redirect:../../../403";
	}

	/**
	 * Page for admin, where he can change user information, add new user.
	 * 
	 * @param RequestMapping
	 *            URL of profile page
	 * @param principal
	 *            represent authenticated users
	 * @return name of userAdministration page
	 * */
	@RequestMapping(value = "/userAdmin", method = RequestMethod.GET)
	public String userAdminPage(Model model, Principal principal) {
		List<User> users = null;
		User currentUser = userService.getUserByEmail(principal.getName());
		if (BitUtils.isBitSet(1, currentUser.getRights())) {
			model.addAttribute("chief", "chief");
			users = userService.getUsers();
		} else {
			users = userService.getUsersForNonChief();
		}
		for (User user : users) {
			if (user.getBanEndTime() != null
					&& user.getBanEndTime().before(
							new Timestamp((new java.util.Date()).getTime())))
				user.setBanEndTime(null);
		}
		model.addAttribute("users", users);
		model.addAttribute("roles", userService.getRoles());
		return "user_administration";
	}

	@RequestMapping(value = "/inviteUser", method = RequestMethod.POST)
	public String createInvitation(Model model, HttpServletRequest request) {
		List<String> alreadyRegistratedOrNotValid = new ArrayList<String>();
		try {
			Role role = userService.getRoleById(Integer.valueOf(request
					.getParameter("role")));
			StringTokenizer st = new StringTokenizer(request.getParameter("emails"));
			while(st.hasMoreTokens()){
				String email = st.nextToken();
				if(userService.getUserByEmail(email) == null && EmailUtils.isValidEmail(email)){
					registrationService.createUnconfirmed(email, role);
					LOGGER.info("Invite user with email: " + email);
				}else{
					alreadyRegistratedOrNotValid.add(email);
				}
			}
		} catch (SQLException e) {
			LOGGER.error("Inviting user with email: "
					+ request.getParameter("email"));
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"Сталася помилка, не вдалося запросити користувача "
							+ request.getParameter("email")
							+ ". Спробуйте, будь ласка, ще раз");
			return "message";
		}
		model.addAttribute("alreadyRegistrated", alreadyRegistratedOrNotValid);
		return "redirect:userAdmin";
	}

	@RequestMapping(value = "/banUser", method = RequestMethod.POST)
	public String banUser(HttpServletRequest request, Model model) {
		Long userId = Long.valueOf(request.getParameter("userId"));
		String time = request.getParameter("time");
		try {
			banUser.banUserById(userId, time);
		} catch (SQLException e) {
			LOGGER.error("Banning user id: " + userId);
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute(
					"description",
					"Сталася помилка, не вдалося забанити користувача. Спробуйте, будь ласка, ще раз");
			return "message";
		}
		LOGGER.info("Ban user with id: " + userId + "; time: " + time);
		return "redirect:userAdmin";
	}

	/**
	 * Page for registration by invitation
	 * 
	 * @param Hash
	 *            which is in personal link which comes at users email
	 * @return name of registration page
	 * */
	@RequestMapping(value = "/registration/{hash}/{key}", method = RequestMethod.GET)
	public String registrationPage(@PathVariable("hash") String hash,
			@PathVariable("key") int key, Model model) {
		Unconfirmed unUser = userService.getUnconfirmedByHash(hash);
		if (unUser == null || !confirmationService.check(hash, key)) {
			model.addAttribute("title", "Oooops!");
			return "404";
		} else {
			return "user_registration";
		}
	}

	@RequestMapping(value = "/registration/{hash}/{key}", method = RequestMethod.POST)
	public ModelAndView registrate(@PathVariable("hash") String hash,
			@PathVariable("key") int key, Model model,
			HttpServletRequest request) {
		Unconfirmed unUser = userService.getUnconfirmedByHash(hash);
		if (unUser == null || !confirmationService.check(hash, key)) {
			model.addAttribute("title", "Oooops!");
			modelAndView.setViewName("redirect:../../404");
			return modelAndView;
		} else {
			String email = unUser.getEmail();
			String password = request.getParameter("password");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			
			registrationService.registrateNewUser(email, password, firstName, lastName, key);
			
			userService.deleteUnconfirmedByHash(unUser.getHash());
			Authentication auth = new UsernamePasswordAuthenticationToken(
					email, password);
			SecurityContextHolder.getContext().setAuthentication(auth);

			LOGGER.info("Ending user's registration; email: "
					+ email);
			modelAndView.setViewName("redirect:../../welcome");
			return modelAndView;
		}
	}

	@RequestMapping(value = "/confirmUser/{hash}/{key}", method = RequestMethod.GET)
	public String confirmUser(@PathVariable("hash") String hash,
			@PathVariable("key") long key, Model model) {
		Unconfirmed unUser = userService.getUnconfirmedByHash(hash);
		if (unUser == null || !confirmationService.check(hash, key)) {
			model.addAttribute("title", "Oooops!");
			modelAndView.setViewName("redirect:../../404");
			return "404";
		} else {
			try {
				confirmationService.confirm(unUser, key);
				model.addAttribute("title", "Ok!");
				model.addAttribute("head", "Email confirmation page");
				model.addAttribute("img", "/lms/resources/img/ok.png");
				model.addAttribute("description","You have confirmed your email successfully, please check your email to get details.");

			} catch (SQLException e) {
				LOGGER.error("Confirming user");
				model.addAttribute("title", "Oooops!");
				model.addAttribute("head", "500 Server Error");
				model.addAttribute("img", "/lms/resources/img/deny.png");
				model.addAttribute(
						"description",
						"Error, enable to confirm user email, please try again later");
				return "message";
			}
			return "message";
		}
	}

	@RequestMapping(value = "/resetPass/{hash}/{key}", method = RequestMethod.GET)
	public String resetPass(@PathVariable("hash") String hash,
			@PathVariable("key") long key, Model model) {
		Unconfirmed unUser = userService.getUnconfirmedByHash(hash);
		if (unUser == null || !confirmationService.check(unUser.getHash(), key)) {
			model.addAttribute("title", "Oooops!");
			modelAndView.setViewName("redirect:../../404");
			return "404";
		} else {

			model.addAttribute("title", "Oooops!");

			try {
				confirmationService.confirm(unUser, key);
				model.addAttribute("title", "Ok!");
				model.addAttribute("head", "Email confirmation page");
				model.addAttribute("img", "/lms/resources/img/ok.png");
				model.addAttribute("description","You have confirmed your email successfully, please check your email to get details.");

			} catch (SQLException e) {
				LOGGER.error("Confirming password change");
				model.addAttribute("title", "Oooops!");
				model.addAttribute("head", "500 Server Error");
				model.addAttribute("img", "/lms/resources/img/deny.png");
				model.addAttribute("description",
						"Error, enable to change user password, please try again");
				return "message";
			}
			return "message";
		}
	}

	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> resetPassword(
			SecurityContextHolderAwareRequestWrapper request,
			HttpServletRequest req, Model model) {
		String email = req.getParameter("email");
		String reset_sec = req.getParameter("reset_sec");
		Map<String, String> res = new HashMap<String, String>();
		if (request.getSession(true).getAttribute("reset_sec")
				.equals(reset_sec)) {
			if (userService.getUserByEmail(email) != null) {
				try {
					resetPasswordService.reset(email);
					request.getSession(true).setAttribute("reset_sec",
							MD5.getRandomHash());
					res.put("status", "ok");
					res.put("message",
							"Please check your email and confirm the request!");
					return res;
				} catch (SQLException e) {
					res.put("status", "fail");
					res.put("message",
							"Error! Something went wrong when resetting password");
					return res;
				}
			} else {
				res.put("status", "fail");
				res.put("message", "Error! Email doesn't exist in system.");
				return res;
			}
		} else {
			res.put("status", "fail");
			res.put("message", "Error occured while sending form!");
			return res;
		}
	}

}
