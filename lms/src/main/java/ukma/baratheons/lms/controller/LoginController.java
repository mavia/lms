package ukma.baratheons.lms.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ukma.baratheons.lms.entity.Room;
import ukma.baratheons.lms.entity.TaskType;
import ukma.baratheons.lms.service.RoomService;
import ukma.baratheons.lms.service.TaskManagementService;
import ukma.baratheons.lms.service.UserAdministrationService;
import ukma.baratheons.lms.util.BitUtils;
import ukma.baratheons.lms.util.MD5;

@Controller
public class LoginController {

	private static final Logger LOGGER = LogManager
			.getLogger(LoginController.class);

	@Autowired
	private UserAdministrationService userService;

	@Autowired
	private RoomService roomService;

	@Autowired
	private TaskManagementService taskService;

	/**
	 * main page The main page of site.
	 * 
	 * @param RequestMapping
	 *            URL of main page
	 * @param error
	 *            true if User write wrong login or password
	 * @param attention
	 *            true if User go to page, which is designed for authenticated
	 *            users
	 * @param login
	 *            change lastOnline attribute of User
	 * @param createTask
	 *            not NULL if user just create task
	 * @return name of main page
	 * */
	@RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
	public String welcomePage(
			Model model,
			SecurityContextHolderAwareRequestWrapper request,
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "attention", required = false) String attention,
			@RequestParam(value = "login", required = false) String login,
			@RequestParam(value = "createTask", required = false) String createTask) {

		request.getSession(true).setAttribute("reset_sec", MD5.getRandomHash());
		
		model.addAttribute("reset_sec",
				request.getSession().getAttribute("reset_sec"));
		
		request.getSession().setAttribute("create_task_sec", MD5.getRandomHash());
		
		model.addAttribute("create_task_sec",
				request.getSession().getAttribute("create_task_sec"));

		List<Room> rooms = roomService.getRoomsLazy(null);
		model.addAttribute("rooms", rooms);
		ArrayList<TaskType> types = taskService.getTaskTypes();
		model.addAttribute("typesTask", types);
		if (request.isRequestedSessionIdValid()) {
			if (request.isUserInRole("admin")) {
				if (BitUtils.isBitSet(1,
						userService.getUserByEmail(request.getRemoteUser())
								.getRights())) {
					model.addAttribute("role", "Chief Admin");
				} else {
					model.addAttribute("role", "Admin");
				}
			} else if (request.isUserInRole("user")) {
				model.addAttribute("role", "Client");
			} else if (request.isUserInRole("laborant")) {
				model.addAttribute("role", "Staff member");
			}
		}

		if (error != null) {
			model.addAttribute("msg", "Login/password is invalid");
		}

		if (attention != null) {
			model.addAttribute("msg", "First log in, please");
		}

		if (login != null) {
			try {
				userService.updateLastOnline(request.getRemoteUser());
			} catch (SQLException e) {
				LOGGER.error("Updating lastOnline for user id: "
						+ request.getRemoteUser());
			}
		}

		if (createTask != null) {
			model.addAttribute("createTask",
					"Please check your email and confirm the task!");
		}

		LOGGER.info("Main page");
		return "main";
	}

	/**
	 * Deny page - if authenticated users go to page, which is designed for
	 * another role.
	 * 
	 * @param RequestMapping
	 *            URL of deny page
	 * @return name of deny page
	 * */
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accessDenied(Model model) {
		LOGGER.info("403 page");
		model.addAttribute("title", "Oooops!");
		model.addAttribute("head", "403 Forbidden");
		model.addAttribute("img", "/lms/resources/img/deny.png");
		model.addAttribute("description", "You can not access to this page");
		return "message";
	}

	/**
	 * Error page - if users go to page, which doesn't exist.
	 * 
	 * @param RequestMapping
	 *            URL of error page
	 * @return name of error page
	 * */
	@RequestMapping(value = "/404", method = RequestMethod.GET)
	public String error404(Model model) {
		LOGGER.info("404 page");
		model.addAttribute("title", "Oooops!");
		model.addAttribute("head", "404 Not Found");
		model.addAttribute("img", "/lms/resources/img/deny.png");
		model.addAttribute("description", "This page not exist");
		return "message";
	}

	/**
	 * Error page - server error.
	 * 
	 * @param RequestMapping
	 *            URL of error page
	 * @return name of error page
	 * */
	@RequestMapping(value = "/500", method = RequestMethod.GET)
	public String error500(Model model) {
		LOGGER.error("500 page");
		model.addAttribute("title", "Oooops!");
		model.addAttribute("head", "500 Server Error");
		model.addAttribute("img", "/lms/resources/img/deny.png");
		model.addAttribute(
				"description",
				"Sorry, but the server encountered an u"
						+ "nexpected condition which prevented it <br/> from fulfilling the "
						+ "request. Please, return to main page and try again.");
		return "message";
	}
}
