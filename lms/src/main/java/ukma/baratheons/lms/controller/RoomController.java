package ukma.baratheons.lms.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import ukma.baratheons.lms.entity.Room;
import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.service.RoomService;
import ukma.baratheons.lms.service.UserAdministrationService;

@Controller
@RequestMapping("/rooms")
public class RoomController {
	
	@Autowired
	private RoomService roomService;
	
	@Autowired 
	UserAdministrationService userService;

	@RequestMapping(path="/list")
	public String listRooms(Model model) throws SQLException {
		List<Room>rooms = roomService.getAllRooms();
		model.addAttribute("rooms", rooms);
		List<User>staff = userService.getStaff();
		model.addAttribute("staff", staff);
		return "rooms";
	}
}
