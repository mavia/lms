package ukma.baratheons.lms.controller;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ukma.baratheons.lms.entity.*;
import ukma.baratheons.lms.query.SearchEngine;
import ukma.baratheons.lms.query.Table;
import ukma.baratheons.lms.service.CommentService;
import ukma.baratheons.lms.service.RoomService;
import ukma.baratheons.lms.service.TaskManagementService;
import ukma.baratheons.lms.service.UserAdministrationService;
import ukma.baratheons.lms.service.ban.BanUser;
import ukma.baratheons.lms.util.MD5;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TaskController {

	@Autowired
	private RoomService roomService;

	@Autowired
	private TaskManagementService taskService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private UserAdministrationService userService;

	@Autowired
	private BanUser banUser;
	
	@Autowired
	private SearchEngine<Task> taskSearcher;

	private static final Logger LOGGER = LogManager
			.getLogger(TaskController.class);

	/**
	 * Page for authenticated users, where they can report a problem.
	 * 
	 * @param RequestMapping
	 *            URL of create task page
	 * @param principal
	 *            represent authenticated users
	 * @return name of create task page
	 * */
	@RequestMapping(path = "/tasks/create", method = RequestMethod.GET)
	public String createTaskPage(Model model,SecurityContextHolderAwareRequestWrapper request) {
		LOGGER.info("Create task page");
		List<Room> rooms = roomService.getRoomsLazy(null);
		ArrayList<TaskType> types = taskService.getTaskTypes();
		request.getSession().setAttribute("create_task_sec", MD5.getRandomHash());
		
		model.addAttribute("create_task_sec",
				request.getSession().getAttribute("create_task_sec"));
		model.addAttribute("typesTask", types);
		model.addAttribute("rooms", rooms);
		return "create_task";
	}

	/**
	 * Update status of task by Client.
	 * 
	 * @param RequestMapping
	 *            URL of create task page
	 * @param id
	 *            ID of current task
	 * @return name of task page
	 * */
	@RequestMapping(path = "/tasks/cancelUser/{id}", method = RequestMethod.GET)
	public String cancelTaskByUser(Model model, @PathVariable long id) {
		try {
			taskService.editTaskByUser(taskService.getTasksById(id));
		} catch (SQLException e) {
			LOGGER.error("Cancelling task by user; task id: " + id);
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"������� �������, �� ������� ��������� ��������. ���������, ���� �����, �� ���");
			return "message";
		}
		LOGGER.info("Cancel task by user; task id: " + id);
		return "redirect:..";
	}

	/**
	 * Cancel task by Staff member.
	 * 
	 * @param RequestMapping
	 *            URL of create task page
	 * @param id
	 *            ID of current task
	 * @return name of task page
	 * */
	@RequestMapping(path = "/tasks/cancelStaff/{id}", method = RequestMethod.GET)
	public String cancelTaskByStaff(Model model, @PathVariable long id) {
		try {
			taskService.cancelTaskByStaff(taskService.getTasksById(id));
		} catch (SQLException e) {
			LOGGER.error("Cancelling task by staff; task id: " + id);
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"������� �������, �� ������� ��������� ��������. ���������, ���� �����, �� ���");
			return "message";
		}
		LOGGER.info("Cancel task by staff; task id: " + id);
		return "redirect:..";
	}

	/**
	 * Done task by Staff member.
	 * 
	 * @param RequestMapping
	 *            URL of create task page
	 * @param id
	 *            ID of current task
	 * @return name of task page
	 * */
	@RequestMapping(path = "/tasks/doneStaff/{id}", method = RequestMethod.GET)
	public String doneTaskByStaff(Model model, @PathVariable long id) {
		try {
			taskService.doneTaskByStaff(id);
		} catch (SQLException e) {
			LOGGER.error("Finishing task by staff; task id: " + id);
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"������� �������, �� ������� ��������� ��������. ���������, ���� �����, �� ���");
			return "message";
		}
		LOGGER.info("Done task by staff; task id: " + id);
		return "redirect:..";
	}

	/**
	 * Update status of task by Administrator.
	 * 
	 * @param RequestMapping
	 *            URL of create task page
	 * @param id
	 *            ID of current task
	 * @return name of task page
	 * */
	@RequestMapping(path = "/tasks/resumeUser/{id}", method = RequestMethod.GET)
	public String resumeTaskByAdmin(Model model, @PathVariable long id) {
		try {
			taskService.editTaskByAdmin(taskService.getTasksById(id));
		} catch (SQLException e) {
			LOGGER.error("Resume task by admin; task id: " + id);
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"������� �������, �� ������� �������� ��������. ���������, ���� �����, �� ���");
			return "message";
		}
		LOGGER.info("Resume task by admin; task id: " + id);
		return "redirect:..";
	}

	/**
	 * Page for authenticated users, where they can view and add comments.
	 * 
	 * @param RequestMapping
	 *            URL of create task page
	 * @param id
	 *            ID of current task
	 * @param changeStatus
	 *            true if some user change status of task
	 * @return name of comment page
	 * */
	@RequestMapping(path = "/tasks/comments/{id}", method = RequestMethod.GET)
	public String getCommentByTask(
			Model model,
			@PathVariable long id,
			@RequestParam(value = "changeStatus", required = false) String changeStatus) {
		LOGGER.info("Comment page for task with id: " + id);
		List<Comment> comments = commentService.getCommentsByTask(id);
		if (comments != null)
			Collections.sort(comments);

		model.addAttribute("comments", comments);
		model.addAttribute("idTask", id);

		if (changeStatus != null) {
			model.addAttribute("changeStatus", changeStatus);
		}

		return "comments";
	}

	/**
	 * Page with tasks for users by role.
	 * 
	 * @param RequestMapping
	 *            URL of profile page
	 * @param request
	 *            represent authenticated users
	 * @param createTask
	 *            not NULL if user just create task
	 * @return name of task page
	 * */
	@RequestMapping(path = "/tasks", method = RequestMethod.GET)
	public String viewAllTasks(
			Model model,
			SecurityContextHolderAwareRequestWrapper request,
			@RequestParam(value = "createTask", required = false) String createTask) {
		User user = userService.getUserByEmail(request.getRemoteUser());
		LOGGER.info("All tasks page for user with email: " + user.getEmail());

		List<Task> tasks = null;
		if (request.isUserInRole("admin")) {
			tasks = taskService.getAllTasksLazy();
		}
		if (request.isUserInRole("user")) {
			tasks = taskService.getTasksByUser(request.getRemoteUser());
		}
		if (request.isUserInRole("laborant")) {
			tasks = taskService.getTasksByStaff(user.getId());
			for (Iterator<Task> taskIter = tasks.iterator(); taskIter.hasNext();) {
				Task t = taskIter.next();
				if (!t.getTaskStatus().getName().equals("open")) {
					if (t.getId() != t.getParentId()) {
						taskIter.remove();
					}
				}
			}
		}

		if (user.getBanEndTime() != null
				&& user.getBanEndTime().after(
						new Timestamp((new java.util.Date()).getTime())))
			model.addAttribute("ban", "ban");

		if (createTask != null) {
			model.addAttribute("createTask", "Thank you for your report!");
		}

		model.addAttribute("tasks", tasks);
		return "tasks";
	}

	/**
	 * Add new comment.
	 * 
	 * @param RequestMapping
	 *            URL of profile page
	 * @param req
	 *            represent authenticated users
	 * @param request
	 *            parameters from form
	 * @param changeStatus
	 *            true if some user change status of task
	 * @return name of comment page
	 * */
	@RequestMapping(path = "/createComment", method = RequestMethod.POST)
	public String createComment(
			Model model,
			SecurityContextHolderAwareRequestWrapper req,
			HttpServletRequest request,
			@RequestParam(value = "changeStatus", required = false) String changeStatus) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		long taskId = Long.valueOf(request.getParameter("idTask"));
		String text = String.valueOf(request.getParameter("commentText"));
		User user = userService.getUserByEmail(req.getRemoteUser());

		try {
			commentService.addNew(taskId, user, text);
		} catch (SQLException e) {
			LOGGER.error("Adding comment for task id: " + taskId);
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"������� �������, �� ������� ������ ��������. ���������, ���� �����, �� ���");
			return "message";
		}

		if (changeStatus != null) {
			if (changeStatus.equals("user")) {
				return "redirect:tasks/cancelUser/" + taskId;
			}
			if (changeStatus.equals("admin")) {
				return "redirect:tasks/resumeUser/" + taskId;
			}
			if (changeStatus.equals("laborant1")) {
				try {
					banUser.increasePenaltyScoreAndBanTime(userService
							.getUserIdByTask(taskId));
				} catch (SQLException e) {
					LOGGER.error("Increasing penalty for task id: " + taskId);
				}
				return "redirect:tasks/cancelStaff/" + taskId;
			}
			if (changeStatus.equals("laborant2")) {
				return "redirect:tasks/doneStaff/" + taskId;
			}
			if (changeStatus.equals("laborant3")) {
				return "redirect:tasks/refuseTask/" + taskId;
			}
		}

		LOGGER.info("Create new comment for task with id: " + taskId
				+ " by user with id: " + user.getId());
		return "redirect:tasks/comments/" + taskId;
	}

	@RequestMapping(path = "/tasks/refuseTask/{id}", method = RequestMethod.GET)
	public String refuseTaskByStaff(@PathVariable long id, Model model) {
		try {
			taskService.refuseTaskByStaff(id);
		} catch (SQLException e) {
			LOGGER.error("Refuse task by staff; task id: " + id);
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"������� �������, ��� �� ������� ���������� �� ��������");
			return "message";
		}
		LOGGER.info("Refuse task by staff; task id: " + id);
		return "redirect:..";
	}

	/**
	 * Add new task.
	 * 
	 * @param RequestMapping
	 *            URL of profile page
	 * @param req
	 *            represent users
	 * @return name of page
	 * */
	@RequestMapping(value = "/createTask", method = RequestMethod.POST)
	public @ResponseBody Map<String, String> createTask(
			SecurityContextHolderAwareRequestWrapper request,
			HttpServletRequest req, Model model) {
		String create_task_sec = req.getParameter("create_task_sec");
		long roomId = Long.valueOf(req.getParameter("room"));
		long eqptId = Long.valueOf(req.getParameter("eqpt"));
		long taskTypeId = Long.valueOf(req.getParameter("typeTask"));
		String description = String.valueOf(req.getParameter("postalAddress"));
		String email = req.getParameter("inputEmail");
		boolean is_auth=false;
		if (!"anonymousUser".equals(SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal())) {
			org.springframework.security.core.userdetails.User us = (org.springframework.security.core.userdetails.User) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal();
			email = us.getUsername();
			is_auth=true;
		}

		Map<String, String> res = new HashMap<String, String>();
		if (request.getSession(true).getAttribute("create_task_sec")
				.equals(create_task_sec)) {
			try {
				taskService.add(roomId, eqptId, taskTypeId, description, email);
			} catch (SQLException e) {
				LOGGER.error("Creating new task {USER EMAIL: " + email
						+ " COMMENT: " + description + "}");
				res.put("status", "fail");
				res.put("message",
						"Error! Something went wrong when creating task.");
				return res;
			}
			LOGGER.info("Create new task {USER EMAIL: " + email + " COMMENT: "
					+ description + "}");
			request.getSession(true).setAttribute("create_task_sec",
					MD5.getRandomHash());
			res.put("status", "ok");
			if (is_auth){
				res.put("message",
						"Thank you for your report!");
			}
			else
			res.put("message",
					"Please check your email and confirm the request!");
			return res;

		} else {
			res.put("status", "fail");
			res.put("message", "Error occured while sending form!");
			return res;
		}
	}

	@RequestMapping(path = "/createTask1", method = RequestMethod.POST)
	public String createTask1(SecurityContextHolderAwareRequestWrapper req,
			HttpServletRequest request, Model model) {
		long roomId = Long.valueOf(request.getParameter("room"));
		long eqptId = Long.valueOf(request.getParameter("eqpt"));
		long taskTypeId = Long.valueOf(request.getParameter("typeTask"));
		String description = String.valueOf(request
				.getParameter("postalAddress"));
		String email = String.valueOf(request.getParameter("inputEmail"));

		String redirect = "redirect:welcome?createTask";
		if (req.isUserInRole("admin") || req.isUserInRole("laborant")
				|| req.isUserInRole("user")) {
			email = req.getRemoteUser();
			redirect = "redirect:tasks?createTask";
		}

		try {
			taskService.add(roomId, eqptId, taskTypeId, description, email);
		} catch (SQLException e) {
			LOGGER.error("Creating new task {USER EMAIL: " + email
					+ " COMMENT: " + description + "}");
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute(
					"description",
					"������� �������, ��� �� ������� ��������� ��� ��������. ���������, ���� �����, �� ���");
			return "message";
		}
		LOGGER.info("Create new task {USER EMAIL: " + email + " COMMENT: "
				+ description + "}");
		return redirect;
	}

	@RequestMapping(path = "/takeTask/{id}", method = RequestMethod.POST)
	public String takeTask(Model model, @PathVariable long id,
			SecurityContextHolderAwareRequestWrapper req,
			HttpServletRequest request) {
		long taskTypeId = Long.valueOf(request.getParameter("typeTask"));
		long parentId = Long.valueOf(request.getParameter("sameTasks"));
		try {
			taskService.takeTask(id, taskTypeId, parentId);
		} catch (SQLException e) {
			LOGGER.error("Taking task by staff; task id: " + id);
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"������� �������, ��� �� ������� ����� ��������. ���������, ���� �����, �� ���");
			return "message";
		}
		LOGGER.info("Take task by staff; task id: " + id);
		return ("redirect:../tasks");
	}

	@RequestMapping(path = "/changeTaskStaff/{id}", method = RequestMethod.POST)
	public String changeTaskStaff(Model model, @PathVariable long id,
			SecurityContextHolderAwareRequestWrapper req,
			HttpServletRequest request) {
		String email = req.getRemoteUser();
		User staff = userService.getUserByEmail(email);
		try {
			taskService.setTaskStaff(id, staff);
		} catch (SQLException e) {
			LOGGER.error("Changing task staff; task id: " + id + "; staff id: "
					+ staff.getId() + "; staff email: " + staff.getEmail());
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"������� �������, �� ������� ������ ���������. ���������, ���� �����, �� ���");
			return "message";
		}
		LOGGER.info("Change task staff; task id: " + id + "; staff id: "
				+ staff.getId() + "; staff email: " + staff.getEmail());
		return ("redirect:../tasks");
	}

	@RequestMapping(path = "/tasks/task/{id}", method = RequestMethod.GET)
	public String getTask(
			Model model,
			@PathVariable long id,
			@RequestParam(value = "changeStatus", required = false) String changeStatus) {
		LOGGER.info("Get task by staff; task id: " + id);
		Task currentTask = taskService.getTasksById(id);
		List<Comment> comments = commentService.getCommentsByTask(id);
		HashMap<Task, Comment> sameTasks = taskService
				.getSameTasks(currentTask);
		ArrayList<TaskType> types = taskService.getTaskTypes();
		model.addAttribute("typesTask", types);

		model.addAttribute("currentTask", currentTask);
		model.addAttribute("comments", comments);
		model.addAttribute("sameTasks", sameTasks);
		if (changeStatus != null) {
			model.addAttribute("changeStatus", changeStatus);
		}
		return "task";
	}

	@RequestMapping(path = "/freeTasks", method = RequestMethod.GET)
	public String getFreeTask(Model model) {
		LOGGER.info("Show all free tasks");
		ArrayList<Task> tasks = (ArrayList<Task>) taskService.getFreeTasks();
		ArrayList<User> staffs = (ArrayList<User>) userService
				.getStaffByActive(true);
		List<Task> refTasks = taskService.getRefusedTasks();
		model.addAttribute("tasks", tasks);
		model.addAttribute("staffs", staffs);
		model.addAttribute("refTasks", refTasks);
		return "freeTasks";
	}

	@RequestMapping(path = "/setTask/{taskId}/{staffId}", method = RequestMethod.POST)
	public String setTask(Model model, @PathVariable long taskId,
			@PathVariable long staffId) {
		User staff = userService.getUserById(staffId);
		LOGGER.info("Change task staff; task id: " + taskId + "; staff id: "
				+ staff.getId() + "; staff email: " + staff.getEmail());
		try {
			taskService.setTaskStaff(taskId, staff);
		} catch (SQLException e) {
			LOGGER.error("Changing task staff");
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"������� �������, �� ������� ������ ���������");
			return "message";
		}
		return "redirect:/freeTasks";
	}
	@RequestMapping(path = "/search", method = RequestMethod.GET)
	public String searchTask(Model model) {
		return "search_task";
	}
	
	@RequestMapping(path = "/search", method = RequestMethod.POST)
	public String searchTask(Model model, HttpServletRequest request) {
		
		String query = request.getParameter("task_search");
		
		List<Task> tasks = taskSearcher.search(1, query);
		
		try {
			model.addAttribute("tasks", tasks);
			
		} catch(final Exception exc) {
			LOGGER.error("Searching tasks");
			model.addAttribute("title", "Oooops!");
			model.addAttribute("head", "500 Server Error");
			model.addAttribute("img", "/lms/resources/img/deny.png");
			model.addAttribute("description",
					"Error occured while searching for tasks");
			return "message";
		}
		
		return "search_task";
	}

}