package ukma.baratheons.lms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ukma.baratheons.lms.dao.RoomDao;
import ukma.baratheons.lms.dao.TaskDao;
import ukma.baratheons.lms.dao.UserDao;
import ukma.baratheons.lms.entity.Room;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class ReportController {

	@Autowired
	TaskDao jdbcTaskDao;

	@Autowired
	UserDao jdbcUserDao;

	@Autowired
	RoomDao jdbcRoomDao;

	List<User> productivityList;
	List<Task> taskList;
	List<Room> roomList;

	@RequestMapping(value = "reports/TaskList/{from}/{to}", method = RequestMethod.GET)
	public String getTask(Model model, @PathVariable String from, @PathVariable String to) throws ParseException {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dfrom = null;
        Date dto = null;
		if(!from.equals("null")) dfrom = format.parse(from);
		if(!to.equals("null")) dto = format.parse(to);
		taskList = jdbcTaskDao.getAllTasksCreatedInPeriod(dfrom, dto);
		model.addAttribute("taskList", taskList);
		return "TaskList";
	}

	@RequestMapping(path = "/reports", method = RequestMethod.GET)
	public String getReports() {
		return "reports";
	}

	@RequestMapping(path = "/reports", method = RequestMethod.POST)
	public String getReportsParam(Model model, HttpServletRequest request) throws IOException {
		Date from = null;
		Date to = null;
        if (!request.getParameter("calendar1").equals("")) from = java.sql.Date.valueOf(request.getParameter("calendar1"));
        if (!request.getParameter("calendar2").equals("")) to = java.sql.Date.valueOf(request.getParameter("calendar2"));
        long report = Long.valueOf(request.getParameter("report"));
        if(report == 1) return "redirect:reports/TaskList/"+from+"/"+to;
            else{
            if(report == 2) return "redirect:reports/ProductivityList/"+from+"/"+to;
                else{
                if(report == 3) return "redirect:reports/RoomList/"+from+"/"+to;
            }
        }
		return "reports";
	}


	@RequestMapping(value = "reports/TaskExport", method = RequestMethod.GET)
	public ModelAndView getExcel() {
		//List<Task> taskList = jdbcTaskDao.getAllTasks();
		return new ModelAndView("TaskListExcel", "taskList", taskList);
	}


	@RequestMapping(value = "reports/ProductivityList/{from}/{to}", method = RequestMethod.GET)
	public String getProductivity(Model model, @PathVariable String from, @PathVariable String to) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dfrom = null;
        Date dto = null;
        if(!from.equals("null")) dfrom = format.parse(from);
        if(!to.equals("null")) dto = format.parse(to);
		productivityList=jdbcUserDao.getStaffProductivityInPeriod(dfrom, dto);
		model.addAttribute("productivityList", productivityList);
		return "ProductivityList";
	}


	@RequestMapping(value = "reports/ProductivityListExport", method = RequestMethod.GET)
	public ModelAndView getExcelProductivity(){
		//List<User> productivityList=jdbcUserDao.getStaffProductivity();
		return new ModelAndView("ProductivityListExcel", "productivityList", productivityList);
	}


	@RequestMapping(value = "reports/RoomList/{from}/{to}", method = RequestMethod.GET)
	public String getRoom(Model model, @PathVariable String from, @PathVariable String to) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dfrom = null;
        Date dto = null;
        if(!from.equals("null")) dfrom = format.parse(from);
        if(!to.equals("null")) dto = format.parse(to);
		roomList=jdbcRoomDao.getAllRoomsInPeriod(dfrom, dto);
		model.addAttribute("roomList", roomList);
		return "RoomList";
	}


	@RequestMapping(value = "reports/RoomListExport", method = RequestMethod.GET)
	public ModelAndView getExcelRoom() {
		//roomList=jdbcRoomDao.getAllRooms();
		return new ModelAndView("RoomListExcel", "roomList", roomList);
	}

}