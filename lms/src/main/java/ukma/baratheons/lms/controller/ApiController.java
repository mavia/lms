package ukma.baratheons.lms.controller;

import java.util.ArrayList;
import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ukma.baratheons.lms.anorm.Operator;
import ukma.baratheons.lms.entity.Comment;
import ukma.baratheons.lms.entity.Equipment;
import ukma.baratheons.lms.entity.Room;
import ukma.baratheons.lms.entity.Task;
import ukma.baratheons.lms.entity.TaskType;
import ukma.baratheons.lms.entity.User;
import ukma.baratheons.lms.query.Table;
import ukma.baratheons.lms.service.*;

//Controller for AJAX requests
@RestController
@RequestMapping("/api")
public class ApiController {

	@Autowired
	private UserAdministrationService userService;

	@Autowired
	private EquipmentService eqptService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private EquipmentService equipmentService;

	@Autowired
	private RoomService roomService;

	@Autowired
	private TaskManagementService taskService;

	private static final Logger LOGGER = LogManager
			.getLogger(TaskController.class);
	
	/**
	 * 
	 * @return List of all Rooms
	 */
	public List<Room> getAllRooms() {

		return null;
	}

	@RequestMapping(path = "/staffproductivity")
	public List<User> getStaffProductiity() {
		List<User> list = userService.getStaffProductivityReport();
		return list;
	}

	/**
	 * 
	 * @param room
	 * @return List of Equipment belonging to room
	 */
	public List<Equipment> getEquipmentByRoom(Room room) {

		return null;
	}

	/**
	 * 
	 * @param room
	 * @return List of Tasks belonging to room
	 */
	public List<Task> getTasksByRoom(Room room) {

		return null;
	}

	/**
	 * 
	 * @param room
	 * @return List of Tasks belonging to User of type Staff Member
	 */
	public List<Task> getTasksByStaffMember(Room room) {

		return null;
	}

	/**
	 * 
	 * @param equipment
	 * @return List of Tasks attached to equipment with status Opened
	 */
	public List<Task> getOpenedTasksByEquipment(Equipment equipment) {

		return null;
	}

	/**
	 * 
	 * @return List of Tasks belonging to User of type Client
	 */
	public List<Task> getTasksByUser() {
		return null;
	}

	/**
	 * 
	 * @param type
	 * @return
	 */
	public List<Task> getTasksByUser(TaskType type) {
		return null;
	}

	// returns of all tasks which were attached to this task
	public List<Comment> getCommentsByTasks(Task task) {
		return null;
	}

	@RequestMapping(value = "/users_filtered", method = RequestMethod.POST)
	public List<User> getUsersFiltered(@RequestBody int filters,
			@RequestParam(value = "chief", required = false) String chief) {
		String chiefAdmin = null;

		if (chief != null) {
			chiefAdmin = "chief";
		}
		List<User> users = userService.getUsersFiltered(filters, chiefAdmin);
		return users;
	}

	@RequestMapping(value = "/listEquipment", method = RequestMethod.POST)
	public List<Equipment> getEquipmentByRoom(@RequestBody int roomId) {
		List<Equipment> equipments = eqptService.getEquipmentByRoom(roomId);
		return equipments;
	}

	@RequestMapping(value = "/listEquipmentEager", method = RequestMethod.POST)
	public List<Equipment> getEquipmentByRoomEager(@RequestBody int roomId) {
		List<Equipment> equipments = eqptService
				.getEquipmentEager(new ukma.baratheons.lms.anorm.Filter(
						Table.EquipmentTable.TABLE_NAME, Table.EquipmentTable.ROOM, Operator.EQ, roomId));
		return equipments;
	}

	@RequestMapping(value = "/listComment", method = RequestMethod.POST)
	public List<String> getCommentByEquipment(@RequestBody int eqptId) {
		List<Comment> comments = commentService.getCommentsByEqpt(eqptId);
		List<String> str = new ArrayList<String>();
		for (Comment c : comments)
			str.add(c.getDate().toString().substring(0, 16) + " - "
					+ c.getText());
		return str;
	}

	@RequestMapping(value = "/isRegistered/{email}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Boolean isValid() {
		return true;
	}

	@RequestMapping(value = "/saveEquipment", method = RequestMethod.POST)
	public Equipment saveEquipment(@RequestBody Equipment equipment) {
		System.out.println(equipment.getLocalNumber()+"#%&@%%^@^%^%");
		try {
			equipmentService.add(equipment);
		} catch (SQLException e) {
			LOGGER.error("Adding equipment");
			return null;
		}
		LOGGER.info("Added equipment id: " + equipment.getId());
		return equipment;
	}

	@RequestMapping(value = "/updateEquipment", method = RequestMethod.POST)
	public Equipment updateEquipment(@RequestBody Equipment equipment) {
		try {
			equipmentService.edit(equipment);
		} catch (SQLException e) {
			LOGGER.error("Updating equipment id: " + equipment.getId());
			return null;
		}
		LOGGER.info("Updated equipment id: "+equipment.getId());
		return equipment;
	}

	@RequestMapping(value = "/updateRoom", method = RequestMethod.POST)
	public Room updateRoom(@RequestBody Room room) {
		try {
			roomService.update(room);
		} catch (SQLException e) {
			LOGGER.error("Updating room id: " + room.getId());
			return null;
		}
		LOGGER.info("Updated room id: " + room.getId());
		return room;
	}

	@RequestMapping(value = "/addRoom", method = RequestMethod.POST)
	public Room saveRoom(@RequestBody Room room) {
		try {
			roomService.create(room);
		} catch (SQLException e) {
			LOGGER.error("Adding a room");
			return null;
		}
		LOGGER.info("Added room id: " + room.getId());
		return room;
	}

	@RequestMapping(value = "/deleteRoom", method = RequestMethod.POST)
	public boolean deleteRoom(@RequestBody Room room) {
		try {
			roomService.delete(room);
		} catch (SQLException e) {
			LOGGER.error("deleting room id: " + room.getId());
			return false;
		}
		LOGGER.info("Deleted room id: " + room.getId());
		return true;
	}
}
