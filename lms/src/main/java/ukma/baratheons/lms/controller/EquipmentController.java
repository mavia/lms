package ukma.baratheons.lms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ukma.baratheons.lms.entity.Equipment;
import ukma.baratheons.lms.entity.EquipmentType;
import ukma.baratheons.lms.entity.Room;
import ukma.baratheons.lms.service.EquipmentService;
import ukma.baratheons.lms.service.RoomService;

@Controller
@RequestMapping("/equipment")
public class EquipmentController {
	
	@Autowired
	RoomService roomService;
	
	@Autowired 
	EquipmentService equipmentService;
	
	@RequestMapping(path="/list")
	public String listEquipment(Model model){
		List<Room>rooms= roomService.getAllRooms();
		model.addAttribute("rooms", rooms);
		List<EquipmentType>eqptTypes = equipmentService.getEquipmentTypes();
		model.addAttribute("eqptTypes", eqptTypes);
		List<Equipment>equipment = equipmentService.getEquipmentEager(null);
		
		model.addAttribute("equipments", equipment);
		return "equipment";
	}	

}
