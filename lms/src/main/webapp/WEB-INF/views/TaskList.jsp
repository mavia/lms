<%@include file="header.jsp"%>
<body>

<span class="pull-left"> <a href="<c:url value="/reports" />"
							data-original-title="Back" data-toggle="tooltip"
							type="button" class="btn btn-sm btn-info"><font size="3"><i
		class="glyphicon glyphicon-arrow-left"></i> Back to report page</font></a>
</span></br>

<h2 align="center" style="clear:both;">Task List</h2>


	<h3><a href="../../TaskExport">Export</a></h3>

<div name="t1" style = "padding:20px;">
		<table id="TaskTable" class="table">
				<thead>
					<tr>
						<td>Id</td>
						<td>ParentId</td>
						<td>TaskStatusId</td>
						<td>TaskTypeId</td>
						<td>EquipmentId</td>
						<td>UserId</td>
						<td>StaffId</td>
						<td>CreationTime</td>
						<td>CompletionTime</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${taskList}" var="task">
						<tr>
							<td><c:out value="${task.id}" /></td>
							<td><c:out value="${task.parentId}" /></td>
							<td><c:out value="${task.taskStatus.name}" /></td>
							<td><c:out value="${task.taskType.name}" /></td>
							<td><c:out value="${task.eqpt.localNumber}" /></td>
							<td><c:out value="${task.user.firstName}" /></td>
							<td><c:out value="${task.staff.firstName}" /></td>
							<td><c:out value="${task.creationTime.toString().substring(0,16)}" /></td>
							<td><c:out value="${task.completionTime}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
</div>
</body>

<script>
	$('#TaskTable').DataTable();
</script>

<%@include file="footer.jsp"%>
