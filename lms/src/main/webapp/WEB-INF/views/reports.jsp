<%--suppress JSUnresolvedFunction --%>
<%--suppress JSUnresolvedFunction --%>
<%--suppress JSUnresolvedFunction --%>
<%--
  Created by IntelliJ IDEA.
  User: Oleksandr
  Date: 16.11.2015
  Time: 21:19
  To change this template use File | Settings | File Templates.
--%>

<%@include file="header.jsp"%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<html lang="en">
<script>
    function isVisibleCal(sel) {
        if(sel.value == 0)
            $('#calendars').hide();
        else if(sel.value == 1){
            var today=getCurrentDate();
            document.getElementById('cal1').max = today;
            document.getElementById('cal2').max = today;
            $('#calendars').show();
        }
    }

    function setMin(from){
        document.getElementById('cal2').min = from;
    }

    function setMax(to){
        if (to=='') document.getElementById('cal1').max = getCurrentDate(); else {
            document.getElementById('cal1').max = to;
        }
    }

    function formatDate(date){
        var dd = date.getDate();
        var mm = date.getMonth()+1;
        var yyyy = date.getFullYear();
        if(dd<10) {
            dd='0'+dd
        }
        if(mm<10) {
            mm='0'+mm
        }
        date = yyyy+'-'+mm+'-'+dd;
        return date;
    }

    function getCurrentDate(){
        var today = new Date();
        today = formatDate(today);
        return today;
    }

</script>

<header id="top" class="header">
    <div class="text-vertical-center">
        <h1>Choose report, please!</h1>
        <div class="container" style="max-width: 300px;">
        <form name = "createReport" action="reports" method='POST'>
            <br/>
            <div>
                <select name="report" class="form-control">
                    <option value="0">Choose report</option>
                    <option value="1">Created tasks</option>
                    <option value="2">Staff efficiency</option>
                    <option value="3">Problems on the rooms</option>
                </select>

                <select name="type" id = "type" class="form-control" onchange="isVisibleCal(this)">
                    <option value="0" selected >All time</option>
                    <option value="1">Some period</option>
                </select>
            </div>
            <br/>
            <div id="calendars" hidden="hidden">
                <p><h4>Report from: <input type="date" name="calendar1" id="cal1" class="form-control" onchange="setMin(this.value)">
                    to: <input type="date" name="calendar2" id="cal2" class="form-control" onchange="setMax(this.value)"></h4></p>
            </div>
            <br/>
            <div id="report-button">
            <p><input type="submit" value="Report" class="btn btn-dark btn-lg"></p>
            </div>
        </form>
    </div>
    </div>
</header>
<%@include file="footer.jsp"%>