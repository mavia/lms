<%@include file="header.jsp"%>
    
    <!-- Top of page -->
    <header id="top" class="header">
        <div class="text-vertical-center">
            <h1> LMS </h1>
            <h3>Here you can change your account information</h3>
            <br />
            <a href="#account" class="btn btn-dark btn-lg">Account Settings</a>
        </div>
    </header>
    
    <hr />

<!-- Account Settings -->
<section id="account">
	<br /> <br /> <br /> <br /> <br />
	<div class="container">
	
		<div class="panel panel-info">

			<div class="panel-heading">
				<label>${user.firstName} ${user.lastName}</label>
				<span class="pull-right"> <a href="<c:url value="/profile/edit/id/${user.id}" />"
					data-original-title="Edit" data-toggle="tooltip"
					type="button" class="btn btn-sm btn-warning"><i
						class="glyphicon glyphicon-edit"></i></a>
				</span>
			</div>

				<div class="panel-body">
					<div>
						<table style="width:100%" class="table table-user-information">
							<tbody>
								<tr>
									<td style="text-align: left">Email:</td>
									<td style="text-align: left">${user.email}</td>
								</tr>
								<tr>
									<td style="text-align: left">Last online:</td>
									<td style="text-align: left">${user.lastOnline}</td>
								</tr>
								<tr>
									<td style="text-align: left">Registration date:</td>
									<td style="text-align: left">${user.regDate}</td>
								</tr>
								<tr>
									<td style="text-align: left">Role:</td>
									<td style="text-align: left">${user.role.name}</td>
								</tr>
								<tr>
									<td style="text-align: left">Active account:</td>
									<td style="text-align: left">${user.active}</td>
								</tr>
								<tr>
									<td style="text-align: left">Your ban date:</td>
									<c:if test="${empty notBan}">
										<td style="text-align: left">${user.banEndTime}</td>
									</c:if>
									<c:if test="${not empty notBan}">
										<td style="text-align: left">${notBan}</td>
									</c:if>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

		</div>
</div>
</section>

<%@include file="footer.jsp"%>