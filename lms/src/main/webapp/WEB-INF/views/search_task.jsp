<%@include file="header.jsp"%>

<c:if test="${not empty createTask}">
	<script>
		$(window).load(function() {
			$("#modalCreateTask").modal('show');
		});
	</script>
</c:if>

<!-- Top of page -->
<header id="top" class="header">
	<div class="text-vertical-center">
		<h1>LMS</h1>
		<h3>Here you can search tasks.</h3>
		<br /> <a href="#search" class="btn btn-dark btn-lg"> Search </a>
	</div>
</header>

<hr />
<section id="search" style="min-height:500px;">
	<div class="container">
		
<div style="width:200px;margin:auto;" align="center">
<form class="form-reset" id="formSearch"
								action="<c:url value="/search" />" method='POST' >
								<h3 class="form-signin-heading">Task search:</h3>
								<input type="text" name="task_search" class="form-control"
									maxlength="320" placeholder="" required autofocus>
								<br> 
								<div id="loadSearch"></div>	
								<button class="btn btn-lg btn-primary btn-block" name="Submit"
									value="Search" id="submitSearch" type="submit" style="padding: 2px 2px 2px 2px;padding-right: 0px;
height: 35px;display: inline-block;width: 80px;">Search</button>
							</form>
							</div>
		<c:if test="${not empty tasks}">

			<table id="tasksTable" class="table" >
				<thead>
					<tr>
						<th>Task id</th>
						<th>Creator</th>
						<th>Staff</th>
						<th>Creation time</th>
						<th>Completion time</th>
						<th>Problem type</th>
						<th>Status</th>
						<th>Equipment</th>
						<th>Local number</th>
						<th>Room number</th>

					</tr>
</thead>
<tbody>
					<c:forEach items="${tasks}" var="task">
						<tr>
							<td><c:out value="${task.id}" /></td>
							<td><c:out value="${task.user.email}" /></td>
							<td><c:out value="${task.staff.email}" /></td>
							<td><c:out value="${task.creationTime}" /></td>
							<td><c:out value="${task.completionTime}" /></td>
							<td><c:out value="${task.taskType.name}" /></td>
							<td><c:out value="${task.taskStatus.name}" /></td>
							<td><c:out value="${task.eqpt.id}" /></td>
							<td><c:out value="${task.eqpt.localNumber}" /></td>
							<td><c:out value="${task.eqpt.room.number}" /></td>

						</tr>
					</c:forEach>
				</tbody>
			</table>


		</c:if>
	</div>
</section>
<script>
	$('#tasksTable').DataTable();
</script>
<%@include file="footer.jsp"%>