<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <title>LMS</title>
    <!-- Shortcut -->
    <link href="resources/img/shortcut.ico" rel="shortcut icon">
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!-- CSS -->
    <link href="resources/css/styles.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 align="center">Welcome To Your Personal Page For Registration</h1>
                <hr size="5" />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <p>Here will be short user manual</p>
            </div>
            <div class="col-lg-4">
              <form role="form" action="" method='POST'>

              <label>First name:</label>
              <input type="text" class="form-control" name="firstName" required>

              <label>Last name:</label>
              <input type="text" class="form-control"  name="lastName"  required>

              <label>Password:</label>
              <input type="password" class="form-control" name="password" id="password1" required/>

              <label>Repeat password:</label>
              <input type="password" class="form-control" name="repeat-password" id="password2" required/>
              <br>
              <input type="submit" class="btn btn-primary" value="Create Account">
          </form>
      </div>

  </div>
</div>
<script type="text/javascript">
    window.onload = function () {
        document.getElementById("password1").onchange = validatePassword;
        document.getElementById("password2").onchange = validatePassword;
    }
    function validatePassword(){
        var pass2=document.getElementById("password2").value;
        var pass1=document.getElementById("password1").value;
        if(pass1!=pass2)
            document.getElementById("password2").setCustomValidity("Passwords Don't Match. Please, check then carefully.");
        else
            document.getElementById("password2").setCustomValidity('');//empty string means no validation error  
    }
</script>
</body>
</html>