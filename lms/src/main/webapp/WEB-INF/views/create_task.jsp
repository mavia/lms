<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<html lang="en">

<head>

	<title>LMS</title>
    
	<!-- Shortcut -->
	<link href="<c:url value="/resources/img/shortcut.ico" />" rel="shortcut icon">
	
	<!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    
    <!-- CSS -->
    <link href="<c:url value="/resources/css/styles.css" />" rel="stylesheet">
    
</head>

<body>
<span class="pull-left"> <a href="<c:url value="/tasks" />"
					data-original-title="Back" data-toggle="tooltip"
					type="button" class="btn btn-sm btn-info"><font size="3"><i
						class="glyphicon glyphicon-arrow-left"></i> Back to task page</font></a>
</span>
	<!-- Create task -->
        	<div class="container">
            	<div class="row">
                	<div class="col-lg-12 text-center">
                		<h1>Create task</h1>
                	</div>
            	</div>
        	</div>
        	<hr size="5" />
        	
        	<div class="container" style="max-width: 500px;">
			<div style="text-align: center;">
			
				<form class="form-horizontal" role="form" name='taskform'
					action="createTask" method='POST' id="formCreateTask">
					<div class="form-group" id="roomAndEqpt" style="max-width: 300px;margin: auto;">

						<select name="room" class="form-control" id="rooms" required>
							<option value='0'>Choose room</option>
							<c:forEach items="${rooms}" var="room">
								<option value="${room.id}">${room.number}</option>
							</c:forEach>
						</select> <select name="eqpt" class="form-control" id="eqpt" required>
							<option value=''>First select a room</option>
						</select> <select name="typeTask" class="form-control" id="typeTask"
							required>
							<option value=''>Choose type</option>
							<c:forEach items="${typesTask}" var="typeTask">
								<option value="${typeTask.id}">${typeTask.name}</option>
							</c:forEach>
						</select>
					</div>

					<br />
				
					<div  id="commentField">
						<p>We already know about some problems with this equipment:</p>
						<ul  id="comments" type="none"  style="height:200px; width:500px; overflow-y:auto;margin: auto;">
						</ul>
						<p>If you find new problem with this equipment, please report:</p>
					</div>
					<br />

					<div class="form-group" id="comment" style="max-width: 300px;margin: auto;">
						
						<textarea rows="3" class="form-control" name="postalAddress"
							id="postalAddress" placeholder="Add description" required></textarea>
							<input type="hidden" name="create_task_sec" value="${create_task_sec}">
							<div class="alert alert-danger" role="alert" id="alertCreateTask">
									<span class="glyphicon"
										aria-hidden="true" id="iconCreateTask"></span> <span class="sr-only" >Error:</span>
									<span id="resultCreateTask"></span>
								</div>
							<div id="loadCreateTask"></div>	
						<input type="submit" class="btn btn-primary" value="Submit">
						<input type="reset" class="btn btn-default" value="Clear all">
					</div>
				</form>
			</div>
		</div>

<script>
var loader="<img src='/lms/resources/img/482.GIF'>";
	$(document).ready(function() {
		$('#commentField').hide();
	});
	// Update equipment list
	var formCreateTask=$('#formCreateTask');
var iconCreateTask = $('#iconCreateTask');
var alertCreateTask = $('#alertCreateTask');
$('#formCreateTask').submit(function(){
	$('#loadCreateTask').html(loader);
	$.ajax({
          type: "POST",
          url: "/lms/createTask",
          data: formCreateTask.serialize(),
          success: function(data) {
        	  if (data.status=="ok"){
        		  iconCreateTask.attr("class","glyphicon glyphicon-ok-sign"); 
        		  alertCreateTask.attr("class","alert alert-success");
        	  }else{
        		  iconCreateTask.attr("class","glyphicon glyphicon-exclamation-sign");
        		  alertCreateTask.attr("class","alert alert-danger");
        	  }
        	  alertCreateTask.show();
        	  $('#loadCreateTask').html("");
        	  $('#resultCreateTask').html(data.message);
        	  formCreateTask.trigger( 'reset' );
        	  setTimeout("alertCreateTask.hide()",5000);
          },
          
        });
	
	return false;
});
	$(document).ready(function() {
		$('#commentField').hide();
		$('#alertCreateTask').hide(); 
		
	});
	// Update equipment list
	$(document)
			.ready(
					
					function() {

						var resetPassword = $('#resetPassword');
						resetPassword.click(function(){
							$("#modalResetPass").modal('show');
							alertResetPass.hide(); 
							
						});
						
						var room = $('#rooms');
						room.change(function() {
									var currentRoom = 0;
									if (room.val() != 0) {
										currentRoom = parseInt(room.val());
									}
									$('#loadCreateTask').html(loader);
									$.ajax({
												type : "POST",
												url : '/lms/api/listEquipment',
												data : JSON
														.stringify(currentRoom),
												contentType : "application/json; charset=utf-8",
												dataType : "json",
												success : function(equipments) {
													$('#loadCreateTask').html("");
													$('#commentField').hide();
													$('#eqpt option').remove();
													$('#eqpt').append("<option value=''>Choose equipment</option>");
													$.each(equipments,function(index,equipment) {
																		$('#eqpt').append("<option value='" + equipment.id + "'>"
																								+ equipment.localNumber
																								+ "</option>");
																	});
												}
											});
								});
					});

	$(document).ready(
			function() {
				var eqpt = $('#eqpt');
				eqpt.change(function() {
					var currentEqpt = 0;
					if (eqpt.val() != 0) {
						currentEqpt = parseInt(eqpt.val());
					}
					$('#loadCreateTask').html(loader);
					$.ajax({
						type : "POST",
						url : '/lms/api/listComment',
						data : JSON.stringify(currentEqpt),
						contentType : "application/json; charset=utf-8",
						dataType : "json",
						success : function(comments) {
							$('#loadCreateTask').html("");
							$('#commentField').hide();
							$('#comments').empty();
							if (comments.length > 0) {
								$('#commentField').show();
							}
							$.each(comments, function(index, comment) {
								$('#comments').append(
										"<li><div class=\"alert alert-info\">"
												+ comment + "</div></li>");
							});
						},
						error : function() {
							$('#loadCreateTask').html("");
							$('#comments').empty();
							$('#commentField').hide();
						}
					});
				});
			});
</script>
<%@include file="footer.jsp"%>