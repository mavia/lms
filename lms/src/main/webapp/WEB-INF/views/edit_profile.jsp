﻿<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>LMS</title>

<!-- Shortcut -->
<link href="<c:url value="/resources/img/shortcut.ico" />"
	rel="shortcut icon">

<!-- Bootstrap Core CSS -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Bootstrap Core JavaScript -->
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- jQuery -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- CSS -->
<link href="<c:url value="/resources/css/styles.css" />"
	rel="stylesheet">
</head>
<body>

	<c:if test="${empty admin}">
		<span class="pull-left"> <a href="<c:url value="/profile" />"
			data-original-title="Back" data-toggle="tooltip" type="button"
			class="btn btn-sm btn-info"><font size="3"><i
					class="glyphicon glyphicon-arrow-left"></i> Back to profile page</font></a>
		</span>
	</c:if>

	<c:if test="${not empty admin}">
		<span class="pull-left"> <a href="<c:url value="/userAdmin" />"
			data-original-title="Back" data-toggle="tooltip" type="button"
			class="btn btn-sm btn-info"><font size="3"><i
					class="glyphicon glyphicon-arrow-left"></i> Back to UserAdmin page</font></a>
		</span>
	</c:if>

	<div class="container">
		<h1 align="center">Edit Profile</h1>
		<hr size="5" />

		<div class="row">
			<h3 align="center">Personal info</h3>

			<c:if test="${empty admin}">
				<p align="center">Please, change your information or return to
					previous page.</p>
				<p align="center">You need to confirm changes by indicating
					password. Also you can indicate new password.</p>
				<c:url var="saveUrl" value="/profile/edit/id/${user.id}" />
			</c:if>

			<c:if test="${not empty admin}">
				<p align="center">Please, change user information or return to
					previous page.</p>
				<c:url var="saveUrl" value="/profile/edit/id/${user.id}?adminEdit" />
			</c:if>

			<form:form class="form-horizontal" onsubmit="return checkForm();"
				acceptCharset="utf-8" modelAttribute="user" action="${saveUrl}"
				method='POST'>
				<div align="center">
					<br />

					<form:input path="lastOnline" style="width: 50%;" type="hidden"
						class="form-control" required="true" />
					<form:input path="penaltyScore" style="width: 50%;" type="hidden"
						class="form-control" required="true" />
					<form:input path="rights" style="width: 50%;" type="hidden"
							class="form-control" required="true" />	
					<c:if test="${empty admin}">
						<label for="activeA" style="display: none">Active/Inactive?
						</label>
						<form:checkbox path="active" style="display:none"
							class="form-control" id="activeA" />
						
						<form:input path="email" style="width: 50%;" type="hidden"
							class="form-control" required="true" />
					</c:if>
					
					<form:input path="role.id" style="width: 50%;" type="hidden"
						class="form-control" required="true" />

					<form:label path="firstName">First name:</form:label>
					<div>
						<form:input path="firstName" style="width: 50%;" type="text"
							class="form-control" maxlength="32" required="true" />
					</div>
					<br />
					<form:label path="lastName">Last name:</form:label>
					<div>
						<form:input path="lastName" style="width: 50%;" type="text"
							class="form-control" maxlength="32" required="true" />
					</div>
					<br />
					
					<c:if test="${empty admin}">

						<c:if test="${empty notMatch}">
							<form:label path="password">Old Password:</form:label>
							<div id="divO">
								<form:input path="password" id="oldPass" style="width: 50%;"
									type="password" class="form-control" value="" />
							</div>
						</c:if>

						<c:if test="${not empty notMatch}">
							<form:label path="password">Old Password:</form:label>
							<div id="divO" class="form-group has-error has-feedback">
								<form:input path="password" id="oldPass" style="width: 50%;"
									type="password" class="form-control" value="" />
								<span class="glyphicon glyphicon-remove form-control-feedback"
									style="width: 50%;"></span>
								<p style="color: red;">${notMatch}</p>
							</div>
						</c:if>
						<br />
						<label>New Password:</label>
						<div>
							<input type="password" style="width: 50%;" class="form-control"
								id="pass" name="pass" />
						</div>
						<br />
						<label>Confirm Password:</label>
						<div id="divP">
							<input type="password" style="width: 50%;" class="form-control"
								id="confirmPassword" name="confirmPassword" />
						</div>
					</c:if>
					
					<br /> <label for="receive">Receive emails? </label>
					<form:checkbox path="receiveEmail" id="receive" />
					<br />
					<br />
					
					<c:if test="${not empty admin}">
						<form:input path="password" style="width: 50%;" type="hidden"
							class="form-control" required="true" />
						<form:label path="email">Email:</form:label>
						<c:if test="${empty emailError}">
							<div>
								<form:input path="email" style="width: 50%;" type="email"
									class="form-control" maxlength="320" required="true" />
							</div>
						</c:if>
						<c:if test="${not empty emailError}">
							<div class="form-group has-warning has-feedback">
								<p style="color: red;">${emailError}</p>
								<form:input path="email" style="width: 50%;" type="email"
									class="form-control" maxlength="320" required="true" />
							</div>
						</c:if>
						<br />
	
						<br />
						<label for="activeA">Active/Inactive? </label>
						<form:checkbox path="active" id="activeA" />
					</c:if>
					
					<br />
					<br /> <input type="submit" class="btn btn-primary"
						value="Save Changes">
				</div>
			</form:form>
		</div>
	</div>

	<script>
		function checkForm() {

			if (document.getElementById('oldPass').value == "") {
				var para = document.createElement("p");
				var message = document
						.createTextNode("You need to confirm changes by indicating your old password");
				para.setAttribute("style", "color: red");
				para.appendChild(message);

				$('#divO p').remove();

				var element = document.getElementById("divO");
				element.appendChild(para);
				return false;
			}

			if (document.getElementById('pass').value != ""
					&& document.getElementById('pass').value != document
							.getElementById('confirmPassword').value) {

				var span = document.createElement("span");
				span.setAttribute("class",
						"glyphicon glyphicon-remove form-control-feedback");
				span.setAttribute("style", "width: 50%;");
				var para = document.createElement("p");
				var message = document
						.createTextNode("The password and its confirm are not the same");
				para.setAttribute("style", "color: red");
				para.appendChild(message);

				$('#divP p').remove();

				var element = document.getElementById("divP");
				element.setAttribute("class",
						"form-group has-error has-feedback");
				element.appendChild(span);
				element.appendChild(para);
				return false;
			}
			return true;
		}
	</script>

</body>
</html>