<%@include file="header.jsp"%>

<!-- Top of page -->
    <header id="top" class="header">
        <div class="text-vertical-center">
            <h1> LMS </h1>
            <h3>Here you can view, add and edit equipments.</h3>
            <br /> 
            <a href="#list_equipments" class="btn btn-dark btn-lg">
            View equipments
            </a>
        </div>
    </header>
    
    <hr />

<section id="list_equipments">
<div class="container" style="max-width:300px;" align="center">
  <h1 align="center">Equipment list</h1>
  <hr size="5"/>
  <div class="form-group">
    <label for="room">Select room</label>
    <select class="form-control" id="room">
	<option value="">All</option>
      <option value="n">Not Attached</option>
      <c:forEach items="${rooms}" var="room">
        <option value="${room.id}">${room.number}</option>
      </c:forEach>
    </select>
  </div>
</div>
</section>

<div class="container" align="right" style="max-width:80%;">
  <button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="addEqpm()">Add equipment</button>
</div>

<div style="padding: 20px 20px 20px 20px">
  <div class="container">
    <table class="table table-hover" id = "eqTable">
      <thead id = "tableHead">
        <tr>
			<td>Id</td>
			<td>Room</td>
	        <td>Local Number</td>
	        <td>Type</td>
	        <td>Decommissioned</td>
	        <td data-orderable="false">Edit</td>
			<td>Room Id</td>
        </tr>
      </thead>
      <tbody>
      	<c:forEach items="${equipments}" var="equipment">
      		<tr>
      			<td>${equipment.id}</td>
      			<td>${equipment.room.number}</td>
      			<td>${equipment.localNumber}</td>
      			<td>${equipment.eqptType.name}</td>
      			<c:if test="${equipment.decommissioned==true}">
      				<td><img src="<c:url value="/resources/img/no.png" />" height="25" width="25"></td>
      			</c:if>
      			<c:if test="${equipment.decommissioned!=true}">
      				<td><img src="<c:url value="/resources/img/yes.png" />" height="25" width="25"></td>
      			</c:if>
      			<td><button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="edit(${equipment.localNumber}, ${equipment.id}, ${equipment.eqptType.id}, ${equipment.room.id}, ${equipment.decommissioned})">Edit</button></td>
      			<td>${equipment.room.id}</td>
      		</tr>
      	</c:forEach>
      </tbody>
    </table>
	<input type="text" id="row" style="display: none">
  </div>
</div>

<!--Modal window for the equipments' editing and adding-->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="editWindow" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
<form method="post" action="#" id="myform">
  <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center" id="label">Edit equipment</h4>
      </div>

      <div class="modal-body">
        <div class="form-group">
          <label for="mNum">Local number:</label>
		  <input type="text" class="form-control" id="mId" style="display: none">
          <input type="text" class="form-control" id="mNum" maxlength="10" required>
        </div>
        <div class="form-group">
          <label for="mType">Type:</label>
          <select class="form-control" id="mType" required>
			<c:forEach items="${eqptTypes}" var="type">
              <option value="${type.id}">${type.name}</option>
            </c:forEach>
          </select>
        </div>
        <div class="form-group">
          <label for="mRoom">Room:</label>
          <select class="form-control" id='mRoom' required>
			<option value="-">Not Attached</option>
            <c:forEach items="${rooms}" var="room">
              <option value="${room.id}">${room.number}</option>
            </c:forEach>
          </select>
        </div>
        <div class="form-group" id="decomChBox">
          <label for="mDecom">isDecommissioned:</label>
          <input type="checkbox" id="mDecom">
        </div>
      </div>

      <div class="modal-footer">
        <div class="form-group">
          <button type="submit" class="btn btn-primary" id="save" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  </div>
  </div>
</div>
</form>    
    </div>
		</div>

<!--Modal window (error)-->
<div class="modal fade error" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="error" aria-hidden="true">
	<div class="modal-dialog modal-sm">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        		<h4 class="modal-title">Error</h4>
      		</div>
      		<div class="modal-body">
       			<p id="errMessage">error</p>
      		</div>
      		<div class="modal-footer">
         		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
 	</div>
</div>

<script>
//cleaning edit window fields
	function addEqpm(){
		$('#label').text("Add equipment"); 
		document.getElementById('mId').value = "";
		document.getElementById('mNum').value = "";
		document.getElementById('mType').value = "";
		document.getElementById('mRoom').value = "";
		document.getElementById('mDecom').checked = false;
		$('#decomChBox').hide();
}
	//ajax call to edit or create a new room
	$('#myform').on('submit',function(e){
		e.preventDefault();
		$('#editWindow').modal('hide');
		var id = document.getElementById('mId').value;
		var localNum = document.getElementById("mNum").value;
		var roomId = document.getElementById("mRoom").value;
		var room=null;
		var decom = document.getElementById("mDecom").checked;
		room = {id: roomId, number: $('#mRoom :selected').text()};
		var typeId = document.getElementById("mType").value;
		var typeName = $('#mType :selected').text();
		var type = {id: typeId, name: typeName};
		var eqpt = {id: id, localNumber: localNum, decommissioned: decom, eqptType: type, room: room};
		var address = '/lms/api/updateEquipment';
		if(id=="")address = '/lms/api/saveEquipment';
		$.ajax({
		  type: "POST",
		  url: address,
		  data: JSON.stringify(eqpt),
		  contentType: "application/json; charset=utf-8",
			success: function (equipment) {
				if(equipment!=null){
					var room;
					var order = "";
					if(equipment.decommissioned) order = no;
						if(equipment.room!=null)room = equipment.room;
						else room = {id: "n", number: ""};
					if(document.getElementById(equipment.id)==null)	{
						var rowIndex = $('#eqTable').dataTable().fnAddData([equipment.id, room.number, equipment.localNumber, equipment.eqptType.name, order, '<input class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" type="button" onclick="edit(\'' + equipment.localNumber + '\', \'' + equipment.id + '\', \'' + equipment.eqptType.id + '\', \'' + room.id + '\', \'' + equipment.decommissioned + '\')" value="Edit"/>',  room.id]);
						var row = $('#eqTable').dataTable().fnGetNodes(rowIndex);
						$(row).attr('id', equipment.id);
					}
					else $('#eqTable').DataTable().row(document.getElementById(equipment.id)).data([equipment.id, room.number, equipment.localNumber, equipment.eqptType.name, order, '<input class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" type="button" onclick="edit(\'' + equipment.localNumber + '\', \'' + equipment.id + '\', \'' + equipment.eqptType.id + '\', \'' + room.id + '\', \'' + equipment.decommissioned + '\')" value="Edit"/>', room.id]).draw();
				}else {
					$('#errMessage').text("Changes could not have been saved. Please try again.");
					$('#editWindow').modal('show');
					$('#error').modal('show');
				}
          },
			error: function(xhr, status, error) {
					$('#errMessage').text("Something went wrong, the changes were not saved. Please try again.");
					$('#error').modal('show');
			}
		});
	});
	//passing equipment info to edit window
  function edit(localNum, id, typeId, roomId, decommissioned) {
	$('#label').text("Edit equipment");  
	document.getElementById('mId').value = id;
    document.getElementById('mNum').value = localNum;
	document.getElementById('mType').value = typeId;
	document.getElementById('mRoom').value = roomId;
	if(decommissioned == 'true')document.getElementById('mDecom').checked = true;
	else document.getElementById('mDecom').checked = false;
	$('#decomChBox').show();
  }
    
  //filtering equipment by room
  $('#room').change(function (){
	  $('#eqTable').DataTable().column(6).search("\\b"+$('#room').val()+"\\b", true, true).draw();
  });
  function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
</script>

<%@include file="footer.jsp"%>