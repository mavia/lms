<%@include file="header.jsp"%>

<!-- Top of page -->
    <header id="top" class="header">
        <div class="text-vertical-center">
            <h1> LMS </h1>
            <h3>Here you can view all users and edit their accounts.</h3>
            <br /> 
            <a href="#list_users" class="btn btn-dark btn-lg">
            View users
            </a>
        </div>
    </header>
    
    <hr />
    
    <c:if test="${not empty chief}">
    	<label id="admin"  style="display:none">chiefAdmin</label>
    </c:if>
    <c:if test="${empty chief}">
    	<label id="admin"  style="display:none">admin</label>
    </c:if>

<section id="list_users">
<div id="ua-div">
	<table style="width:100%;">
		<tr>
			<td style="text-align:left;">
				<button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">
					Add users
				</button>
			</td>
			<td style="text-align:right; width:25%;">
				<select id='roleFilter' class="form-control">
					<option value='0'>All roles</option>
					<c:forEach items="${roles}" var="role">
						<option value="${role.id}">${role.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
	</table>
	<hr>
	<div>
		<p><h2>Users:</h2></p>
		<table id="usersTable" class="table">
			<thead>
				<tr>
					<td>Id</td>
					<td>First Name</td>
					<td>Last Name</td>
					<td>Email</td>
					<td>Role</td>
					<td>Penalty Score</td>
					<td>Ban Time</td>
					<td data-orderable="false">Active</td>
					<td data-orderable="false"></td>
					<td data-orderable="false"></td>
					<td data-orderable="false"></td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${users}" var="user">
					<tr>
						<td>${user.id}</td>
						<td>${user.firstName}</td>
						<td>${user.lastName}</td>
						<td>${user.email}</td>
						<td>${user.role.name}</td>
						<td>${user.penaltyScore}</td>
						<c:if test="${empty user.banEndTime}">
						<td>Not Banned</td>
						</c:if>
						<c:if test="${not empty user.banEndTime}">
							<td>${user.banEndTime}</td>
						</c:if>
						<td>
						<c:choose>
							<c:when test="${user.active}">
   								<img src="<c:url value="/resources/img/yes.png" />" height="25" width="25">
   							</c:when>
   							<c:otherwise>
    							<img src="<c:url value="/resources/img/no.png" />" height="25" width="25">
    						</c:otherwise>
						</c:choose>
						</td>
						<td><a href="<c:url value="/profile/edit/id/${user.id}?adminEdit" />"
							data-original-title="Edit" data-toggle="tooltip"
							type="button" class="btn btn-sm btn-warning">Edit
							<i class="glyphicon glyphicon-edit"></i>
							</a>
						</td>
						<td><button onclick="ban(${user.id})" data-toggle="modal"  data-target=".bs-example-modal-lg"
									data-original-title="Edit" class="btn btn-sm btn-danger">Ban
							<i class="glyphicon glyphicon-remove"></i>
							</button>
						</td>
						<td><c:if test="${not empty user.banEndTime}">
						<a href="<c:url value="/profile/edit/unbanUser/${user.id}?adminEdit" />"
							   data-original-title="Edit" data-toggle="tooltip"
							   type="button" class="btn btn-sm btn-success">Unban
							<i class="glyphicon glyphicon-ok-circle"></i>
						</a>
						</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
</section>

<!--Modal window for adding a new users-->		
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-sm">
    	<div class="modal-content">
    		<div id="modal-style">
                <form method="post" action="inviteUser">
          			<div class="form-group">
          				<select class="form-control" name="role" required>
            				<option value=''>Choose Role</option>
            				<c:if test="${not empty chief}">
    							<c:forEach items="${roles}" var="role">
    								<option value="${role.id}">${role.name}</option>
    							</c:forEach>
    						</c:if>
    						<c:if test="${empty chief}">
    							<c:forEach items="${roles}" var="role">
    								<c:if test="${role.name != 'admin'}">
    									<option value="${role.id}">${role.name}</option>
    								</c:if>
    							</c:forEach>
    						</c:if>
          				</select>	
          			</div>
          			<div class="form-group">
          				<!--  <input type="email" class="form-control" name="email" placeholder="Email" required>-->
                        <textarea class="form-control" placeholder="Please, input each email in new line" name="emails" id="emails" required></textarea>
          			</div>
          			<div class="form-group" style="text-align:right;">
          				<input type="submit" class="btn btn-primary" value="Submit">
          			</div>
                </form>
    		</div>
    	</div>
  	</div>
</div>


<!--Modal window for banning users-->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel1" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div id="modal-style1">
				<form method="post" action="banUser">
					<div class="form-group">
						<input type="text" class="form-control" style="display: none" name="userId" id="userId">
						<select class="form-control" name="time" required>
							<option value=''>Choose Ban Time</option>
							<option value='hour'>hour</option>
							<option value='day'>day</option>
							<option value='week'>week</option>
							<option value='month'>month</option>
							<option value='year'>year</option>
						</select>
					</div>
					<div class="form-group" style="text-align:right;">
						<input type="submit" class="btn btn-primary" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>




<script>



	$('#usersTable').DataTable();

	function ban (user) {
		document.getElementById("userId").value = user;
			}

    var roleFilter = $('#roleFilter');
    var chiefAdmin = document.getElementById("admin").innerHTML;
	roleFilter.change(function() {
		var filter = parseInt(roleFilter.val());
		var urlFilter = '/lms/api/users_filtered';
		if(chiefAdmin == "chiefAdmin"){
			urlFilter = '/lms/api/users_filtered?chief';
		}
		$.ajax({
		    type: "POST",
		    url: urlFilter,
		    data: JSON.stringify(filter),
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
		    success: function(users){
		    	$('#usersTable tbody tr').remove();
				$.each(users, function(index, user) {
					
					var banTime = 'Not Banned';
					
					if(user.banEndTime != null){
						banTime = new Date(user.banEndTime).toUTCString();
					}
					
					var unbanUser = '<td></td>'
					
					if(banTime != 'Not Banned'){
						unbanUser = '<td><a href="<c:url value="/profile/edit/unbanUser/' + user.id + '?adminEdit" />"data-original-title="Edit" data-toggle="tooltip" type="button" class="btn btn-sm btn-success">Unban<i class="glyphicon glyphicon-ok-circle"></i></a></td>';
					}
					
					if (user.active) {	
						$('#usersTable').append(
								'<tr><td>' + user.id + 
								'</td><td>' + user.firstName + 
								'</td><td>' + user.lastName + 
								'</td><td>' + user.email + 
								'</td><td>' + user.role.name +
								'</td><td>' + user.penaltyScore +
								'</td><td>' + banTime +
								'</td><td><img src="<c:url value="/resources/img/yes.png" />" height="25" width="25"></td><td><a href="<c:url value="/profile/edit/id/' + user.id + '?adminEdit" />"data-original-title="Edit" data-toggle="tooltip"type="button" class="btn btn-sm btn-warning">Edit   <i class="glyphicon glyphicon-edit"></i></a></td><td><button onclick="ban('+user.id+')" data-original-title="Edit" data-toggle="modal"  data-target=".bs-example-modal-lg" class="btn btn-sm btn-danger">Ban   <i class="glyphicon glyphicon-remove"></i></button></td></td>'+unbanUser+'</tr>');
					}
					else {
						$('#usersTable').append(
								'<tr><td>' + user.id + 
								'</td><td>' + user.firstName + 
								'</td><td>' + user.lastName + 
								'</td><td>' + user.email + 
								'</td><td>' + user.role.name + 
								'</td><td>' + user.penaltyScore +
								'</td><td>' + banTime +
								'</td><td><img src="<c:url value="/resources/img/no.png" />" height="25" width="25"></td><td><a href="<c:url value="/profile/edit/id/' + user.id + '?adminEdit" />"data-original-title="Edit" data-toggle="tooltip"type="button" class="btn btn-sm btn-warning">Edit   <i class="glyphicon glyphicon-edit"></i></a></td><td><button onclick="ban('+user.id+')" data-original-title="Edit" data-toggle="modal"  data-target=".bs-example-modal-lg" class="btn btn-sm btn-danger">Ban   <i class="glyphicon glyphicon-remove"></i></button></td></td>'+unbanUser+'</tr>');
					}
				})
			}
		});
	});

     window.onload = function () {
        document.getElementById("emails").onchange = validateEmails;
    }

    function validateEmails(){
        var emails = document.getElementById("emails").value;
        var emailsArray = emails.split("\n");
        document.getElementById("emails").setCustomValidity("");
        for(var i=0; i < emailsArray.length; i++)
            if (!validateEmail(emailsArray[i])) document.getElementById("emails").setCustomValidity("Please, input each email in new line and make sure that emails are correct.");
    }
    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
</script>
<%@include file="footer.jsp"%>