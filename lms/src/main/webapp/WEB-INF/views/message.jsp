<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="false"%>
<html>
<head>
  <title>${title}</title>

  <!-- Bootstrap Core CSS -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <!-- CSS -->
  <link href="<c:url value="/resources/css/styles.css" />" rel="stylesheet">

</head>
<body>
<header id="top" class="header">
  <div class="text-vertical-center">
    <h1> ${head}</h1>
    <br/>
    <img src="${img}">
    <br>
    <h3>${description}</h3>
    <div id="main-button">
      <a href="<c:url value="/welcome"/>" class="btn btn-dark btn-lg">Return to main page</a>
    </div>
  </div>
</header>
</body>
</html>