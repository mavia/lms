<%@include file="header.jsp"%>

<!-- Top of page -->
<header id="top" class="header">
  <div class="text-vertical-center">
    <h1> LMS </h1>
    <h3>Tasks without executor</h3>
    <br />
    <a href="#task" class="btn btn-dark btn-lg">
      <sec:authorize access="hasRole('laborant')">
        Take task
      </sec:authorize>
      <sec:authorize access="hasRole('admin')">
        View tasks
      </sec:authorize>
    </a>
  </div>
</header>

<hr />

<!-- Task view/create/edit -->
<section id="task">
<!-- Tasks of inactive staffs -->
  <div class="container">
    <h2 align="center" style="clear:both;">Tasks of inactive staffs</h2>
    <br />
    <hr size="5" />

    <table id="tasksTable" class="table">
      <thead>
      <tr>
        <th style="text-align:center;">Id</th>
        <th style="text-align:center;">Room</th>
        <th style="text-align:center;">Creation Time</th>
        <th style="text-align:center;">Creator</th>
        <th style="text-align:center;">Status</th>
        <th style="text-align:center;">Type</th>
        <th style="text-align:center;">Equipment</th>
        <th style="text-align:center;" data-orderable="false">Comments</th>
        <sec:authorize access="hasRole('admin')">
          <th style="text-align:center;">Set staff</th>
          <th style="text-align:center;"></th>
        </sec:authorize>
        <sec:authorize access="hasRole('laborant')">
          <th style="text-align:center;">Take task</th>
        </sec:authorize>

      </tr>
      </thead>
      <tbody>
      <c:forEach items="${tasks}" var="task">
        <tr>
            <td>${task.id}</td>
            <td>${task.eqpt.room.number}</td>
            <td>${task.creationTime.toString().substring(0,16)}</td>
            <td>${task.user.firstName} ${task.user.lastName}</td>
            <td>${task.taskStatus.name}</td>
            <td>${task.taskType.name}</td>
            <td>${task.eqpt.localNumber}</td>
            <td><a href="<c:url value="/tasks/comments/${task.id}" />"
                   data-original-title="View" data-toggle="tooltip"
                   type="button" class="btn btn-sm btn-info"><i
                    class="glyphicon glyphicon-comment"></i> View</a></td>

            <sec:authorize access="hasRole('admin')">
            	<td>
                	<select name="staff${task.id}" class="form-control" id="staff${task.id}" required>
                  		<c:forEach items="${staffs}" var="staff">
                    		<option value='${staff.id}'>${staff.firstName} ${staff.lastName}</option>
                  		</c:forEach>
                	</select>
             	</td>
             	<td>
                	<button data-toggle="modal" data-original-title="Open" data-target=".bs-example-modal-sm"
                        	class="btn btn-sm btn-success" onclick="setStaff(${task.id})"><i
                        	class="glyphicon glyphicon-ok"></i> Set</button>
                </td>
            </sec:authorize>
            <sec:authorize access="hasRole('laborant')">
            	<td>
                	<button data-toggle="modal" data-original-title="Open" data-target=".bs-example-modal-sm"
                        	class="btn btn-sm btn-success" onclick="takeTask(${task.id})"><i
                        	class="glyphicon glyphicon-ok"></i> Take task</button>
            	</td>
            </sec:authorize>
        </tr>
      </c:forEach>
      </tbody>
    </table>

  </div>
  
  <!-- Refused tasks -->
  <div class="container">
  	<h2 align="center" style="clear:both;">Refused tasks</h2>
    <br />
    <hr size="5" />
    
    <table id="tasksTable" class="table">
      <thead>
      <tr>
        <th style="text-align:center;">Id</th>
        <th style="text-align:center;">Room</th>
        <th style="text-align:center;">Creation Time</th>
        <th style="text-align:center;">Creator</th>
        <th style="text-align:center;">Status</th>
        <th style="text-align:center;">Type</th>
        <th style="text-align:center;">Equipment</th>
        <th style="text-align:center;" data-orderable="false">Comments</th>
        <sec:authorize access="hasRole('admin')">
          <th style="text-align:center;">Set staff</th>
          <th style="text-align:center;"></th>
        </sec:authorize>
        <sec:authorize access="hasRole('laborant')">
          <th style="text-align:center;">Take task</th>
        </sec:authorize>

      </tr>
      </thead>
      
      <tbody>
      	<c:forEach items="${refTasks}" var="refTask">
      		<tr>
            <td>${refTask.id}</td>
            <td>${refTask.eqpt.room.number}</td>
            <td>${refTask.creationTime.toString().substring(0,16)}</td>
            <td>${refTask.user.firstName} ${refTask.user.lastName}</td>
            <td>${refTask.taskStatus.name}</td>
            <td>${refTask.taskType.name}</td>
            <td>${refTask.eqpt.localNumber}</td>
            <td><a href="<c:url value="/tasks/comments/${refTask.id}" />"
                   data-original-title="View" data-toggle="tooltip"
                   type="button" class="btn btn-sm btn-info"><i
                    class="glyphicon glyphicon-comment"></i> View</a></td>

            <sec:authorize access="hasRole('admin')">
            	<td>
                	<select name="staff${refTask.id}" class="form-control" id="staff${refTask.id}" required>
                  		<c:forEach items="${staffs}" var="staff">
                    		<option value='${staff.id}'>${staff.firstName} ${staff.lastName}</option>
                  		</c:forEach>
                	</select>
             	</td>
             	<td>
                	<button data-toggle="modal" data-original-title="Open" data-target=".bs-example-modal-sm"
                        	class="btn btn-sm btn-success" onclick="setStaff(${refTask.id})"><i
                        	class="glyphicon glyphicon-ok"></i> Set</button>
                </td>
            </sec:authorize>
            <sec:authorize access="hasRole('laborant')">
            	<td>
                	<button data-toggle="modal" data-original-title="Open" data-target=".bs-example-modal-sm"
                        	class="btn btn-sm btn-success" onclick="takeTask(${refTask.id})"><i
                        	class="glyphicon glyphicon-ok"></i> Take task</button>
            	</td>
            </sec:authorize>
        </tr>
      	</c:forEach>
      </tbody>
    </table>
  </div>
</section>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center">Are you sure?</h4>
      </div>

      <div class="modal-body" align="center">
        <form id="conf" name="conf" action="" method="POST">
          <button type="submit" class="btn btn-sm btn-info">Yes</button>
        </form>
      </div>

    </div>
  </div>
</div>

<script>

  function takeTask(taskId){
    document.getElementById("conf").action = "<c:url value='/changeTaskStaff/" + taskId+ "'/>";
    document.getElementById("conf").method='POST';
  }
  function setStaff(taskId){
    var staffId = $('#staff'+taskId).val();
    if(staffId != 0) {
      document.getElementById("conf").action = ("<c:url value='/setTask/"+taskId+"/"+staffId+"' />");
      document.getElementById("conf").method='POST';
    }
  }
</script>

<%@include file="footer.jsp"%>
