<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Home Page of LabFI">

    <title>LMS</title>
    
	<!-- Shortcut -->
	<link href="<c:url value="/resources/img/shortcut.ico" />" rel="shortcut icon">
	
	<!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    
    <!-- jQuery DataTable -->
    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    
	<!-- Bootstrap Core JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	
    <!-- Bootstrap DataTable JavaScript -->
    <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
	
	 <!-- Bootstrap DataTable CSS -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
	
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    
    <!-- CSS -->
    <link href="<c:url value="/resources/css/styles.css" />" rel="stylesheet">

    <!-- Fonts -->
    <link href="<c:url value="/resources/font-awesome/css/font-awesome.min.css" />" rel="stylesheet" type="text/css">
    
</head>

<body>

    <!-- Navigation -->
    <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle">Menu <i class="fa fa-bars"></i></a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
            <li class="sidebar-brand">
                <a href="#top"  onclick = "$('#menu-close').click()" >Menu</a>
            </li>
            <li>
                <a href="#top" onclick = "$('#menu-close').click()" id="m-id">Top of page</a>
            </li>
            <li>
               <a href="<c:url value="/welcome" />" id="hp-id"> Home page </a>
            </li>
            <sec:authorize access="isAuthenticated()">
            	<li>
               		<a href="<c:url value="/profile" />" id="p-id"> Profile </a>
            	</li>
            	<li>
                <a href="<c:url value="/tasks" />" id="t-id">Tasks</a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasRole('admin')">
            <li>
                <a href="<c:url value="/userAdmin" />" id="ua-id">User Administration</a>
            </li>
            <li>
                <a href="<c:url value="/equipment/list"/>" id="e-id">Equipment</a>
            </li>
            <li>
                <a href="<c:url value="/rooms/list"/>" id="e-id">Rooms</a>
            </li>
            <li>
                <a href="<c:url value="/reports" />" id="r-id">Reports</a>
            </li>
            <li>
                <a href="<c:url value="/freeTasks" />" id="f-id">Free tasks</a>
            </li>
            </sec:authorize>
            <sec:authorize access="hasRole('laborant')">
                <li>
                    <a href="<c:url value="/freeTasks" />" id="f-id">Free tasks</a>
                </li>
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
            	<li>
                	<a href="#login" onclick = "$('#menu-close').click();" >Login</a>
            	</li>
            	<li>
                	<a href="#createtask" onclick = "$('#menu-close').click();" >Report problem</a>
            	</li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
            	<li>
                	<a href="<c:url value="/logout" />" >Logout</a>
            	</li>
            </sec:authorize>
        </ul>
    </nav>