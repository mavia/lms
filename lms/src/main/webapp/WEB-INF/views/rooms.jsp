<%@include file="header.jsp"%>
    
<!-- view/create/edit rooms-->
<section id="room">
	<div class="container">
		<h2 align="center">All rooms</h2>		
		<hr size="5" />
  		<a class="btn btn-sm btn-success" data-toggle="modal" onclick="cleanFields()" data-target=".addRoom" type="button">
  			<font size="3">
  				<i class="glyphicon glyphicon-plus"></i>
  				Add room
  			</font>
  		</a>

		<table class="table table-hover" id = "table">
			<thead id = "tableHead">
				<tr>
					<td>Room</td>
					<td>Staff</td>
					<td data-orderable="false">Edit/Remove</td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<input type="text" id="row" style="display: none">
	</div>
</section>

<!--Modal window for adding a room-->
<div class="modal fade addRoom" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="addWindow">
	<div class="modal-dialog modal-sm">
    	<div class="modal-content">
      		<form method="post" action="#" id="addForm">
      			<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal">&times;</button>
       				<h4 class="modal-title" align="center">Add new room</h4>
      			</div>
      			<div class="modal-body">
     				<div class="form-group">
     					<label for="roomNumber">Room number:</label>
				        <input type="text" class="form-control" id="roomNumber" maxlength="10" required>
			        </div>
      			  	<div class="form-group">
   				   		<label for="staff">Staff:</label>
 				 		<select class="form-control" id="staff" required>
							<option value="-">Not assigned</option>
         			   		<c:forEach items="${staff}" var="staffMember">
           				   		<option value="${staffMember.id}">${staffMember.firstName} ${staffMember.lastName}</option>
            				</c:forEach>
         				</select>
        			</div>
      			</div>
      			<div class="modal-footer">
        			<div class="form-group">
          				<button type="submit" class="btn btn-default">Add</button>
          				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        			</div>
      			</div>
  			</form>  
 		</div>
	</div>
</div>
    
<!--Modal window for editing a room-->
<div class="modal fade editRoom" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="editWindow">
	<div class="modal-dialog modal-sm">
    	<div class="modal-content">
      		<form method="post" action="#" id="editForm">
      			<div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal">&times;</button>
        			<h4 class="modal-title" align="center">Edit room</h4>
 			     </div>
 			     <div class="modal-body">
 			       <div class="form-group">
  				        <label for="roomNumberEdit">Room number:</label>
  				        <input type="text" class="form-control" id="roomNumberEdit" maxlength="10" required>
						<input type="text" class="form-control" id="roomId" style="display:none">
    			    </div>
   				    <div class="form-group">
   				 		<label for="staffEdit">Staff:</label>
   						<select class="form-control" id="staffEdit" required>
							<option value="-">Not assigned</option>
         			  		<c:forEach items="${staff}" var="staffMember">
								<option value="${staffMember.id}">${staffMember.firstName} ${staffMember.lastName}</option>
           					</c:forEach>
 						</select>
        			</div>
      			</div>
      			<div class="modal-footer">
       				<div class="form-group">
       					<button type="submit" class="btn btn-default">Save</button>
         				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        			</div>
      			</div>
  			</form>  
    	</div>
 	</div>
</div> 

<!--Modal window (confirm deleting)-->
<div class="modal fade confirm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="confirm" aria-hidden="true">
	<div class="modal-dialog modal-sm">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        		<h4 class="modal-title">Confirmation</h4>
      		</div>
      		<div class="modal-body">
       			<p>Do you really want to delete room <span id="roomToDelete"></span>?</p>
      		</div>
      		<div class="modal-footer">
       			<button type="button" class="btn btn-default" onclick="del()" data-dismiss="modal">Yes</button>
         		<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      		</div>
    	</div>
 	</div>
</div>

<!--Modal window (error)-->
<div class="modal fade error" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="error" aria-hidden="true">
	<div class="modal-dialog modal-sm">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        		<h4 class="modal-title">Error</h4>
      		</div>
      		<div class="modal-body">
       			<p id="errMessage"></p>
      		</div>
      		<div class="modal-footer">
         		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      		</div>
    	</div>
 	</div>
</div>

<script>
//passing room info to the edit window
  function edit(id, number, staffId) {
	document.getElementById('roomId').value = id;
    document.getElementById('roomNumberEdit').value = number;
    document.getElementById('staffEdit').value = staffId;
  }
  
  //cleaning fields of the addRoom window
  function cleanFields(){
	  $('#staff').val('-');
	  $('#roomNumber').val('');
  }
  
  //ajax call to edit room
  $('#editForm').on('submit', function(e){
	  e.preventDefault();
	  $('#editWindow').modal('hide');
	  var staff = null;
	  if($('#staffEdit').val()!="-")staff = {id: $('#staffEdit').val(), firstName: $('#staffEdit :selected').text()};
	  var room = {id: $('#roomId').val(), number: $('#roomNumberEdit').val(), user: staff};
	  $.ajax({
		  type: "POST",
		  url: '/lms/api/updateRoom',
		  data: JSON.stringify(room),
		  contentType: "application/json; charset=utf-8",
			success: function (room) {
				if(room==""){
					$('#errMessage').text("This room already exists");
					$('#editWindow').modal('show');
					$('#error').modal('show');
				}else{
					var user = room.user;
					if(user ==null)user = {id: "-", firstName: ""};
					$('#table').DataTable().row(document.getElementById(room.id)).data(['<a href="<c:url value="/equipment/list?r='+room.id+'"/>"/>'+room.number+'</a>', user.firstName, '<a data-toggle="modal" data-target=".editRoom" type="button" onclick="edit(' + room.id+', \''+room.number+'\','+ user.id+')" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i>Edit</a> <a data-toggle="tooltip" onclick="confirm('+room.id+', \''+room.number+'\')" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i>Remove</a>']).draw();
				}
			},
			error: function(xhr, status, error) {
				$('#errMessage').text("Something went wrong, the changes were not saved. Please try again.");
				$('#error').modal('show');
			}
	  });
  });
  
  //confirmation dialogue for deleting a room 
  function confirm(id, number){
	  $('#roomToDelete').text(number);
	  $('#confirm').modal('show');
	  $('#confirm').val(id);
  }
  
  //ajax call to delete a room
  function del(){
	  var room = {id: $('#confirm').val()};
		$.ajax({
		  type: "POST",
		  url: '/lms/api/deleteRoom',
		  data: JSON.stringify(room),
		  contentType: "application/json; charset=utf-8",
			success: function (result) {
				if(result === true)$('#table').dataTable().fnDeleteRow(document.getElementById(room.id));	
				else {
					$('#errMessage').text("Can not delete a room with equipment attached.");
					$('#error').modal('show');
				}
			}
		});
  }
  
  //ajax call to create a room
  $('#addForm').on('submit', function (e){
	  e.preventDefault();
	  $('#addWindow').modal('hide');
	  var staff = {id: $('#staff').val(), firstName: $('#staff :selected').text()};
	  var room = {number: $('#roomNumber').val(), user: staff};
	  var roomTable = $('#table').dataTable();	 
	 $.ajax({
		  type: "POST",
		  url: '/lms/api/addRoom',
		  data: JSON.stringify(room),
		  contentType: "application/json; charset=utf-8",
			success: function (room) {
				if(room==""){
					$('#errMessage').text("This room already exists");
					$('#addWindow').modal('show');
					$('#error').modal('show');
				}else{
					var user = room.user;
					if(user ==null)user = {id: "-", firstName: ""};
					var rowIndex = roomTable.fnAddData(['<a href="<c:url value="/equipment/list?r='+room.id+'"/>"/>'+room.number+'</a>', user.firstName, '<a data-toggle="modal" data-target=".editRoom" type="button" onclick="edit('+room.id + ', \'' + room.number + '\',' + user.id+')" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i>Edit</a> <a data-toggle="tooltip" onclick="confirm('+room.id+', \''+room.number+'\')" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i>Remove</a>']);
					var row = roomTable.fnGetNodes(rowIndex);
					$(row).attr('id', room.id);
				}
			},
			error: function(xhr, status, error) {
					$('#errMessage').text("Something went wrong, the room was not created. Please try again.");
					$('#error').modal('show');
				}
	  });
  });
  
  //room table generation
  $(document).ready(function(){
	  var roomTable = $('#table').dataTable();
	  <c:forEach items="${rooms}" var="room">
	  <c:if test="${room.user==null}">var rowIndex = roomTable.fnAddData(['<a href="<c:url value="/equipment/list?r=${room.id}"/>"/>${room.number}</a>', ' ', '<a data-toggle="modal" data-target=".editRoom" type="button" onclick="edit(${room.id}, \'${room.number}\', \'-\')" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i>Edit</a> <a data-toggle="tooltip" type="button" onclick="confirm(${room.id}, \'${room.number}\')" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i>Remove</a>']);</c:if>
		<c:if test="${room.user!=null}">var rowIndex = roomTable.fnAddData(['<a href="<c:url value="/equipment/list?r=${room.id}"/>"/>${room.number}</a>', '${room.user.firstName}'+' '+'${room.user.lastName}', '<a data-toggle="modal" data-target=".editRoom" type="button" onclick="edit(${room.id}, \'${room.number}\', ${room.user.id})" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i>Edit</a> <a data-toggle="tooltip" type="button" onclick="confirm(${room.id}, \'${room.number}\')" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i>Remove</a>']);</c:if>
        	var row = roomTable.fnGetNodes(rowIndex);
        	$(row).attr('id', ${room.id});
      </c:forEach>
  });
</script>


<%@include file="footer.jsp"%>