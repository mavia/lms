<%@include file="header.jsp"%>
<body>

<span class="pull-left"> <a href="<c:url value="/reports" />"
							data-original-title="Back" data-toggle="tooltip"
							type="button" class="btn btn-sm btn-info"><font size="3"><i
		class="glyphicon glyphicon-arrow-left"></i> Back to report page</font></a>
</span></br>

<h2 align="center" style="clear:both;">Room List</h2>

	<h3><a href="../../RoomListExport">Export</a></h3>

<div name="t1" style = "padding:20px;">
		<table id="RoomTable" class="table">
			<thead>
		<tr>
			<td>Room Number</td>
			<td>Malfunctions Count</td>
		</tr>
			</thead>
			<tbody>
		<c:forEach items="${roomList}" var="room">
			<tr>
				<td><c:out value="${room.number}" /></td>
				<td><c:out value="${room.malfunctionsCount}" /></td>
			</tr>
		</c:forEach>
			</tbody>
		</table>
	</div>
</body>

<script>
	$('#RoomTable').DataTable();
</script>

<%@include file="footer.jsp"%>
