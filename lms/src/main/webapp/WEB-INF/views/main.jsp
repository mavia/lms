<%@include file="header.jsp"%>

<c:if test="${not empty createTask}">
	<script>
		$(window).load(function() {
			//$("#modalCreateTask").modal('show');
		});
	</script>
</c:if>

<!-- Main -->
<header id="top" class="header">
	<div class="text-vertical-center">
		<h1>LMS</h1>
		<c:if test="${not empty role}">
			<h2>
				You are logged in as <b>${role}</b>
			</h2>
		</c:if>
		<sec:authorize access="!isAuthenticated()">
			<h3>
				This system allow you to report a problem with equipment in our
				university. <br /> So, please login and help improve our work. <br />
				If you are not registered, you can report a problem too.
			</h3>
			<br>

			<div id="main-button">
				<a href="#login" class="btn btn-dark btn-lg">Login</a>
			</div>
			<br>

			<div id="report-button">
				<a href="#createtask" class="btn btn-dark btn-lg">Report problem</a>
			</div>
		</sec:authorize>
		<sec:authorize access="isAuthenticated()">
			<h3>You are logged in. Now you can click on 'Menu' and start
				work</h3>
		</sec:authorize>
	</div>
</header>

<hr />

<!--Login-->
<section id="login">
	<br />
	<br />
	<br />
	<br />
	<br />

	<div class="container" id="login-style">
		<div style="text-align: center;">
			<!--Login only for not logged in users-->
			<sec:authorize access="!isAuthenticated()">
				<!--If login or password is incorrect-->
				<c:if test="${not empty msg}">
					<div class="alert alert-danger" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign"
							aria-hidden="true"></span> <span class="sr-only">Error:</span>
						${msg}
					</div>
				</c:if>
				<form class="form-signin" role="form" name='myform'
					action="j_spring_security_check" method='POST'>
					<h2 class="form-signin-heading">Please sign in</h2>
					<input type="email" name="username" class="form-control"
						maxlength="320" placeholder="Email address" required> <input
						type="password" name="password" class="form-control"
						placeholder="Password" required><span id="resetPassword" style="cursor:pointer;">Reset password</span> <label class="checkbox">
						<c:if test="${empty msg}">
							<input id="remember_me" type="checkbox" name="_spring_security_remember_me" value="true"> Remember me
                        </c:if> <c:if test="${not empty msg}">
							<input id="remember_me" type="checkbox" name="_spring_security_remember_me" value="true" autofocus> Remember me
                        </c:if>
					</label>
					
					<button class="btn btn-lg btn-primary btn-block" name="Submit"
						value="Login" type="submit">Sign in</button>
				</form>
			</sec:authorize>
		</div>
	</div>
	<br />
	<br />
	<br />
	<hr />
</section>

<!--Report problem-->
<sec:authorize access="!isAuthenticated()">
	<!-- Create task -->
	<section id="createtask">
		<br />
		<br />
		<br />

		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h1>Create task</h1>
				</div>
			</div>
		</div>
	</section>



	<section id="createtask-style">
		<div class="container" style="max-width: 500px;">
			<div style="text-align: center;">
			
				<form class="form-horizontal" role="form" name='taskform'
					action="createTask" method='POST' id="formCreateTask">
					<div class="form-group" id="roomAndEqpt" style="max-width: 300px;margin: auto;">

						<select name="room" class="form-control" id="rooms" required>
							<option value='0'>Choose room</option>
							<c:forEach items="${rooms}" var="room">
								<option value="${room.id}">${room.number}</option>
							</c:forEach>
						</select> <select name="eqpt" class="form-control" id="eqpt" required>
							<option value=''>First select a room</option>
						</select> <select name="typeTask" class="form-control" id="typeTask"
							required>
							<option value=''>Choose type</option>
							<c:forEach items="${typesTask}" var="typeTask">
								<option value="${typeTask.id}">${typeTask.name}</option>
							</c:forEach>
						</select>
					</div>

					<br />
				
					<div  id="commentField">
						<p>We already know about some problems with this equipment:</p>
						<ul  id="comments" type="none"  style="height:200px; width:500px; overflow-y:auto;margin: auto;">
						</ul>
						<p>If you find new problem with this equipment, please report:</p>
					</div>
					<br />

					<div class="form-group" id="comment" style="max-width: 300px;margin: auto;">
						<input type="email" class="form-control" name="inputEmail"
							id="inputEmail" maxlength="320" placeholder="Email" required>
						<textarea rows="3" class="form-control" name="postalAddress"
							id="postalAddress" placeholder="Add description" required></textarea>
							<input type="hidden" name="create_task_sec" value="${create_task_sec}">
							<div class="alert alert-danger" role="alert" id="alertCreateTask">
									<span class="glyphicon"
										aria-hidden="true" id="iconCreateTask"></span> <span class="sr-only" >Error:</span>
									<span id="resultCreateTask"></span>
								</div>
							<div id="loadCreateTask"></div>	
						<input type="submit" class="btn btn-primary" value="Submit">
						<input type="reset" class="btn btn-default" value="Clear all">
					</div>
				</form>
			</div>
		</div>

	</section>
</sec:authorize>

<!-- After create task -->
<div id="modalCreateTask" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
			</div>

			<div class="modal-body">
				<h4 class="modal-title" align="center">${createTask}</h4>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- After create task -->
<div id="modalResetPass" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
			</div>

			<div class="modal-body">
				<div class="container" id="login-style">
					<div style="text-align: center;">
						<!--Login only for not logged in users-->
						<sec:authorize access="!isAuthenticated()">
							<!--If login or password is incorrect-->
							<div class="alert alert-danger" role="alert" id="alertResetPass">
									<span class="glyphicon"
										aria-hidden="true" id="iconResetPass"></span> <span class="sr-only" >Error:</span>
									<span id="resultResetPass"></span>
								</div>
							<form class="form-reset" id="formResetPass"
								action="" method='POST' >
								<h3 class="form-signin-heading">Please enter your email:</h3>
								<input type="email" name="email" class="form-control"
									maxlength="320" placeholder="Email address" required>
								<input type="hidden" name="reset_sec" value="${reset_sec}">
								<br> 
								<div id="loadResetPass"></div>	
								<button class="btn btn-lg btn-primary btn-block" name="Submit"
									value="Reset" id="submitResetPass" type="submit">Reset</button>
							</form>
						</sec:authorize>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<hr />

<script>
var loader="<img src='/lms/resources/img/482.GIF'>";
var formResetPass=$('#formResetPass');
var iconResetPass = $('#iconResetPass');
var alertResetPass = $('#alertResetPass');
$('#formResetPass').submit(function(){
	$('#loadResetPass').html(loader);
	$.ajax({
          type: "POST",
          url: "/lms/resetPassword",
          data: formResetPass.serialize(),
          success: function(data) {
        	  if (data.status=="ok"){
        		  iconResetPass.attr("class","glyphicon glyphicon-ok-sign"); 
        		  alertResetPass.attr("class","alert alert-success");
        		  setTimeout("alertResetPass.hide();$('.close').click();",5000);
        	  }else{
        	  	iconResetPass.attr("class","glyphicon glyphicon-exclamation-sign");
        	  	alertResetPass.attr("class","alert alert-danger");
        	  }
        	  alertResetPass.show(); 
        	  $('#loadResetPass').html("");
        	  $('#resultResetPass').html(data.message);
        	  formResetPass.trigger( 'reset' );
        	  
          },
          
        });
	return false;
});
var formCreateTask=$('#formCreateTask');
var iconCreateTask = $('#iconCreateTask');
var alertCreateTask = $('#alertCreateTask');
$('#formCreateTask').submit(function(){
	$('#loadCreateTask').html(loader);
	$.ajax({
          type: "POST",
          url: "/lms/createTask",
          data: formCreateTask.serialize(),
          success: function(data) {
        	  if (data.status=="ok"){
        		  iconCreateTask.attr("class","glyphicon glyphicon-ok-sign"); 
        		  alertCreateTask.attr("class","alert alert-success");
        	  }else{
        		  iconCreateTask.attr("class","glyphicon glyphicon-exclamation-sign");
        		  alertCreateTask.attr("class","alert alert-danger");
        	  }
        	  alertCreateTask.show();
        	  $('#loadCreateTask').html("");
        	  $('#resultCreateTask').html(data.message);
        	  formCreateTask.trigger( 'reset' );
        	  setTimeout("alertCreateTask.hide()",5000);
          },
          
        });
	
	return false;
});
	$(document).ready(function() {
		$('#commentField').hide();
		$('#alertCreateTask').hide(); 
		
	});
	// Update equipment list
	$(document)
			.ready(
					
					function() {

						var resetPassword = $('#resetPassword');
						resetPassword.click(function(){
							$("#modalResetPass").modal('show');
							alertResetPass.hide(); 
							
						});
						
						var room = $('#rooms');
						room.change(function() {
									var currentRoom = 0;
									if (room.val() != 0) {
										currentRoom = parseInt(room.val());
									}
									$('#loadCreateTask').html(loader);
									$.ajax({
												type : "POST",
												url : '/lms/api/listEquipment',
												data : JSON
														.stringify(currentRoom),
												contentType : "application/json; charset=utf-8",
												dataType : "json",
												success : function(equipments) {
													$('#loadCreateTask').html("");
													$('#commentField').hide();
													$('#eqpt option').remove();
													$('#eqpt').append("<option value=''>Choose equipment</option>");
													$.each(equipments,function(index,equipment) {
																		$('#eqpt').append("<option value='" + equipment.id + "'>"
																								+ equipment.localNumber
																								+ "</option>");
																	});
												}
											});
								});
					});

	$(document).ready(
			function() {
				var eqpt = $('#eqpt');
				eqpt.change(function() {
					var currentEqpt = 0;
					if (eqpt.val() != 0) {
						currentEqpt = parseInt(eqpt.val());
					}
					$('#loadCreateTask').html(loader);
					$.ajax({
						type : "POST",
						url : '/lms/api/listComment',
						data : JSON.stringify(currentEqpt),
						contentType : "application/json; charset=utf-8",
						dataType : "json",
						success : function(comments) {
							$('#loadCreateTask').html("");
							$('#commentField').hide();
							$('#comments').empty();
							if (comments.length > 0) {
								$('#commentField').show();
							}
							$.each(comments, function(index, comment) {
								$('#comments').append(
										"<li><div class=\"alert alert-info\">"
												+ comment + "</div></li>");
							});
						},
						error : function() {
							$('#loadCreateTask').html("");
							$('#comments').empty();
							$('#commentField').hide();
						}
					});
				});
			});
</script>
<%@include file="footer.jsp"%>