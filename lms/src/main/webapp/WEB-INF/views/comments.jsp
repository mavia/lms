<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<html lang="en">

<head>

<title>LMS</title>

<!-- Shortcut -->
<link href="<c:url value="/resources/img/shortcut.ico" />"
	rel="shortcut icon">

<!-- jQuery -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Bootstrap Core CSS -->
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- CSS -->
<link href="<c:url value="/resources/css/styles.css" />"
	rel="stylesheet">
	
<style>
	.floatcss {
		list-style:none;
		padding:0;
		margin:0;
		width:100%;
		word-wrap:break-word;
 	}
</style>

</head>

<body>
	<span class="pull-left"> <a href="<c:url value="/tasks" />"
		data-original-title="Back" data-toggle="tooltip" type="button"
		class="btn btn-sm btn-info"><font size="3"><i
				class="glyphicon glyphicon-arrow-left"></i> Back to task page</font></a>
	</span>

	<div class="container" align="center">
		<div class="detailBox">

			<div class="titleBox" align="center">
				<label>Task comments</label>
			</div>

			<div class="commentBox" align="center">
				<c:if test="${not empty changeStatus}">
					<p class="taskDescription">Add comment to change status of
						task.</p>
				</c:if>
				<c:if test="${empty changeStatus}">
					<p class="taskDescription">You can view all comments of
						selected task and add new comments.</p>
				</c:if>
			</div>

			<div class="actionBox" align="left">
				<ul class="commentList">
					<c:forEach items="${comments}" var="comment">
						<li class="floatcss">
							<div class="commenterImage">
								<img src="<c:url value="/resources/img/shortcut.ico" />" />
							</div>
							<div class="commentText">
								<p class="">${comment.text}</p>
								<span class="date sub-text">by <b>${comment.user.firstName}
										${comment.user.lastName}</b>, ${comment.date}
								</span>
							</div>
						</li>
						<hr />
					</c:forEach>
				</ul>

				<c:if test="${empty changeStatus}">
					<form class="form-inline" role="form"
						action="<c:url value="/createComment" />" method='POST'>
						<div class="form-group">
							<input class="form-control" type="text" name="idTask" id="idTask"
								style="display: none" value="${idTask}" />
							<textarea rows="3" class="form-control" name="commentText"
								placeholder="Your comments" required></textarea>
						</div>
						<div class="form-group">
							<button class="btn btn-default">Add</button>
						</div>
					</form>
				</c:if>

				<c:if test="${not empty changeStatus}">
					<form class="form-inline" role="form"
						action="<c:url value="/createComment?changeStatus=${changeStatus}" />"
						method='POST'>
						<div class="form-group">
							<input class="form-control" type="text" name="idTask" id="idTask"
								style="display: none" value="${idTask}" />
							<textarea rows="3" class="form-control" name="commentText"
								placeholder="Your comments" required></textarea>
						</div>
						<div class="form-group">
							<button class="btn btn-default">Add</button>
						</div>
					</form>
				</c:if>
			</div>
		</div>
	</div>

	<%@include file="footer.jsp"%>