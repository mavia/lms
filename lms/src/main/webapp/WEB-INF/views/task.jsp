<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<html lang="en">

<head>

  <title>LMS</title>

  <!-- Shortcut -->
  <link href="<c:url value="/resources/img/shortcut.ico" />" rel="shortcut icon">

  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  <!-- Bootstrap Core CSS -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <!-- CSS -->
  <link href="<c:url value="/resources/css/styles.css" />" rel="stylesheet">

</head>
<body>
  <span class="pull-left"> <a href="<c:url value="/tasks" />"
                              data-original-title="Back" data-toggle="tooltip"
                              type="button" class="btn btn-sm btn-info"><font size="3"><i
          class="glyphicon glyphicon-arrow-left"></i> Back to task page</font></a>
  </span>
  </br>
  <div class="container" style="max-width:500px;">
    <div style="text-align:center;">
      <form class="form-horizontal" role="form" name='taskform' action="<c:url value="/takeTask/${currentTask.id}" />"  method='POST'>
        <div class="form-group" id="currentTask">
          <div class="table-responsive">
            <table class="table">
              <h2>
                <thead>
                  <tr class="info">
                    <th>Date :</th>
                    <th>${currentTask.creationTime.toString().substring(0,16)}</th>
                  </tr>
                  <tr class="info">
                    <th>Status:</th>
                    <th>${currentTask.taskStatus.name}</th>
                  </tr>
                  <tr class="info">
                    <th>Room:</th>
                    <th>${currentTask.eqpt.room.number}</th>
                  </tr>
                  <tr class="info">
                    <th>Equipment:</th>
                    <th>${currentTask.eqpt.localNumber}</th>
                  </tr>
                  <tr class="info">
                    <th>Type:</th>
                    <th>
                      	<select name="typeTask" class="form-control" id="typeTask" required>
							<c:forEach items="${typesTask}" var="typeTask">
								<c:if test="${currentTask.taskType.id == typeTask.id}">
									<option value="${typeTask.id}" selected>${typeTask.name}</option>
								</c:if>
								<c:if test="${currentTask.taskType.id != typeTask.id}">
									<option value="${typeTask.id}">${typeTask.name}</option>
								</c:if>
							</c:forEach>
						</select>
                    </th>
                  </tr>
                  <tr class="info">
                    <th>This task can duplicates another task for this equipment:</th>
                    <th>
                      <select name="sameTasks" class="form-control" id="sameTasks" required>
                        <option value='${currentTask.id}'> This task is original </option> </br>
                        <c:forEach items="${sameTasks}" var="same">
                          <option value='${same.key.id}'> ${same.value.text} </option> </br>
                        </c:forEach>
                      </select>
                    </th>
                  </tr>
               </thead>
              </h2>
            </table>
            <div name="commentField" id="commentField">
              <p>Comment:</p>
              <ul class="commentList">
                <c:forEach items="${comments}" var="comment">
                  <li>
                    <div class="commenterImage">
                      <img src="<c:url value="/resources/img/shortcut.ico" />" />
                    </div>
                    <div class="commentText">
                      <p class="">${comment.text}</p> <span class="date sub-text">by <b>${comment.user.firstName} ${comment.user.lastName}</b>, ${comment.date}</span>
                    </div>
                  </li>
                  <hr />
                </c:forEach>
              </ul>
            </div>
          </div>
          <input type="submit" class="btn btn-primary" value="Take task">
        </div>
      </form>
    </div>
  </div>
  <%@include file="footer.jsp"%>

</body>
</html>