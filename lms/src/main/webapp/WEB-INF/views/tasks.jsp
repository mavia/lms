<%@include file="header.jsp"%>
    
    <c:if test="${not empty createTask}">
    	<script>
    		$(window).load(function(){
    			$("#modalCreateTask").modal('show');
    		});
    	</script>
    </c:if>
    
    <!-- Top of page -->
    <header id="top" class="header">
        <div class="text-vertical-center">
            <h1> LMS </h1>
            <h3>Here you can work with tasks.</h3>
            <br /> 
            <a href="#task" class="btn btn-dark btn-lg">
            <sec:authorize access="hasRole('laborant')">
            Do tasks
            </sec:authorize>
            <sec:authorize access="hasRole('user')">
            Report problem
            </sec:authorize>
            <sec:authorize access="hasRole('admin')">
            View tasks
            </sec:authorize>
            </a>
        </div>
    </header>
    
    <hr />
    
<!-- Task view/create/edit -->
<section id="task">
	<div class="container">
		<sec:authorize access="hasRole('admin')">
			<h2 align="center" style="clear:both;">All tasks</h2>
		</sec:authorize>
		<sec:authorize access="!hasRole('admin')">
			<h2 align="center" style="clear:both;">Your tasks</h2>
		</sec:authorize>
		<sec:authorize access="!hasRole('laborant')">
		<c:if test="${empty ban}">
		<span class="pull-right"> <a href="<c:url value="/tasks/create" />"
					data-original-title="Add" data-toggle="tooltip"
					type="button" class="btn btn-sm btn-success"><font size="3"><i
						class="glyphicon glyphicon-plus"></i>Report problem</font></a>
		</span>
		</c:if>
		<c:if test="${not empty ban}">
			<span class="pull-right">You are banned</span>
		</c:if>
		</sec:authorize>
		<br />
		<hr size="5" />
		
		<table id="tasksTable" class="table">
			<thead>
				<tr>
					<th style="text-align:center;">Id</th>
					<th style="text-align:center;">Room</th>
				 <sec:authorize access="hasRole('user')">
				 	<th style="text-align:center;">Staff member</th>
					<th style="text-align:center;">Status</th>
					<th style="text-align:center;">Type</th>
					<th style="text-align:center;">Equipment</th>
					<th style="text-align:center;" data-orderable="false">Comments</th>
				 </sec:authorize>
				 <sec:authorize access="hasRole('admin')">
					<th style="text-align:center;">Creation Time</th>
					<th style="text-align:center;">Creator</th>
					<th style="text-align:center;">Staff member</th>
					<th style="text-align:center;">Status</th>
					<th style="text-align:center;">Type</th>
					<th style="text-align:center;">Equipment</th>
					<th style="text-align:center;" data-orderable="false">Comments</th>
					<th style="text-align:center;">Completion Time</th>
				</sec:authorize>
				<sec:authorize access="hasRole('laborant')">
					<th style="text-align:center;">Creation Time</th>
					<th style="text-align:center;">Status</th>
					<th style="text-align:center;">Type</th>
					<th style="text-align:center;">Equipment</th>
					<th style="text-align:center;" data-orderable="false">Comments</th>
				</sec:authorize>
					<th style="text-align:center;" data-orderable="false">Change Status</th>
				</tr>
			</thead>
				<tbody>
					<c:forEach items="${tasks}" var="task">
						<tr>
							<td>${task.id}</td>
							<th style="text-align:center;">${task.eqpt.room.number}</th>
						<sec:authorize access="hasRole('admin')">
        					<td>${task.creationTime.toString().substring(0,16)}</td>
        					<td>${task.user.firstName} ${task.user.lastName}</td>
        					<td>${task.staff.firstName} ${task.staff.lastName}</td>
        					<td>${task.taskStatus.name}</td>
        					<td>${task.taskType.name}</td>
        					<td>${task.eqpt.localNumber}</td>
        					<td><a href="<c:url value="/tasks/comments/${task.id}" />"
									data-original-title="View" data-toggle="tooltip"
									type="button" class="btn btn-sm btn-info"><i
									class="glyphicon glyphicon-comment"></i> View</a></td>
        					<td>${task.completionTime}</td>
        					<td>
        						<c:if test="${task.taskStatus.name == 'completed'}">
        							<button data-toggle="modal" data-original-title="Completed" 
									class="btn btn-sm btn-success" data-target=".bs-example-modal-lg" onclick="resumeByAdmin(${task.id})"><i
									class="glyphicon glyphicon-repeat"></i> Resume</button>
        						</c:if>
        					</td>
        				</sec:authorize>
        				<sec:authorize access="hasRole('user')">
        					<td>${task.staff.firstName} ${task.staff.lastName}</td>
        					<td>${task.taskStatus.name}</td>
        					<td>${task.taskType.name}</td>
        					<td>${task.eqpt.localNumber}</td>
        					<td><a href="<c:url value="/tasks/comments/${task.id}" />" data-original-title="View" data-toggle="tooltip"
									type="button" class="btn btn-sm btn-info"><i
									class="glyphicon glyphicon-comment"></i> View</a></td>
							<td>
        						<c:if test="${task.taskStatus.name == 'open'}">
        							<button data-toggle="modal" data-original-title="Open"
									class="btn btn-sm btn-danger" data-target=".bs-example-modal-lg" onclick="cancelByUser(${task.id})"><i
									class="glyphicon glyphicon-remove"></i> Cancel</button>
        						</c:if>
        					</td>
        				</sec:authorize>
        				<sec:authorize access="hasRole('laborant')">
        					<td>${task.creationTime.toString().substring(0,16)}</td>
        					<td>${task.taskStatus.name}</td>
        					<td>${task.taskType.name}</td>
        					<td>${task.eqpt.localNumber}</td>
        					<td><a href="<c:url value="/tasks/comments/${task.id}" />"
									data-original-title="View" data-toggle="tooltip"
									type="button" class="btn btn-sm btn-info"><i
									class="glyphicon glyphicon-comment"></i> View</a></td>
							<td>
								<c:if test="${task.taskStatus.name == 'open'}">

									<button data-original-title="Open"
									class="btn btn-sm btn-warning"  onclick="takeTask(${task.id})">
									<i class="glyphicon glyphicon-list-alt"></i> Take</button>

									<button data-toggle="modal" data-original-title="Open"
									class="btn btn-sm btn-danger" data-target=".bs-example-modal-lg" onclick="cancelByStaff(${task.id})"><i
									class="glyphicon glyphicon-remove"></i> Cancel</button>

									<button data-toggle="modal" data-original-title="Open"
									class="btn btn-sm btn-primary" data-target=".bs-example-modal-lg" onclick="refuseByStaff(${task.id})"><i
									class="glyphicon glyphicon-upload"></i> Refuse</button>
								</c:if>

								<c:if test="${task.taskStatus.name == 'in progress'}">
									<button data-toggle="modal" data-original-title="Open" data-target=".bs-example-modal-lg"
									class="btn btn-sm btn-success" onclick="doneTask(${task.id})"><i
									class="glyphicon glyphicon-ok"></i> Done</button>
									<button data-toggle="modal" data-original-title="Open"
									class="btn btn-sm btn-primary" data-target=".bs-example-modal-lg" onclick="refuseByStaff(${task.id})"><i
									class="glyphicon glyphicon-upload"></i> Refuse</button>
								</c:if>
							</td>
        				</sec:authorize>
      					</tr>
					</c:forEach>
				</tbody>
		</table>
	</div>
</section>

<!-- Confirm change status of task -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    
    	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal">&times;</button>
        	<h4 class="modal-title" align="center">You need to add a comment to complete the operation.</h4>
      	</div>
      	
      	<div class="modal-body" align="center">
      		<a href="#" id="addComment" data-original-title="Open" data-toggle="tooltip" type="button" class="btn btn-sm btn-info">Add comment</a>
      	</div>
      
    </div>
  </div>
</div>

<!-- After create task -->
<div id="modalCreateTask" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">&times;</button>
            </div>
            
            <div class="modal-body">
            	<h3 class="modal-title" align="center">${createTask}</h3>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>	
        </div>	
    </div>
</div>

<script>
	$('#tasksTable').DataTable();

	function refuseByStaff(taskId){
		document.getElementById("addComment").href = "<c:url value='/tasks/comments/"+taskId+"?changeStatus=laborant3' />";
	}
	
	function cancelByUser(taskId){
		document.getElementById("addComment").href = "<c:url value='/tasks/comments/"+taskId+"?changeStatus=user' />";
	}
	
	function resumeByAdmin(taskId){
		document.getElementById("addComment").href = "<c:url value='/tasks/comments/"+taskId+"?changeStatus=admin' />";
	}
	
	function cancelByStaff(taskId){
		document.getElementById("addComment").href = "<c:url value='/tasks/comments/"+taskId+"?changeStatus=laborant1' />";
	}
	
	function doneTask(taskId){
		document.getElementById("addComment").href = ("<c:url value='/tasks/comments/"+taskId+"?changeStatus=laborant2' />");
	}

	function takeTask(taskId){
		window.location.replace("<c:url value='/tasks/task/"+taskId+"?changeStatus=laborant1' />");
	}
</script>
    
<%@include file="footer.jsp"%>