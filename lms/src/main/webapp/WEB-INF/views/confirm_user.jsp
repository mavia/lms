<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <title>LMS</title>
    <!-- Shortcut -->
    <link href="resources/img/shortcut.ico" rel="shortcut icon">
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!-- CSS -->
    <link href="resources/css/styles.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 align="center">Email confirmation page</h1>
                <hr size="5" />
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <p>You have confirmed your email successfully, please check your email to get details.</p>
            </div>
            <div class="col-lg-4">
      </div>

  </div>
</div>
</body>
</html>