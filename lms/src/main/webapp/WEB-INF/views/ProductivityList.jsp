<%@include file="header.jsp"%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-1"%>
<html lang="en">

<body>

<span class="pull-left"> <a href="<c:url value="/reports" />"
							data-original-title="Back" data-toggle="tooltip"
							type="button" class="btn btn-sm btn-info"><font size="3"><i
		class="glyphicon glyphicon-arrow-left"></i> Back to report page</font></a>
</span></br>

<h2 align="center" style="clear:both;">Productivity List</h2>

	<h3><a href="../../ProductivityListExport">Export</a></h3>

<div name="t1" style = "padding:20px;">
		<table id="ProductivityTable" class="table">
			<thead>
		<tr>
			<td>User ID</td>
			<td>First Name</td>
			<td>Second Name</td>
			<td>Completed</td>
			<td>Rejected</td>
		</tr>
			</thead>
			<tbody>
		<c:forEach items="${productivityList}" var="staff">
			<tr>
				<td><c:out value="${staff.id}" /></td>
				<td><c:out value="${staff.firstName}" /></td>
				<td><c:out value="${staff.lastName}" /></td>
				<td><c:out value="${staff.countCompleted}" /></td>
				<td><c:out value="${staff.countCanceled}" /></td>
			</tr>
		</c:forEach>
			</tbody>
		</table>
</div>

</body>

<script>
	$('#ProductivityTable').DataTable();
</script>

<%@include file="footer.jsp"%>
