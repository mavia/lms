package ukma.baratheons.lms.service;

import org.mockito.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ukma.baratheons.lms.dao.CommentDao;
import ukma.baratheons.lms.entity.Comment;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class CommentServiceImplTest {
    @Mock
    private CommentDao commentDao;
    @InjectMocks
    private CommentService commentService;
    @Spy
    private List<Comment> comments;
    @Captor
    private ArgumentCaptor<Comment> captor;

    @BeforeClass
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        comments = getCommentsList();
    }

    private List<Comment> getCommentsList() {
        return null;
    }

    @Test
    public void testAddNew() throws Exception {

    }

    @Test
    public void testGetCommentsByEqpt() throws Exception {

    }

    @Test
    public void testGetCommentsByTask() throws Exception {

    }
}