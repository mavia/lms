package ukma.baratheons.lms.query;


import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.BeforeClass;
import org.junit.Test;

import ukma.baratheons.lms.query.Logical.Operator;



public class TestQueryBuilder {


    /**Database name;*/
    private static final String TABLE_USER = "user_temp";
    /**Database name;*/
    private static final String TABLE_PURCHASE = "purchase_temp";

    private static Connection connection;


    @BeforeClass
    public static void onSetup() {

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://fbaratheons.cloudapp.net/test", "admin", "dev4NetCr");

            StringBuilder createSql = new StringBuilder("CREATE TEMPORARY TABLE IF NOT EXISTS ");
            createSql.append('"').append(TABLE_USER).append('"');
            createSql.append(" (id SERIAL PRIMARY KEY NOT NULL, name TEXT, age INTEGER, salary INTEGER)");

            System.out.println(createSql.toString());
            connection.prepareStatement(createSql.toString()).execute();
            createSql.setLength(0);

            createSql.append("INSERT INTO ").append('"').append(TABLE_USER).append('"');
            createSql.append(" (name,age,salary) VALUES ('Max', 20, 400)");
            System.out.println(createSql.toString());
            connection.prepareStatement(createSql.toString()).execute();
            createSql.setLength(0);

            createSql.append("INSERT INTO ").append('"').append(TABLE_USER).append('"');
            createSql.append(" (name,age,salary) VALUES ('Ivan', 18, 500)");
            System.out.println(createSql.toString());
            connection.prepareStatement(createSql.toString()).execute();
            createSql.setLength(0);

            createSql.append("INSERT INTO ").append('"').append(TABLE_USER).append('"');
            createSql.append(" (name,age,salary) VALUES ('Root', 30, 140)");
            System.out.println(createSql.toString());
            connection.prepareStatement(createSql.toString()).execute();
            createSql.setLength(0);

            createSql.append("INSERT INTO ").append('"').append(TABLE_USER).append('"');
            createSql.append(" (name,age,salary) VALUES ('Mark', 21, 240)");
            System.out.println(createSql.toString());
            connection.prepareStatement(createSql.toString()).execute();
            createSql.setLength(0);

            createSql.append("CREATE TEMPORARY TABLE IF NOT EXISTS ");
            createSql.append('"').append(TABLE_PURCHASE).append('"');
            createSql.append(" (user_id INTEGER REFERENCES user_temp(id) NOT NULL, description TEXT, price INTEGER)");
            System.out.println(createSql.toString());
            connection.prepareStatement(createSql.toString()).execute();
            createSql.setLength(0);

            createSql.append("INSERT INTO ").append('"').append(TABLE_PURCHASE).append('"');
            createSql.append(" VALUES (1,'max purchase', 100)");
            System.out.println(createSql.toString());
            connection.prepareStatement(createSql.toString()).execute();
            createSql.setLength(0);

            createSql.append("INSERT INTO ").append('"').append(TABLE_PURCHASE).append('"');
            createSql.append(" VALUES (4,'max purchase', 43)");
            System.out.println(createSql.toString());
            connection.prepareStatement(createSql.toString()).execute();
            createSql.setLength(0);

            createSql.append("INSERT INTO ").append('"').append(TABLE_PURCHASE).append('"');
            createSql.append(" VALUES (2,null, 23)");
            System.out.println(createSql.toString());
            connection.prepareStatement(createSql.toString()).execute();
            createSql.setLength(0);

            createSql.append("INSERT INTO ").append('"').append(TABLE_PURCHASE).append('"');
            createSql.append(" VALUES (3,null, 10)");
            System.out.println(createSql.toString());
            connection.prepareStatement(createSql.toString()).execute();
            createSql.setLength(0);
            
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Test
    public void testSelectAll() {

        SelectQuery queryBuilder = SelectQueryBuilder.selectQuery();
        String sql = queryBuilder.from(TABLE_USER).toSql();

        System.out.println("*********test select all********");
        System.out.println("SQL " + sql);

        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql)){

            while(result.next()) {

                System.out.print(result.getInt("id") + String.valueOf(' '));
                System.out.print(result.getString("name") + String.valueOf(' '));
                System.out.print(result.getInt("age") + String.valueOf(' '));
                System.out.println(result.getDouble("salary"));
            }

        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSelectCondition1() {

        SelectQuery queryBuilder = SelectQueryBuilder.selectQuery();
        Where where = WhereBuilder.where("id", Logical.Operator.EQ, 2, '\'');
        
        String sql = queryBuilder.from(TABLE_USER).where(where).toSql();

        System.out.println("*********test select condition id = 2********");
        System.out.println("SQL " + sql);

        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql)){

            while(result.next()) {

                System.out.print(result.getInt("id") + String.valueOf(' '));
                System.out.print(result.getString("name") + String.valueOf(' '));
                System.out.print(result.getInt("age") + String.valueOf(' '));
                System.out.println(result.getDouble("salary"));
            }

        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void testSelectConditionPrepared1() {

        SelectQuery queryBuilder = SelectQueryBuilder.selectQuery();
        Where where = WhereBuilder.where("name", '?');
        
        String sql = queryBuilder.from(TABLE_USER).where(where).toSql();

        System.out.println("*********test select condition on name like 'oot' prepared********");
        System.out.println("SQL " + sql);

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
        	statement.setString(1, "%Root%");
             ResultSet result = statement.executeQuery();

            while(result.next()) {

                System.out.print(result.getInt("id") + String.valueOf(' '));
                System.out.print(result.getString("name") + String.valueOf(' '));
                System.out.print(result.getInt("age") + String.valueOf(' '));
                System.out.println(result.getDouble("salary"));
            }

        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSelectCondition2() {

        SelectQuery queryBuilder = SelectQueryBuilder.selectQuery();
        Where where = WhereBuilder.where("salary", Logical.Operator.LT, 250, '\'');
        
        String sql = queryBuilder.from(TABLE_USER).where(where).toSql();

        System.out.println("*********test select condition salary < 250********");
        System.out.println("SQL " + sql);

        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql)){

            while(result.next()) {

                System.out.print(result.getInt("id") + String.valueOf(' '));
                System.out.print(result.getString("name") + String.valueOf(' '));
                System.out.print(result.getInt("age") + String.valueOf(' '));
                System.out.println(result.getDouble("salary"));
            }

        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testJoin1() {

        SelectQuery queryBuilder = SelectQueryBuilder.selectQuery();
        Join join = JoinBuilder.innerJoin
				        		(
				        		TABLE_PURCHASE,
				                "purchase",
				                "purchase.user_id",
				                Logical.Operator.EQ,
				                TABLE_USER + ".id"
				                );
       
        queryBuilder.from(TABLE_USER).join(join);
        
        String sql = queryBuilder.toSql();

        System.out.println("*********test inner join on id********");
        System.out.println("SQL " + sql);

        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql)){

            while(result.next()) {

                System.out.print(result.getInt("id") + String.valueOf(' '));
                System.out.print(result.getString("name") + String.valueOf(' '));
                System.out.print(result.getInt("age") + String.valueOf(' '));
                System.out.print(result.getDouble("salary"));

                System.out.print(result.getInt("user_id") + String.valueOf(' '));
                System.out.print(result.getString("description") + String.valueOf(' '));
                System.out.println(result.getInt("price") + String.valueOf(' '));

            }

        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void testJoin2() {

        SelectQuery queryBuilder = SelectQueryBuilder.selectQuery();
        Join join = JoinBuilder.innerJoin
		        				(
		        				 TABLE_PURCHASE,
		                         "purchase",
		                         "purchase.user_id",
		                         Logical.Operator.EQ,
		                         "2",
		                         '\''
		        				);
        
        String sql = queryBuilder.from(TABLE_USER).join(join).toSql();

        System.out.println("*********test inner join on id********");
        System.out.println("SQL " + sql);

        try (Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(sql)){

            while(result.next()) {

                System.out.print(result.getInt("id") + String.valueOf(' '));
                System.out.print(result.getString("name") + String.valueOf(' '));
                System.out.print(result.getInt("age") + String.valueOf(' '));
                System.out.print(result.getDouble("salary"));

                System.out.print(result.getInt("user_id") + String.valueOf(' '));
                System.out.print(result.getString("description") + String.valueOf(' '));
                System.out.println(result.getInt("price") + String.valueOf(' '));

            }

        } catch (SQLException e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void testJoinPrepared2() {

        SelectQuery queryBuilder = SelectQueryBuilder.selectQuery();
        Join join = JoinBuilder.innerJoin
		        				(
		        				 TABLE_PURCHASE,
		                         "purchase",
		                         "purchase.user_id",
		                         Logical.Operator.EQ,
		                         '?'
		        				);
        
        String sql = queryBuilder.from(TABLE_USER).join(join).toSql();

        System.out.println("*********test inner join on id prepared********");
        System.out.println("SQL " + sql);

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
        		statement.setInt(1, 2);
        		ResultSet result = statement.executeQuery();

        		while(result.next()) {

	                System.out.print(result.getInt("id") + String.valueOf(' '));
	                System.out.print(result.getString("name") + String.valueOf(' '));
	                System.out.print(result.getInt("age") + String.valueOf(' '));
	                System.out.print(result.getDouble("salary"));
	
	                System.out.print(result.getInt("user_id") + String.valueOf(' '));
	                System.out.print(result.getString("description") + String.valueOf(' '));
	                System.out.println(result.getInt("price") + String.valueOf(' '));

        		}

        } catch (SQLException e) {
        	e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchQuery() {

        Connection connection = null;
		try {
			connection = DriverManager.getConnection(
			        "jdbc:postgresql://fbaratheons.cloudapp.net/lms","admin", "dev4NetCr");
		} catch (SQLException e) {
			fail(e.getMessage());
		}

		/*Selects all information about task
		 * t1 is task table
		 * t2 is staff
		 * t3 is creator
		 * t4 is task type
		 * t5 is task status
		 * t6 is equipment
		 * t7 is room
		 * */
		SelectQuery baseSql = SelectQueryBuilder.selectQuery(
                "t1." + Table.TaskTable.ID, "t1." + Table.TaskTable.STATUS,
                "t1." + Table.TaskTable.TYPE, "t1." + Table.TaskTable.EQUIPMENT,
                "t1." + Table.TaskTable.CREATOR , "t1." + Table.TaskTable.STAFF,
                "t1." + Table.TaskTable.CREATION_TIME, "t1." + Table.TaskTable.COMPLETION_TIME,
                "t1." + Table.TaskTable.VISIBILITY, "t4.*", "t5.*", "t6.*", "t7.*"
        );
        
        baseSql.column("t2." + Table.UserTable.FIRST_NAME, "t_staff_fn");
        baseSql.column("t2." + Table.UserTable.LAST_NAME, "t_staff_ln");
        baseSql.column("t2." + Table.UserTable.EMAIL, "t_staff_email");
        
        baseSql.column("t3." + Table.UserTable.FIRST_NAME, "t_user_fn");
        baseSql.column("t3." + Table.UserTable.LAST_NAME, "t_user_ln");
        baseSql.column("t3." + Table.UserTable.EMAIL, "t_user_email");
        
        Join joinStaff = JoinBuilder.innerJoin(
        		Table.UserTable.TABLE_NAME, "t2",
        		"t1." + Table.TaskTable.STAFF, Logical.Operator.EQ, "t2." + Table.UserTable.ID
        		);
        
        Join joinUser = JoinBuilder.innerJoin(
        		Table.UserTable.TABLE_NAME, "t3", 
        		"t1." + Table.TaskTable.CREATOR, Logical.Operator.EQ, "t3." + Table.UserTable.ID);
        
        Join joinTaskType = JoinBuilder.innerJoin(
        		Table.TaskTypeTable.TABLE_NAME, "t4",
        		"t1." + Table.TaskTable.TYPE, Logical.Operator.EQ, "t4." + Table.TaskTypeTable.ID);
        
        Join joinTaskStatus = JoinBuilder.innerJoin(
        		Table.TaskStatusTable.TABLE_NAME, "t5",
        		"t1." + Table.TaskTable.STATUS, Logical.Operator.EQ, "t5." + Table.TaskStatusTable.ID);
        
        Join joinEquipment = JoinBuilder.innerJoin(
        		Table.EquipmentTable.TABLE_NAME, "t6",
        		"t1." + Table.TaskTable.EQUIPMENT, Operator.EQ, "t6." + Table.EquipmentTable.ID);
        
        Join joinRoom = JoinBuilder.innerJoin(
        		Table.RoomTable.TABLE_NAME, "t7",
        		"t7." + Table.RoomTable.ID, Operator.EQ, "t6." + Table.EquipmentTable.ROOM).
        		andOn("t1." + Table.TaskTable.EQUIPMENT, Operator.EQ, "t6." + Table.EquipmentTable.ID);
        
        baseSql.from(Table.TaskTable.TABLE_NAME, "t1").
        join(joinStaff).
        join(joinUser).
        join(joinTaskType).
        join(joinTaskStatus).
        join(joinEquipment).
        join(joinRoom);
        

        System.out.println("*********test search query********");
        System.out.println("SQL: "+baseSql.toSql());
        
        Statement ps = null;
        
        try {
			ps = connection.createStatement();
			ResultSet rs = ps.executeQuery(baseSql.toSql());
			
			while(rs.next()) {
				System.out.println(rs.getInt(Table.TaskTable.ID));
			}
			
			connection.close();
		} catch (SQLException e) {
			fail(e.getMessage());
		}
    }
    
    @Test
    public void testInsertQuery() {
    	
    	InsertQuery insert = new InsertQuery(TABLE_USER, '\'');
    	String sql = insert.columns(
	    			"name",
	    			"age",
	    			"salary"
	    			).
	    	        values(
	    			"Mark",
	    			21,
	    			240
	    			).toSql();
    	
    	System.out.println("*********test insert query********");
        System.out.println("SQL: " + sql);
    	
    	try {
			connection.prepareStatement(sql.toString()).execute();
		} catch (SQLException e) {
			fail(e.getMessage());
		}
    }
    
    @Test
    public void testInsertPreparedQuery() {
    	
    	InsertQuery insert = new InsertQuery(TABLE_USER);
    	String sql = insert.columns(
	    			"name",
	    			"age",
	    			"salary"
	    			).
	    	        values(
	    			'?',
	    			'?',
	    			'?'
	    			).toSql();
    	
    	System.out.println("*********test insert prepared query********");
        System.out.println("SQL: " + sql);
    	
    	try (PreparedStatement ps = connection.prepareStatement(sql)) {
    		
    		ps.setString(1, "'Mark'");
    		ps.setInt(2, 21);
    		ps.setInt(3, 240);
    		
    		ps.execute();
		} catch (SQLException e) {
			fail(e.getMessage());
		}
    }

    public static void onTearDown() throws SQLException {
        connection.close();
    }
}

