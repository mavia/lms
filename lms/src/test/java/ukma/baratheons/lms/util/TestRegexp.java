package ukma.baratheons.lms.util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * This class tests regular expressions
 * which are used by the system
 * @author Max Oliynick
 * */

@RunWith(Parameterized.class)
public class TestRegexp {
	
	private static final Pattern SPLITTER = Pattern.compile("[\\s*](?=[\\'\\[\\\"])|(?<=[\\'\\]\\\"])[\\s*]");
	private static final Pattern MULTIPLE_SPACES = Pattern.compile("\\s{2,}+");
	private static final Pattern ALL_DIGITS = Pattern.compile("^[0-9]+$");
	private static final Pattern HAS_DIGITS = Pattern.compile("(.*)[0-9](.*)");
	
	private static class Touple {
		
		private final String input;
		private final String[] expected;
		
		public Touple(final String input, final String[] expected) {
			this.input = input;
			this.expected = Arrays.copyOf(expected, expected.length);
		}

		public String getInput() {
			return input;
		}

		public String[] getExpected() {
			return expected;
		}
		
	}
	
	private Touple [] testSet = null;
	
	/**
	 * Filling test set with test data
	 * */
	public TestRegexp(final Touple[] args) {
		this.testSet = Arrays.copyOf(args, args.length);
	}

	@Test
	public void testSplitter() {
		
		String [] actualArr = null;
		String actual = null;
		for(final Touple t : testSet) {
			
			actualArr = t.getInput().split(SPLITTER.toString());
			
			for(int i = 0; i < t.getExpected().length; ++i) { 
				
				actual = actualArr[i].replaceAll(MULTIPLE_SPACES.toString(), "");
				
				Assert.assertEquals(
						"Error on testSplitter() on inputs: expected " 
				        + t.getExpected()[i] 
				        + " but was: " + actual,
				        t.getExpected()[i],
						actual);
			}
			
		}
		
	}
	
	@Parameters
	//	Data set
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][]{
		    {new Touple []
		    		{
		    		new Touple(
		    				"example@gmail.com \"In progress\" 345   ",
		    				new String[]{"example@gmail.com", "\"In progress\"", "345"}
		    				),
		    		new Touple(
		    				"   emaxxx@yandexmail.ru       [Waiting for approvation]    room-101",
		    				new String[]{"emaxxx@yandexmail.ru", "[Waiting for approvation]", "room-101"}
		    				),
		    		new Touple(
		    				"somemail@mail.com    'some text'    pc-1", 
		    				new String[]{"somemail@mail.com", "'some text'", "pc-1"}
		    				)
		    		}
		    },
		    {new Touple []
		    		{
		    		new Touple(
		    				"Max Oliynick \"In progress\" 345 canceled",
		    				new String[]{"example@gmail.com", "\"In progress\"", "345"}
		    				),
		    		new Touple(
		    				"   emaxxx@yandexmail.ru       [Waiting for approvation]    room-101",
		    				new String[]{"emaxxx@yandexmail.ru", "[Waiting for approvation]", "room-101"}
		    				),
		    		new Touple(
		    				"somemail@mail.com    'some text'    pc-1", 
		    				new String[]{"somemail@mail.com", "'some text'", "pc-1"}
		    				)
		    		}
		    }
			/*
			 
			 {"Max Oliynick \"In progress\" 345 canceled", 
				 "'In progress]",
				 "[In progress'"},
				 
			 {"324234", "234234 r", "task_No 3", "room-101"},
				 
			 {"task-3", "user-5", "room-333"},*/
		});
	}


}
