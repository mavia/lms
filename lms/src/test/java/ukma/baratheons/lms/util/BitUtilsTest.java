package ukma.baratheons.lms.util;
import static org.junit.Assert.*;

import org.hamcrest.core.Is;
import org.junit.Test;

/**
 * Tests class {@link BitUtils}
 * @author Max Oliynick
 * */
public class BitUtilsTest {

    @Test
    public void testUnsetBit() {
        assertThat("error in the unsetBit method", 50, Is.is(BitUtils.unsetBit(3, 54)));
        assertThat("error in the unsetBit method", 254, Is.is(BitUtils.unsetBit(1, 255)));
        assertThat("error in the unsetBit method", 50, Is.is(BitUtils.unsetBit(35, 54)));
        assertThat("error in the unsetBit method", 254, Is.is(BitUtils.unsetBit(33, 255)));
    }

    @Test
    public void testSetBit() {
        assertThat("error in the setBit method", 54, Is.is(BitUtils.setBit(3, 50)));
        assertThat("error in the setBit method", 255, Is.is(BitUtils.setBit(1, 254)));
        assertThat("error in the setBit method", 54, Is.is(BitUtils.setBit(35, 50)));
        assertThat("error in the setBit method", 255, Is.is(BitUtils.setBit(33, 254)));
    }

    @Test
    public void testBit() {

        int before = 2523;
        int position = 7;

        assertThat(before, Is.is(BitUtils.setBit(position, BitUtils.unsetBit(position, before))));
    }

    @Test
    public void testCheckIsSet() {
        assertTrue("error on method isBitSet(6,35)", BitUtils.isBitSet(6, 35));
        assertFalse("error on method isBitSet(10,5673)", BitUtils.isBitSet(14, 5673));
    }

    @Test
    public void testSetByMask() {
        int mask = BitUtils.BitMask.BYTE_1 | BitUtils.BitMask.BYTE_2;
        mask = mask | BitUtils.BitMask.BYTE_3 | BitUtils.BitMask.BYTE_4;

        assertThat("error in the setByMask method", Integer.MAX_VALUE, Is.is(BitUtils.setByMask(mask, 0)));
    }

    @Test
    public void testUnsetByMask() {
        int mask = BitUtils.BitMask.BYTE_1 | BitUtils.BitMask.BYTE_2;
        mask = mask | BitUtils.BitMask.BYTE_3 | BitUtils.BitMask.BYTE_4;

        assertThat("error in the unsetByMask method", 0, Is.is(BitUtils.unsetByMask(mask, Integer.MAX_VALUE)));
    }

    @Test
    public void testSetByMaskS() {
        int mask = BitUtils.BitMask.BYTE_1 | BitUtils.BitMask.BYTE_2;
        mask = mask | BitUtils.BitMask.BYTE_3 | BitUtils.BitMask.BYTE_4_S;

        assertThat("error in the setByMask method", -1, Is.is(BitUtils.setByMask(mask, 0)));
    }

    @Test
    public void testUnsetByMaskS() {
        int mask = BitUtils.BitMask.BYTE_1 | BitUtils.BitMask.BYTE_2;
        mask = mask | BitUtils.BitMask.BYTE_3 | BitUtils.BitMask.BYTE_4_S;

        assertThat("error in the unsetByMask method", 0, Is.is(BitUtils.unsetByMask(mask, -1)));
    }
}