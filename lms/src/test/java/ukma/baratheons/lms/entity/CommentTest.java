package ukma.baratheons.lms.entity;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.Timestamp;

import static org.testng.Assert.assertEquals;

public class CommentTest {
    CommentStub stub_one;
    CommentStub stub_two;
    CommentStub stub_three;

    @BeforeTest
    public void setUp() throws Exception {
        System.err.println("start");
        stub_one = new CommentStub(1, "comment", new Timestamp(1420070400L));
        stub_two = new CommentStub(2, "comment", new Timestamp(1420070400L));
        stub_three = new CommentStub(1, "comment", new Timestamp(1420070401L));
    }

    @Test
    public void testHashCodeWithDiffUserId() throws Exception {
        System.err.println("test1");
        if (stub_one.equals(stub_two)) {
            assertEquals(stub_one.hashCode(), stub_two.hashCode());
        }
    }

    @Test
    public void testHashCodeWithDiffTime() throws Exception {
        System.err.println("test2");
        if (stub_one.equals(stub_three)) {
            assertEquals(stub_one.hashCode(), stub_three.hashCode());
        }
    }


    private class CommentStub extends Comment {
        public CommentStub(long id, String text, Timestamp date) {
            super(id, text, date, 0, 0, 0);
        }
    }
}