package ukma.baratheons.lms.entity;

import org.testng.annotations.BeforeMethod;

import java.util.Date;

import static org.testng.Assert.assertEquals;

public class BanTimeTest {
    BanTimeStub stub_one;
    BanTimeStub stub_two;

    @BeforeMethod
    public void setUp() throws Exception {
        stub_one = new BanTimeStub(1, new Date(1420070400L));
        stub_two = new BanTimeStub(1, new Date(1420070401L));

    }

    @org.testng.annotations.Test
    public void testHashCode() throws Exception {
        if (stub_one.equals(stub_two)) {
            assertEquals(stub_one.hashCode(), stub_two.hashCode());
        }

    }

    private class BanTimeStub extends BanTime {
        public BanTimeStub(int penaltyScore, Date date) {
            super(penaltyScore,date,null);
        }
    }
}